var logo_loaded_status = 0;
var video_loaded_status = 0;
var upload_num = 0;

$(function(){
    var uploader_logo = WebUploader.create({
        swf: '/js/Uploader.swf',
        compress: null,
        server: upload_url,
        pick: '#picker-logo',
        resize: false
    });
    uploader_logo.on('fileQueued', function(file) {
        logo_loaded_status = 1;
        uploader_logo.makeThumb(file, function(error, ret){
            var image_html = '<img src="' + ret + '" class="img-thumbnail">';
            $("#thelist-logo").html(image_html);
        }, 0.5, 0.5);
    });
    uploader_logo.on('uploadSuccess', function(file, response){
        setLogoValue(response.id);
        upload_num --;
        if (isFormSubmit()) {
            submitForm();
        }
    });
    var uploader_video = WebUploader.create({
        swf: '/js/Uploader.swf',
        compress: null,
        server: upload_url,
        pick: '#picker-video',
        resize: false
    });
    uploader_video.on('fileQueued', function(file) {
        video_loaded_status = 1;
        uploader_video.makeThumb(file, function(error, ret){
            var image_html = '<img src="' + ret + '" class="img-thumbnail">';
            $("#thelist-video").html(image_html);
        }, 0.5, 0.5);
    });
    uploader_video.on('uploadSuccess', function(file, response){
        setVideoValue(response.id);
        upload_num --;
        if (isFormSubmit()) {
            submitForm();
        }
    });
    $("#submit").on('click', function(){
        layer.load(1, {
          shade: [0.4,'#000']
        });
        switch(true){
            case isLogoLoaded() && isVideoLoaded() :
                upload_num = 2;
                uploader_logo.upload();
                uploader_video.upload();
                break;
            case isLogoLoaded():
                upload_num = 1;
                uploader_logo.upload();
                break;
            case isVideoLoaded():
                upload_num = 1;
                uploader_video.upload();
                break;
            default:
                submitForm();
        }   
    });
});
function isLogoLoaded(){
    if(logo_loaded_status === 1){
        return true;
    }
    return false;
}
function isVideoLoaded(){
    if(video_loaded_status === 1){
        return true;
    }
    return false;
}
function setLogoValue(value){
    $("input[name='logo']").val(value);
}
function setVideoValue(value){
    $("input[name='video']").val(value);
}
function isFormSubmit(){
    if (upload_num === 0) {
        return true;
    }
    return false;
}
function submitForm(){
    var form = document.getElementById("update-settings-form");
    form.submit();
}
function isImagesLoaded(){
    if (images_loaded_status === 1) {
        return true;
    }
    return false;
}