$(function(){
    /**
     * 事件：删除订单
     */
    $("#table-orders").on('click', '.delete-order', function(){
        var order_id = $(this).attr("data-id");
        var url = "/api/orders/" + order_id; 
        $.ajax({
            method: "delete",
            url: url,
            success: function(data){
                refreshOrdersTable();
            }
        });
    });

    /**
     * 事件：提交单个订单
     */
    $("#table-orders").on('click', '.confirm-order', function(){
        var order_id = $(this).attr("data-id");
        var url = "/orders/" + order_id + "/confirm";
        layer.confirm("Order submitted", {
            btn: ['OK', 'Cancel'],
            title: "Message"
        }, function(){
            $.ajax({
                method: "post",
                url: url,
                success: function(data){
                    layer.msg("Success！");
                    refreshOrdersTable();
                }
            });
        })
    });

    /**
     * 事件：批量提交订单
     */
    $("#multi-confirm").on('click', function(){
        var selected_orders = $("#table-orders").bootstrapTable('getAllSelections');
        if (selected_orders.length === 0) {
            return layer.msg("Please select orders!");
        }
        layer.confirm("Order submitted", {
            btn: ['OK', 'Cancel'],
            title: "Message"
        }, function(){
            $.ajax({
                method: "post",
                url: '/orders/confirm',
                data: {
                    ids: selected_orders.map(function(order){
                        return order.id;
                    }),
                },
                success: function(data){
                    layer.msg("Success！");
                    refreshOrdersTable();
                }
            });
        })
    });
    /**
     * 事件：table单选事件
     */
    $("#table-orders").on('check.bs.table', function(){
        calculateOrdersTotal()
    });
    $("#table-orders").on('uncheck.bs.table', function(){
        calculateOrdersTotal()
    });

    /**
     * 事件：table全选事件
     */
    $("#table-orders").on('check-all.bs.table', function(){
        calculateOrdersTotal()
    });
    $("#table-orders").on('uncheck-all.bs.table', function(){
        calculateOrdersTotal()
    });

    /**
     * 事件：订单导出
     */
    $("#export-orders").on('click', function(){
        var selected_orders = $("#table-orders").bootstrapTable('getAllSelections');
        var order_ids = selected_orders.map(function(order){
            return order.id;
        });
        window.location.href = "/admin/orders/export?ids=" + order_ids.join();
    })
});

/**
 * 计算并更新订单金额
 */
function calculateOrdersTotal(){
    var total = 0;
    var selected_orders = $("#table-orders").bootstrapTable('getAllSelections');
    selected_orders.forEach(function(order){
        total += parseFloat(order.product.price) * parseInt(order.purchase_number, 10);
    });
    $("#total").text(total);
}

function refreshOrdersTable(){
    $('#table-orders').bootstrapTable('refresh');
}

function formatResponseData(data){
    data.total = data.meta.pagination.total;
    return data;
}

function formatTime(value, row, index){
    return value.date ? value.date.substring(0,19) : "";
}

function formatOperate(value, row, index){
    var html = "<div class='btn-group' role='group'>";
    if (role === "role_user") {
        html += "<a class='btn btn-default btn-xs' href='/orders/" + value + "/update'>Modify</a>";
    }else{
        html += "<a class='btn btn-default btn-xs' href='/admin/orders/update/" + value + "'>Modified</a>";
    }
    if (row.status === 0) {
        html += "<a class='btn btn-success btn-xs confirm-order' data-id='" + value + "'>Ok</a><a data-id='" + value + "' class='btn btn-danger btn-xs delete-order'>Delete</a>";
    }else if(role === "role_user"){
        return "Order submitted";
    }
    html += "</div>";
    return html;
}

function formatUser(value, row, index){
    return value.name;
}

function formatProduct(value, row, index){
    if (value === null) {
        return "<span class='text-danger'>Product has been deleted!</span>";
    }
    var image_array = value.image_ids.split(",");
    var html = "<img class='img-thumbnail' width=200 src='/images/" + image_array[0] + "'>";
    return html;
}