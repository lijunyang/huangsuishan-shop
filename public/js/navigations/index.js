$(function(){
    $("#table-navigations").on('click', '.delete-navigation', function(){
        var navigation_id = $(this).attr("data-id");
        var url = "/api/navigations/" + navigation_id; 
        $.ajax({
            method: "delete",
            url: url,
            success: function(data){
                refreshNavigationsTable();
            }
        });
    });
});

function refreshNavigationsTable(){
    $('#table-navigations').bootstrapTable('refresh');
}
function formatResponseData(data){
    data.total = data.meta.pagination.total;
    return data;
}

function formatTime(value, row, index){
    return value.date ? value.date.substring(0,19) : "";
}

function formatOperate(value, row, index){
    var html = "<div class='btn-group' role='group'><a class='btn btn-default btn-xs' href='/admin/navigations/update/" + value + "'>修改</a><a data-id='" + value + "' class='btn btn-danger btn-xs delete-navigation'>删除</a></div>";
    return html;
}
function formatParentNavigation(value, row, index){
    if(value === 0){
        return "顶级导航";
    }
    for (var key in navigations){
        if (navigations[key].id === value) {
            return navigations[key].title;
        }
    }
}
function formatType(value, row, index){
    return navigation_type[value];
}
function formatRelatedPage(value, row, index){
    switch(row.type){
        case 1:
            for (var key in collections){
                if (collections[key].id === value){
                    return collections[key].title;
                }
            }
        case 2:
            for (var key in pages){
                if (pages[key].id === value){
                    return pages[key].title;
                }
            }
    }
}