$(function(){
    if (!isCurrentNavigationSetted()){
        initRelatedPages();
    }
    $("select[name='type']").on('change', function(){
        initRelatedPages();
    });
    $("#submit").on('click', function(){
        $("#add-navigation-form").submit();
    });
});

function initRelatedPages(){
    var type = parseInt(getType());
    getAllRelatedPages(type);
}
function getType(){
    var type = $("select[name='type']").val();
    return type;
}
function getAllRelatedPages(type){
    switch(type){
        case 1:
            getAllCollections();
            break;
        case 2:
            getAllPages();
            break;
    }
}
function getAllCollections(){
    $.ajax({
        url: "/api/collections/getAll",
        method: "GET",
        success: function(data){
            renderRelatedPage(data);
        }
    });
}
function getAllPages(){
    $.ajax({
        url: "/api/pages/getAll",
        method: "GET",
        success: function(data){
            renderRelatedPage(data);
        }
    });
}
function renderRelatedPage(data_array){
    var select_html;
    for(var key in data_array){
        select_html += '<option value=' + data_array[key].id;
        if (isCurrentNavigationSetted()) {
            if (data_array[key].id === current_navigation.id) {
                select_html += " selected='selected' ";
            }
        }
        select_html += '>' + data_array[key].title + '</option>';
    }
    $("select[name='type_id']").html(select_html);
}
function isCurrentNavigationSetted(){
    if (typeof current_navigation === 'undefined'){
        return false;
    }
    return true;
}