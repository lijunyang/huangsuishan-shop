$(function(){
    $("#table-users").on('click', '.delete-user', function(){
        var user_id = $(this).attr("data-id");
        var url = "/api/users/" + user_id; 
        $.ajax({
            method: "delete",
            url: url,
            success: function(data){
                refreshUsersTable();
            }
        });
    });
});

function refreshUsersTable(){
    $('#table-users').bootstrapTable('refresh');
}

function formatResponseData(data){
    data.total = data.meta.pagination.total;
    return data;
}

function formatOperate(value, row, index){
    var html = "<div class='btn-group' role='group'><a class='btn btn-default btn-xs' href='/admin/users/update/" + value + "'>修改</a><a data-id='" + value + "' class='btn btn-danger btn-xs delete-user'>删除</a></div>";
    return html;
}

function formatRole(value, row, index){
    var rtn;
    switch(value){
        case 'role_user':
            rtn = "普通用户";
            break;
        case 'role_admin':
            rtn = "管理员";
            break;
    }
    return rtn;
}