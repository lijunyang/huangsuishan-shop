var images_num = 0;

$(function(){
    var uploader = WebUploader.create({
        swf: '/js/Uploader.swf',
        server: upload_url,
        pick: '#picker',
        fileNumLimit: 30,
        resize: false,
        thumb: {
            width: 110,
            crop: false
        }
    });
    uploader.on('fileQueued', function(file) {
        images_num ++;
        uploader.makeThumb(file, function(error, ret){
            var image_html = '<div class="multiple-images col-md-3" id="' + file.id + '" ><img src="' + ret + '" class="img-thumbnail"><br><a class="btn btn-xs btn-danger delete-image">删除</a></div>';
            $("#thelist").prepend(image_html);
            $("#" + file.id + "").on('click', '.delete-image', function(){
                uploader.removeFile(file, true);
                images_num --;
                $("#" + file.id + "").remove();
            });
        });
    });
    uploader.on('uploadSuccess', function(file, response){
        updateImageIdsValue(response.id);
        images_num --;
        if (isFormSubmit()) {
            $("#add-product-form").submit();
        }
    });

    $("#submit").on('click', function(){
        layer.load(1, {
          shade: [0.4,'#000']
        });
        if (images_num === 0){
            $("#add-product-form").submit();
        }else{
            uploader.upload();
        }
    });

    $("#thelist").on('click', '.related-image', function(){
        var image_id = $(this).attr("data-id");
        deleteIdFromImageIdsValue(image_id);
        $(this).closest(".multiple-images").remove();
    });

    $("#collection-ids").select2({
        theme: "classic"
    });
});

function isFormSubmit(){
    if (images_num === 0){
        return true;
    }
    return false;
}
function updateImageIdsValue(id){
    var image_ids = $("input[name='image_ids']").val();
    if (image_ids === "") {
        image_ids = id;
    }else{
        image_ids += "," + id; 
    }
    $("input[name='image_ids']").val(image_ids);
}
function deleteIdFromImageIdsValue(id){
    var image_ids = $("input[name='image_ids']").val();
    image_ids_array = image_ids.split(",");
    image_ids_array = image_ids_array.filter(function(image_id){
        return image_id !== id;
    });
    image_ids = image_ids_array.join(",");
    $("input[name='image_ids']").val(image_ids);
}