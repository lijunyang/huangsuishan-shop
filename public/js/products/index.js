$(function(){
    $("#table-products").on('click', '.delete-article', function(){
        var article_id = $(this).attr("data-id");
        var url = "/api/products/" + article_id; 
        $.ajax({
            method: "delete",
            url: url,
            success: function(data){
                refreshArticlesTable();
            }
        });
    });
    $("#search-products").on("click", function(){
        $('#table-products').bootstrapTable('refresh');
    });
});

function queryParams(params){
    params.collection_id = $("select[name='collection_id']").val();
    params.brand = $("input[name='brand']").val();
    return params;
}

function refreshArticlesTable(){
    $('#table-products').bootstrapTable('refresh');
}

function formatResponseData(data){
    data.total = data.meta.pagination.total;
    return data;
}

function formatTime(value, row, index){
    return value.date ? value.date.substring(0,19) : "";
}

function formatImage(value, row, index){
    if (value === null) {
        return "-";
    }
    var image_array = value.split(",");
    var html = "<img class='img-thumbnail' width=200 src='/images/" + image_array[0] + "'>";
    return html;
}

function formatOperate(value, row, index){
    var html = "<div class='btn-group' role='group'><a class='btn btn-default btn-xs' href='/admin/products/update/" + value + "'>修改</a><a data-id='" + value + "' class='btn btn-danger btn-xs delete-article'>删除</a></div>";
    return html;
}

function formatCollection(value, row, index){
    if (value.length === 0) {
        return "无";
    }
    var collections_title = "";
    for (var key in value){
        collections_title += value[key].title + ",";
    }
    collections_title = collections_title.replace(/,$/, "");
    return collections_title;
}