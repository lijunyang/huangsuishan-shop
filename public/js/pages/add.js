var image_pc_loaded_status = 0;
var image_m_loaded_status = 0;
var upload_images_num = 0;

function submitForm(){
    var form = document.getElementById("add-page-form");
    form.submit();
}

$(function() {
    var uploader_pc = WebUploader.create({
        swf: '/js/Uploader.swf',
        server: upload_url,
        pick: '#picker-pc',
        resize: false
    });
    uploader_pc.on('fileQueued', function(file) {
        image_pc_loaded_status = 1;
        uploader_pc.makeThumb(file, function(error, ret){
            var image_html = '<img src="' + ret + '" class="img-thumbnail">';
            $("#thelist-pc").html(image_html);
        }, 1, 1);
    });
    uploader_pc.on('uploadSuccess', function(file, response){
        setPcImageValue(response.id);
        upload_images_num --;
        if (isFormSubmit()) {
            submitForm();
        }
    });

    $("#submit").on('click', function(){
        switch(true){
            case isPcImageLoaded():
                upload_images_num = 1;
                submitPcImage();
                break;
            default:
                submitForm();
        }   
    });

    function submitPcImage(){
        uploader_pc.upload();
    }
});

function isPcImageLoaded(){
    if(image_pc_loaded_status === 1){
        return true;
    }
    return false;
}
function setPcImageValue(value){
    $("input[name='pc_image_id']").val(value);
}
function isFormSubmit(){
    if (upload_images_num === 0) {
        return true;
    }
    return false;
}