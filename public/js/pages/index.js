$(function(){
    $("#table-pages").on('click', '.delete-article', function(){
        var article_id = $(this).attr("data-id");
        var url = "/api/pages/" + article_id; 
        $.ajax({
            method: "delete",
            url: url,
            success: function(data){
                refreshArticlesTable();
            }
        });
    });
});

function refreshArticlesTable(){
    $('#table-pages').bootstrapTable('refresh');
}

function formatResponseData(data){
    data.total = data.meta.pagination.total;
    return data;
}

function formatTime(value, row, index){
    return value.date ? value.date.substring(0,19) : "";
}

function formatImage(value, row, index){
    if (value === null) {
        return "-";
    }
    var html = "<img class='img-thumbnail' width=200 src='/images/" + value + "'>";
    return html;
}

function formatOperate(value, row, index){
    var html = "<div class='btn-group' role='group'><a class='btn btn-default btn-xs' href='/admin/pages/update/" + value + "'>修改</a><a data-id='" + value + "' class='btn btn-danger btn-xs delete-article'>删除</a></div>";
    return html;
}

function formatCollection(value, row, index){
    return value.title;
}