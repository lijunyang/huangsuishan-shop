var image_pc_loaded_status = 0;
var image_m_loaded_status = 0;
var upload_images_num = 0;

$("#submit").on("click", function(){
    submitForm();
});
function submitForm(){
    var form = document.getElementById("add-article-form");
    form.submit();
}

$(function() {
    var uploader_pc = WebUploader.create({
        swf: '/js/Uploader.swf',
        server: upload_url,
        pick: '#picker-pc',
        resize: false
    });
    uploader_pc.on('fileQueued', function(file) {
        image_pc_loaded_status = 1;
        uploader_pc.makeThumb(file, function(error, ret){
            var image_html = '<img src="' + ret + '" class="img-thumbnail">';
            $("#thelist-pc").html(image_html);
        }, 0.5, 0.5);
    });
    uploader_pc.on('uploadSuccess', function(file, response){
        setPcImageValue(response.id);
        upload_images_num --;
        if (isFormSubmit()) {
            submitForm();
        }
    });

    var uploader_m = WebUploader.create({
        swf: '/js/Uploader.swf',
        server: upload_url,
        pick: '#picker-m',
        resize: false
    });
    uploader_m.on('fileQueued', function(file) {
        image_m_loaded_status = 1;
        uploader_m.makeThumb(file, function(error, ret){
            var image_html = '<img src="' + ret + '" class="img-thumbnail">';
            $("#thelist-m").html(image_html);
        }, 0.5, 0.5);
    });
    uploader_m.on('uploadSuccess', function(file, response){
        setMImageValue(response.id);
        upload_images_num --;
        if (isFormSubmit()) {
            submitForm();
        }
    });

    $("#submit").on('click', function(){
        switch(true){
            case isPcImageLoaded() && isMImageLoaded() :
                upload_images_num = 2;
                submitPcImage();
                submitMImage();
                break;
            case isPcImageLoaded():
                upload_images_num = 1;
                submitPcImage();
                break;
            case isMImageLoaded():
                upload_images_num = 1;
                submitMImage();
                break;
            default:
                submitForm();
        }   
    });

    function submitPcImage(){
        uploader_pc.upload();
    }
    function submitMImage(){
        uploader_m.upload();
    }
});

function isPcImageLoaded(){
    if(image_pc_loaded_status === 1){
        return true;
    }
    return false;
}
function isMImageLoaded(){
    if(image_m_loaded_status === 1){
        return true;
    }
    return false;
}
function setPcImageValue(value){
    $("input[name='pc_image_id']").val(value);
}
function setMImageValue(value){
    $("input[name='app_image_id']").val(value);
}
function isFormSubmit(){
    if (upload_images_num === 0) {
        return true;
    }
    return false;
}