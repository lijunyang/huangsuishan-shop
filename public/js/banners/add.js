var image_loaded_status = 0;
var upload_images_num = 0;

function submitForm(){
    var form = document.getElementById("add-banner-form");
    form.submit();
}

$(function() {
    var uploader = WebUploader.create({
        swf: '/js/Uploader.swf',
        compress: null,
        server: upload_url,
        pick: '#picker-pc',
        resize: false
    });
    uploader.on('fileQueued', function(file) {
        image_loaded_status = 1;
        uploader.makeThumb(file, function(error, ret){
            var image_html = '<img src="' + ret + '" class="img-thumbnail">';
            $("#thelist-pc").html(image_html);
        }, 0.5, 0.5);
    });
    uploader.on('uploadSuccess', function(file, response){
        setImageValue(response.id);
        upload_images_num --;
        if (isFormSubmit()) {
            submitForm();
        }
    });

    $("#submit").on('click', function(){
        layer.load(1, {
          shade: [0.4,'#000']
        });
        switch(true){
            case isImageLoaded():
                upload_images_num = 1;
                submitImage();
                break;
            default:
                submitForm();
        }   
    });

    function submitImage(){
        uploader.upload();
    }
});

function isImageLoaded(){
    if(image_loaded_status === 1){
        return true;
    }
    return false;
}
function setImageValue(value){
    $("input[name='image_id']").val(value);
}
function isFormSubmit(){
    if (upload_images_num === 0) {
        return true;
    }
    return false;
}