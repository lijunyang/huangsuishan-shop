let current_page = 1;
$(function(){
    filter_collections.push(filterCollectionsIdArray(collections_id));
    // getProducts();
    addPagerEvent();
    addSearchCollectionsEvent();
});

function getProducts(){
    $.ajax({
        'url': "/front/products",
        'method': "get",
        'data': {
            'limit': 18,
            'order': 'desc',
            'page': current_page,
            'collections_id': JSON.stringify(filter_collections),
        },
        'success': function(data){
            let products_html = "";
            for (let key in data.data){
                products_html += parseProductTemplateByProduct(data.data[key]);
            }
            $(".product-list").html(products_html);
            updatePager(data.meta.pagination);
        }
    });
}

function parseProductTemplateByProduct(product){
    let product_html = $("#product-template").html();
    product_html = product_html.replace(/{\*\s* id \s*\*}/m, product.id);
    product_html = product_html.replace(/{\*\s* image_id \s*\*}/m, product.image_ids.split(",")[0]);
    product_html = product_html.replace(/{\*\s* title \s*\*}/m, product.title);
    product_html = product_html.replace(/{\*\s* type \s*\*}/m, product.type);
    product_html = product_html.replace(/{\*\s* price \s*\*}/m, product.price);
    return product_html;
}

function updatePager(pagination){
    if (current_page === pagination.total_pages) {
        $(".next").addClass("disabled");
    }else{
        $(".next").removeClass("disabled");
    }
    if (current_page === 1) {
        $(".previous").addClass("disabled");
    }else{
        $(".previous").removeClass("disabled");
    }
    const per_page_select = `
        <select class="form-control pagination-page-select">
            <option value="10">10</option>
            <option value="20">20</option>
        </select>
    `;
    $(".pagination-info").html("Page:<input class='form-control pagination-page-input' value=" + pagination.current_page + " /> <span class='pagination-page-total'>Total: " + pagination.total + "</span> PerPage:" + per_page_select );
}

function addPagerEvent(){
    // $(".previous").on("click", function(){
    //     if (!$(this).hasClass("disabled")) {
    //         current_page --;
    //         getProducts();
    //     }
    // });
    // $(".next").on("click", function(){
    //     if (!$(this).hasClass("disabled")) {
    //         current_page ++;
    //         getProducts();
    //     }
    // });
    $("#change-page").on('click', function(){
        var page = $("#page").val();
        var per_page = $("#per-page").val();
        window.location.href = window.location.pathname + "?page=" + page + "&limit=" + per_page;
    });
    $('#per-page').on('change', function(){
        var page = $("#page").val();
        var per_page = $("#per-page").val();
        window.location.href = window.location.pathname + "?page=" + page + "&limit=" + per_page;
    })
}

function addSearchCollectionsEvent(){
    $(".search-collections input[type='checkbox']").change(function(){
        filter_collections = [];
        filter_collections.push(filterCollectionsIdArray(collections_id));
        $(".search-collections").each(function(search_collection){
            let search_collections_id = [];
            $(this).find("input[type='checkbox']").each(function(){
                if ($(this).prop("checked") === true) {
                    search_collections_id.push($(this).val());
                }
            });
            filter_collections.push(filterCollectionsIdArray(search_collections_id));
        });
        getProducts();
    });
    
}
function filterCollectionsIdArray(collections_id_array){
    collections_id_array = collections_id_array.map(function(collection_id){
        return collection_id.replace(/^\d*-/, "");
    });
    return collections_id_array;
}
