<?php

Route::get('login', [
	'uses' => 'Auth\LoginController@index',
	'as' => 'auth.login.index',
]);
Route::get('/front/login', [
	'uses' => 'Auth\LoginController@frontLogin',
	'as' => 'auth.front.login',
]);
Route::post('/front/login', [
	'uses' => 'Auth\LoginController@frontLoginPost',
	'as' => 'auth.front.loginPost',
]);
Route::post('login', [
	'uses' => 'Auth\LoginController@auth',
	'as' => 'auth.login.auth',
]);
Route::get('logout', [
	'uses' => 'Auth\LoginController@logout',
	'as' => 'auth.logout',
]);
Route::get('/', [
	'uses' => 'IndexController@index',
	'as' => 'front.index',
]);
Route::get('/register', [
	'uses' => 'IndexController@register',
	'as' => 'front.register',
]);
Route::post('/register', [
	'uses' => 'IndexController@userAdd',
	'as' => 'front.userAdd',
]);
Route::get('/images/{image_id}', [
	'uses' => 'IndexController@showImage',
	'as' => 'front.showImage',
]);
Route::get('/collections/{collection_id}/{navigation_id?}', [
	'uses' => 'IndexController@collection',
	'as' => 'front.collection',
]);
Route::get('/navigations/{parent_navigation_id}/all', [
	'uses' => 'IndexController@showSonNavigations',
	'as' => 'front.showSonNavigations',
]);
Route::get('/navigations/{id}', [
	'uses' => 'IndexController@navigation',
	'as' => 'front.navigation',
]);
Route::get('/recently/products', [
	'uses' => 'IndexController@recentlyProducts',
	'as' => 'front.recentlyProducts',
]);
Route::get('/pages/{page_id}', [
	'uses' => 'IndexController@page',
	'as' => 'front.page',
]);
Route::get('/products/{product_id}', [
	'uses' => 'IndexController@product',
	'as' => 'front.product',
]);
Route::get('/search/{key_words}', [
	'uses' => 'IndexController@search',
	'as' => 'front.search',
]);

Route::post('/order/addPost', [
	'uses' => 'OrderController@addPost',
	'middleware' => 'auth.front',
	'as' => 'front.orders.addPost',
]);
Route::get('/orders/{id}/update', [
	'uses' => 'OrderController@frontUpdate',
	'middleware' => 'auth.front',
	'as' => 'front.orders.update',
]);
Route::post('/orders/{id}/updatePost', [
	'uses' => 'OrderController@updatePost',
	'middleware' => 'auth.front',
	'as' => 'front.orders.updatePost',
]);
Route::post('/orders/{id}/confirm', [
	'uses' => 'OrderController@confirm',
	'middleware' => 'auth.front',
	'as' => 'front.orders.confirm',
]);
Route::post('/orders/confirm', [
	'uses' => 'OrderController@multiConfirm',
	'middleware' => 'auth.front',
	'as' => 'front.orders.multiConfirm',
]);

Route::get('/orders/{status}', [
	'uses' => 'OrderController@frontIndex',
	'middleware' => 'auth.front',
	'as' => 'front.orders.index',
]);

Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function () {

	Route::get('/', [
		'uses' => 'AdminController@index',
		'as' => 'admin.index',
	]);
	Route::get('/settings', [
		'uses' => 'AdminController@settings',
		'as' => 'admin.settings',
	]);
	Route::post('/settingsPost', [
		'uses' => 'AdminController@settingsPost',
		'as' => 'admin.settingsPost',
	]);

	Route::group(['prefix' => 'banners'], function () {

		Route::get('/', [
			'uses' => 'BannerController@index',
			'as' => 'admin.banners.index',
		]);
		Route::get('/add', [
			'uses' => 'BannerController@add',
			'as' => 'admin.banners.add',
		]);
		Route::post('/add', [
			'uses' => 'BannerController@addPost',
			'as' => 'admin.banners.addPost',
		]);
		Route::get('/update/{id}', [
			'uses' => 'BannerController@update',
			'as' => 'admin.banners.update',
		]);
		Route::post('/update/{id}', [
			'uses' => 'BannerController@updatePost',
			'as' => 'admin.banners.updatePost',
		]);
	});

	Route::group(['prefix' => 'collections'], function () {

		Route::get('/', [
			'uses' => 'CollectionController@index',
			'as' => 'admin.collections.index',
		]);
		Route::get('/add', [
			'uses' => 'CollectionController@add',
			'as' => 'admin.collections.add',
		]);
		Route::post('/add', [
			'uses' => 'CollectionController@addPost',
			'as' => 'admin.collections.addPost',
		]);
		Route::get('/update/{id}', [
			'uses' => 'CollectionController@update',
			'as' => 'admin.collections.update',
		]);
		Route::post('/update/{id}', [
			'uses' => 'CollectionController@updatePost',
			'as' => 'admin.collections.updatePost',
		]);
	});

	Route::group(['prefix' => 'articles'], function () {
		Route::get('/', [
			'uses' => 'ArticleController@index',
			'as' => 'admin.articles.index',
		]);
		Route::get('/add', [
			'uses' => 'ArticleController@add',
			'as' => 'admin.articles.add',
		]);
		Route::post('/add', [
			'uses' => 'ArticleController@addPost',
			'as' => 'admin.articles.addPost',
		]);
		Route::get('/update/{id}', [
			'uses' => 'ArticleController@update',
			'as' => 'admin.articles.update',
		]);
		Route::post('/update/{id}', [
			'uses' => 'ArticleController@updatePost',
			'as' => 'admin.articles.updatePost',
		]);
	});

	Route::group(['prefix' => 'pages'], function () {
		Route::get('/', [
			'uses' => 'PageController@index',
			'as' => 'admin.pages.index',
		]);
		Route::get('/add', [
			'uses' => 'PageController@add',
			'as' => 'admin.pages.add',
		]);
		Route::post('/add', [
			'uses' => 'PageController@addPost',
			'as' => 'admin.pages.addPost',
		]);
		Route::get('/update/{id}', [
			'uses' => 'PageController@update',
			'as' => 'admin.pages.update',
		]);
		Route::post('/update/{id}', [
			'uses' => 'PageController@updatePost',
			'as' => 'admin.pages.updatePost',
		]);
	});

	Route::group(['prefix' => 'navigations'], function () {
		Route::get('/', [
			'uses' => 'NavigationController@index',
			'as' => 'admin.navigations.index',
		]);
		Route::get('/add', [
			'uses' => 'NavigationController@add',
			'as' => 'admin.navigations.add',
		]);
		Route::post('/add', [
			'uses' => 'NavigationController@addPost',
			'as' => 'admin.navigations.addPost',
		]);
		Route::get('/update/{id}', [
			'uses' => 'NavigationController@update',
			'as' => 'admin.navigations.update',
		]);
		Route::post('/update/{id}', [
			'uses' => 'NavigationController@updatePost',
			'as' => 'admin.navigations.updatePost',
		]);
	});

	Route::group(['prefix' => 'products'], function () {
		Route::get('/', [
			'uses' => 'ProductController@index',
			'as' => 'admin.products.index',
		]);
		Route::get('/add', [
			'uses' => 'ProductController@add',
			'as' => 'admin.products.add',
		]);
		Route::post('/add', [
			'uses' => 'ProductController@addPost',
			'as' => 'admin.products.addPost',
		]);
		Route::get('/update/{id}', [
			'uses' => 'ProductController@update',
			'as' => 'admin.products.update',
		]);
		Route::post('/update/{id}', [
			'uses' => 'ProductController@updatePost',
			'as' => 'admin.products.updatePost',
		]);
	});

	Route::group(['prefix' => 'users'], function () {
		Route::get('/', [
			'uses' => 'UserController@index',
			'as' => 'admin.users.index',
		]);
		Route::get('/add', [
			'uses' => 'UserController@add',
			'as' => 'admin.users.add',
		]);
		Route::post('/add', [
			'uses' => 'UserController@addPost',
			'as' => 'admin.users.addPost',
		]);
		Route::get('/update/{id}', [
			'uses' => 'UserController@update',
			'as' => 'admin.users.update',
		]);
		Route::post('/update/{id}', [
			'uses' => 'UserController@updatePost',
			'as' => 'admin.users.updatePost',
		]);
	});

	Route::group(['prefix' => 'orders'], function () {
		Route::get('/', [
			'uses' => 'OrderController@index',
			'as' => 'admin.orders.index',
		]);
		Route::get('/update/{id}', [
			'uses' => 'OrderController@update',
			'as' => 'admin.orders.update',
		]);
		Route::post('/update/{id}', [
			'uses' => 'OrderController@updatePost',
			'as' => 'admin.orders.updatePost',
		]);
		Route::get('export/all', [
			'uses' => 'OrderController@exportAll',
			'as' => 'admin.orders.exportAll',
		]);
		Route::get('export', [
			'uses' => 'OrderController@export',
			'as' => 'admin.orders.export',
		]);
	});

	Route::group(['prefix' => 'images'], function () {

		Route::post('/upload', [
			'uses' => 'ImageController@upload',
			'as' => 'admin.images.upload',
		]);
		Route::get('/{id}', [
			'uses' => 'ImageController@getImage',
		]);
		Route::post('/multiUpload', [
			'uses' => 'ImageController@multiUpload',
			'as' => 'admin.images.multiUpload',
		]);

	});

});