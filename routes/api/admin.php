<?php
$api = app('Dingo\Api\Routing\Router');

// 首页 轮播图

$api->group(['prefix' => 'banner'],
    function ($api) {
        $api->get('/', ['uses' => 'BannerController@index', 'as' => "api.banners.get"]);
        $api->get('/{id}', 'BannerController@show');
        $api->post('/', 'BannerController@create');
        $api->put('/{id}', 'BannerController@update');
        $api->delete('/{id}', 'BannerController@destroy');
        $api->post('/sort', 'BannerController@sort');
    }
);

// 首页 关键字

$api->group(['prefix' => 'keyword'],
    function ($api) {
        $api->get('/', 'KeywordController@index');
        $api->get('/{id}', 'KeywordController@show');
        $api->post('/', 'KeywordController@create');
        $api->put('/{id}', 'KeywordController@update');
        $api->delete('/{id}', 'KeywordController@destroy');
        $api->post('/sort', 'KeywordController@sort');
    }
);

$api->group(['prefix' => 'collections'], 
    function ($api) {
        $api->get('/', ['as' => 'api.collections.get', 'uses' => 'CollectionController@get']);
        $api->delete('/{id}', 'CollectionController@destroy');
        $api->get('/getAll', 'CollectionController@getAll');
    }
);

$api->group(['prefix' => 'pages'], 
    function ($api) {
        $api->get('/', ['as' => 'api.pages.get', 'uses' => 'PageController@get']);
        $api->delete('/{id}', 'PageController@destroy');
        $api->get('/getAll', 'PageController@getAll');
    }
);

$api->group(['prefix' => 'orders'], 
    function ($api) {
        $api->get('/', ['as' => 'api.orders.get', 'uses' => 'OrderController@get']);
        $api->delete('/{id}', 'OrderController@destroy');
        $api->get('/getAll', 'OrderController@getAll');
    }
);

$api->group(['prefix' => 'products'], 
    function ($api) {
        $api->get('/', ['as' => 'api.products.get', 'uses' => 'ProductController@get']);
        $api->delete('/{id}', 'ProductController@destroy');
        $api->get('/getAll', 'ProductController@getAll');
    }
);

$api->group(['prefix' => 'users'], 
    function ($api) {
        $api->get('/', ['as' => 'api.users.get', 'uses' => 'UserController@get']);
        $api->delete('/{id}', 'UserController@destroy');
        $api->get('/getAll', 'UserController@getAll');
    }
);

$api->group(['prefix' => 'navigations'], 
    function ($api) {
        $api->get('/', ['as' => 'api.navigations.get', 'uses' => 'NavigationController@get']);
        $api->delete('/{id}', 'NavigationController@destroy');
        $api->get('/{id}/getSubNavigations', 'NavigationController@getSubNavigations');
    }
);

$api->group(['prefix' => 'templates'],
    function ($api) {
        $api->get('/', ['as' => 'api.templates.get', 'uses' => 'TemplateController@get']);
    }
);
// 首页 经营范围

$api->group(['prefix' => 'scope'],
    function ($api) {
        $api->get('/', 'BusinessScopeController@index');
        $api->get('/{id}', 'BusinessScopeController@show');
        $api->post('/', 'BusinessScopeController@create');
        $api->put('/{id}', 'BusinessScopeController@update');
        $api->delete('/{id}', 'BusinessScopeController@destroy');
        $api->post('/sort', 'BusinessScopeController@sort');
    }
);

// 关于细刻 奖项和成果

$api->group(['prefix' => 'award'],
    function ($api) {
        $api->get('/', 'AwardController@index');
        $api->get('/{id}', 'AwardController@show');
        $api->post('/', 'AwardController@create');
        $api->put('/{id}', 'AwardController@update');
        $api->delete('/{id}', 'AwardController@destroy');
        $api->post('/sort', 'AwardController@sort');
    }
);

// 关于细刻 发展进程

$api->group(['prefix' => 'process'],
    function ($api) {
        $api->get('/', 'DevelopmentProcessController@index');
        $api->get('/{id}', 'DevelopmentProcessController@show');
        $api->post('/', 'DevelopmentProcessController@create');
        $api->put('/{id}', 'DevelopmentProcessController@update');
        $api->delete('/{id}', 'DevelopmentProcessController@destroy');
        $api->post('/sort', 'DevelopmentProcessController@sort');
    }
);

// 多彩细刻 文章管理
$api->group(['prefix' => 'article'],
    function ($api) {
        $api->get('/', ['as' => 'api.article.get', 'uses' => 'ArticleController@index']);
        $api->get('/{id}', 'ArticleController@show');
        $api->post('/', 'ArticleController@create');
        $api->put('/{id}', 'ArticleController@update');
        $api->delete('/{id}', 'ArticleController@destroy');
        $api->post('/{id}/status/{status}', 'ArticleController@status');
    }
);

// 加入我们 人才理念 & 员工福利 & 职业晋升途径

$api->group(['prefix' => 'person'],
    function ($api) {
        $api->get('/', 'PersonController@index');
        $api->get('/{id}', 'PersonController@show');
        $api->post('/', 'PersonController@create');
        $api->put('/{id}', 'PersonController@update');
        $api->delete('/{id}', 'PersonController@destroy');
    }
);

// 加入我们 招聘岗位
$api->group(['prefix' => 'recruit'],
    function ($api) {
        $api->get('/', 'RecruitController@index');
        $api->get('/{id}', 'RecruitController@show');
        $api->post('/', 'RecruitController@create');
        $api->put('/{id}', 'RecruitController@update');
        $api->delete('/{id}', 'RecruitController@destroy');
    }
);
