<?php

$api = app('Dingo\Api\Routing\Router');

$api->get('/banner', 'BannerController@index');
$api->get('/scope', 'BusinessScopeController@index');
$api->get('/article/home', 'ArticleController@home');

$api->get('/process', 'DevelopmentProcessController@index');
$api->get('/award', 'AwardController@index');

$api->get('/article', 'ArticleController@index');
$api->get('/article/{id}', 'ArticleController@show');
$api->put('/article/{id}/good', 'ArticleController@good');

$api->get('/products', 'ProductController@index');

$api->get('/person', 'PeopleController@index');
$api->get('/recruit', 'RecruitController@index');
$api->post('/recruit/{recruit_id}', 'RecruitController@delivery');

$api->get('/{user_id}/orders/{status}/get', [
	'as' => 'front.orders.getCurrentUserOrders',
	'uses' => 'OrderController@get',
]);
