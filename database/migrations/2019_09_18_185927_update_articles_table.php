<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('articles', function (Blueprint $table) {
            $table->dropForeign('articles_image_id_foreign');
            $table->dropColumn('image_id');
        });

        Schema::table('articles', function (Blueprint $table) {
            $table->unsignedInteger('template_id')->nullable()->change();
            $table->unsignedInteger('pc_image_id')->nullable()->after('description');
            $table->unsignedInteger('app_image_id')->nullable()->after('pc_image_id');
            $table->foreign('pc_image_id')->references('id')->on('images');
            $table->foreign('app_image_id')->references('id')->on('images');
            $table->unsignedTinyInteger('status')->default(1)->after('description');
            $table->unsignedInteger('good_count')->after('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('articles', function (Blueprint $table) {
            $table->unsignedInteger('image_id');
            $table->foreign('image_id')->references('id')->on('images');
        });

        Schema::table('articles', function (Blueprint $table) {
            $table->dropForeign('articles_pc_image_id_foreign');
            $table->dropForeign('articles_app_image_id_foreign');
            $table->dropColumn(['pc_image_id', 'app_image_id', 'status', 'good_count']);
        });

    }
}
