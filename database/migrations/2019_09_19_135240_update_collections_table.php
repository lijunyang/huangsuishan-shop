<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCollectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('collections', function (Blueprint $table) {
            $table->dropForeign('collections_image_id_foreign');
            $table->dropColumn('image_id');
            $table->unsignedInteger('pc_image_id')->nullable()->after('description');
            $table->unsignedInteger('app_image_id')->nullable()->after('pc_image_id');
            $table->foreign('pc_image_id')->references('id')->on('images');
            $table->foreign('app_image_id')->references('id')->on('images');
            $table->unsignedInteger('template_id')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('collections', function (Blueprint $table) {
            $table->unsignedInteger('image_id');
            $table->foreign('image_id')->references('id')->on('images');
            $table->dropForeign('collections_pc_image_id_foreign');
            $table->dropForeign('collections_app_image_id_foreign');
            $table->dropColumn(['pc_image_id', 'app_image_id']);
        });
    }
}
