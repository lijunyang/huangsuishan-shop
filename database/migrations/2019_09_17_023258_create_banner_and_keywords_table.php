<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBannerAndKeywordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banner_and_keywords', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('type')->default(1);
            $table->unsignedInteger('pc_image_id');
            $table->unsignedInteger('app_image_id');
            $table->unsignedInteger('sort')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banner_and_keywords');
    }
}
