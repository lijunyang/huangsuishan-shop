<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecruitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recruits', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedSmallInteger('area_id');
            $table->string('position');
            $table->string('employee_email');
            $table->unsignedTinyInteger('status');
            $table->unsignedSmallInteger('deliver_number')->default(0);
            $table->string('duty');
            $table->string('require');
            $table->string('address');
            $table->string('phone');
            $table->string('email');
            $table->string('contact_people');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recruits');
    }
}
