<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->text('description');
            $table->unsignedInteger('pc_image_id')->nullable();
            $table->unsignedInteger('app_image_id')->nullable();
            $table->foreign('pc_image_id')->references('id')->on('images');
            $table->foreign('app_image_id')->references('id')->on('images');
            $table->unsignedInteger('template_id')->nullable();
            $table->foreign('template_id')->references('id')->on('templates');
            $table->text('content');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pages');
    }
}
