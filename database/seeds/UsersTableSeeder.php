<?php

use Illuminate\Database\Seeder;
use App\Models\Users;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (Users::where("name", "admin")->first() === null) {
            DB::table('users')->insert([
                'name' => "admin",
                'email' => "test@gmail.com",
                'password' => bcrypt("123456"),
                'role' => 'role_admin',
                'telephone' => '123456',
                'province' => '2',
                'city' => '3',
                'district' => '4',
                'detail_address' => '5',
                'zip_code' => '6',
                'created_at' => date("Y-m-d H:i:s" ,time()),
                'updated_at' => date("Y-m-d H:i:s" ,time()),
            ]);
        }
    }
}
