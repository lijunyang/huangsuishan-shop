<?php

use Illuminate\Database\Seeder;
use App\Models\Settings;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (Settings::findByName("site_name") === false){
            DB::table('settings')->insert([
                'name' => "site_name",
                'value' => "",
            ]);
        }
        if (Settings::findByName("logo") === false){
            DB::table('settings')->insert([
                'name' => "logo",
                'value' => "",
            ]);
        }
        if (Settings::findByName("about_us") === false){
            DB::table('settings')->insert([
                'name' => "about_us",
                'value' => "",
            ]);
        }
        if (Settings::findByName("collection_first") === false){
            DB::table('settings')->insert([
                'name' => "collection_first",
                'value' => "",
            ]);
        }
        if (Settings::findByName("collection_second") === false){
            DB::table('settings')->insert([
                'name' => "collection_second",
                'value' => "",
            ]);
        }
        if (Settings::findByName("video") === false){
            DB::table('settings')->insert([
                'name' => "video",
                'value' => "",
            ]);
        }
        if (Settings::findByName("footer_description") === false){
            DB::table('settings')->insert([
                'name' => "footer_description",
                'value' => "",
            ]);
        }
    }
}
