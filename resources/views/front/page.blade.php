<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <title>{{ $site_name }}</title>
    <meta name="Keywords" content="{$res.keywords}"/>
    <meta name="Description" content="{$res.description}"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="stylesheet" type="text/css" href="/front/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/front/css/style.css">
    <link rel="stylesheet" type="text/css" href="/front/css/zzsc.css">
    <link href="http://cdn.bootcss.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/front/css/menu-css.css">
    
    <script src="/front/js/jquery.min.js"></script>
    <script src="http://cdn.bootcss.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="/front/js/modernizr.custom.js"></script>
    <script src="/front/js/scroll.js"></script>


</head>
<body>

@include ('front.header')

<!-- 导航轮播结束 -->
<!-- about开始 -->
<div class="container about">
    <img src="/images/{{ $current_page->pc_image_id }}" alt="" class="img-responsive" />
    <h3>{{ $current_page->title }}</h3>
    <div style="text-align: justify;word-break: break-all;">
        {!! $current_page->content !!}
    </div>
</div>
<!-- about结束 -->

@include ('front.footer')

<script src="/front/js/cbpHorizontalSlideOutMenu.min.js"></script>

<link href="/front/css/jquery.exzoom.css" rel="stylesheet">
<script src="/front/js/jquery.exzoom.js"></script>

<script>
    var menu = new cbpHorizontalSlideOutMenu( document.getElementById( 'cbp-hsmenu-wrapper' ) );
</script>
<script type=text/javascript> $(document).ready(function(){ $("#firstpane .menu_body:eq(0)").show(); $("#firstpanep.menu_head").click(function(){
    $(this).addClass("current").next("div.menu_body").slideToggle(300).siblings("div.menu_body").slideUp("slow");
    $(this).siblings().removeClass("current"); }); $("#secondpane .menu_body:eq(0)").show(); $("#secondpanep.menu_head").mouseover(function(){
$(this).addClass("current").next("div.menu_body").slideDown(500).siblings("div.menu_body").slideUp("slow");
$(this).siblings().removeClass("current"); }); }); </script> <script type="text/javascript">
    $("#exzoom").exzoom({
        autoPlay: false,
    });//方法调用，务必在加载完后执行
</script>
</body>
</html>
