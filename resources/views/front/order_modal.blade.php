<!-- 模态框（Modal） -->
<div class="modal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    Add to order
                </h4>
            </div>

            <div class="modal-body">
                <form class="form-horizontal" role="form" data-toggle="validator" role="form" action="{{ route('front.orders.addPost') }}" enctype="multipart/form-data" method="post">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Name</label>
                        <div class="col-sm-10">
                            <input type="text" name="name" class="form-control" placeholder="Name" required @if (Auth::check()) value="{{ Auth::user()->name }}" @endif disabled>
                            <div class="help-block  with-errors"></div>
                        </div>
                    </div>
                    <!-- <div class="form-group">
                        <label class="col-sm-2 control-label">Phone Number</label>
                        <div class="col-sm-10">
                            <input type="text" name="telephone" class="form-control" placeholder="Phone Number" required @if (Auth::check()) value="{{ Auth::user()->telephone }}" @endif>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Province</label>
                        <div class="col-sm-10">
                            <input type="text" name="province" class="form-control" placeholder="Province" required @if (Auth::check()) value="{{ Auth::user()->province }}" @endif>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Prefecture</label>
                        <div class="col-sm-10">
                            <input type="text" name="city" class="form-control" placeholder="Prefecture" required @if (Auth::check()) value="{{ Auth::user()->city }}" @endif>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Country</label>
                        <div class="col-sm-10">
                            <input type="text" name="district" class="form-control" placeholder="Country" required @if (Auth::check()) value="{{ Auth::user()->district }}" @endif>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Address</label>
                        <div class="col-sm-10">
                            <input type="text" name="detail_address" class="form-control" placeholder="Address" required @if (Auth::check()) value="{{ Auth::user()->detail_address }}" @endif>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">E-Mail</label>
                        <div class="col-sm-10">
                            <input type="email" name="email" class="form-control" placeholder="E-Mail" required @if (Auth::check()) value="{{ Auth::user()->email }}" @endif>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Zip Code</label>
                        <div class="col-sm-10">
                            <input type="text" name="zip_code" class="form-control" placeholder="Zip Code" @if (Auth::check()) value="{{ Auth::user()->zip_code }}" @endif>
                        </div>
                    </div> -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Quantity</label>
                        <div class="col-sm-10">
                            <input type="text" name="purchase_number" value=""  class="form-control" placeholder="Quantity" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Remark</label>
                        <div class="col-sm-10">
                            <textarea name="comment" style="width:100%;height: 150px;"></textarea>
                        </div>
                    </div>
                    <input type="hidden" name="product_id" value="{{ $current_product->id }}">
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close
                        </button>
                        <button type="submit" class="btn btn-primary">
                            Add to order
                        </button>
                    </div>
                </form>
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal -->
</div>