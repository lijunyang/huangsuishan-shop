<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <title>{{ $site_name }}</title>
    <meta name="Keywords" content="{$res.keywords}"/>
    <meta name="Description" content="{$res.description}"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="stylesheet" type="text/css" href="/front/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/front/css/style.css">
    <link rel="stylesheet" type="text/css" href="/front/css/zzsc.css">
    <link href="http://cdn.bootcss.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/front/css/menu-css.css">
    <script src="/front/js/jquery.min.js"></script>
    <script src="http://cdn.bootcss.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="/front/js/modernizr.custom.js"></script>
    <script src="/front/js/scroll.js"></script>


</head>
<body>

@include ('front.header')

<!-- 大图开始 -->
<div class="shop-header-banner">
    <img src="/front/images/shop-header-banner.webp.jpg" alt="" class="img-responsive" />
</div>
<!-- 大图结束 -->
<!-- 大主题开始开始 -->
<div class="container">
    <div class="shop-header-con we">
        <h2>NEWLY ADDED</h2>
    </div>
</div>
<!-- 大主题开始结束 -->
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <ul class="product-list">
                @foreach ($products as $product)
                <li>
                    <div><a href="/products/{{ $product->id }}"><img src="/images/{{ explode(',', $product->image_ids)[0] }}" alt="" /></a></div>
                    <div>
                        <h4><a href="">{{ $product->title }}</a></h4>
                        <p class="hcode"><a href="">Material: {{ $product->type }}</a></p>
                        <p class="price"><a href="">Price:{{ $product->price }}</a></p>
                    </div>
                </li>
                @endforeach
            </ul>
            {{ $products->links() }}
        </div>
    </div>
</div>

@include ('front.footer')

<script src="/front/js/cbpHorizontalSlideOutMenu.min.js"></script>


<script>
    var menu = new cbpHorizontalSlideOutMenu( document.getElementById( 'cbp-hsmenu-wrapper' ) );
</script>
<script type=text/javascript>
    $(document).ready(function(){
        $("#firstpane .menu_body:eq(0)").show();
        $("#firstpane p.menu_head").click(function(){
            $(this).addClass("current").next("div.menu_body").slideToggle(300).siblings("div.menu_body").slideUp("slow");
            $(this).siblings().removeClass("current");
        });
        $("#secondpane .menu_body:eq(0)").show();
        $("#secondpane p.menu_head").mouseover(function(){
            $(this).addClass("current").next("div.menu_body").slideDown(500).siblings("div.menu_body").slideUp("slow");
            $(this).siblings().removeClass("current");
        });

    });
</script>
</body>
</html>