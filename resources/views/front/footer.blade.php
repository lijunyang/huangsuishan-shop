<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="flog"><img src="/front/images/ft_logo.png" alt="" /></div>
                <p class="store-info">{{ $footer_description }}</p>
                <div class="social-links">
                    <a href=""><img src="" alt="" /><i class="fa fa-facebook"></i></a>
                    <a href=""><img src="" alt="" /><i class="fa fa-twitter"></i></a>
                    <a href=""><img src="" alt="" /><i class="fa fa-linkedin"></i></a>
                    <a href=""><img src="" alt="" /><i class="fa fa-google-plus"></i></a>
                </div>
            </div>
            @foreach ($navigations as $navigation)
                @if ($navigation->parent_id === 0)
                <div class="col-md-3 footer-information">
                    <h5>{{ $navigation->title }}</h5>
                    @foreach ($navigations as $sub_navigation)
                        @if ($sub_navigation->parent_id === $navigation->id)
                            <a href="/navigations/{{ $sub_navigation->id }}">{{ $sub_navigation->title }}</a>
                        @endif
                    @endforeach
                </div>
                @endif
            @endforeach
        </div>
    </div>
</footer>
<div class="text-center footer-bottom">
    <div>Copyright © 2019 </div>
</div>