<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <title>{{ $site_name }}</title>
    <meta name="Keywords" content="{$res.keywords}"/>
    <meta name="Description" content="{$res.description}"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="stylesheet" type="text/css" href="/front/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/front/css/style.css">
    <link rel="stylesheet" type="text/css" href="/front/css/zzsc.css">
    <link href="http://cdn.bootcss.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/front/css/menu-css.css">
    <script src="/front/js/jquery.min.js"></script>
    <script src="http://cdn.bootcss.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="/front/js/modernizr.custom.js"></script>
    <script src="/front/js/scroll.js"></script>
    <script type="text/javascript" src="/front/js/collections.js"></script>

</head>
<body>

@include ('front.header')

<div class="shop-header-banner">
    <img src="/front/images/shop-header-banner.webp.jpg" alt="" class="img-responsive collection-image" />
</div>

<div class="container">
    <div class="shop-header-con we">
        <h2>{{ $current_collection->title }}</h2>
        <p>{{ $current_collection->description }}</p>
        <div class="text-center story"><a href="/navigations/{{ $current_navigation->id }}"> {{ $current_collection->title }}</a></div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-3 panel-group">
            @include('front.collection-left-panel-group')
        </div>
        <div class="col-md-9">
            <ul class="product-list">
                @foreach($collection_products as $collection_product)
                    <li>
                        <div><a href="/products/{{$collection_product->product->id}}"><img src="/images/{{explode(',' ,$collection_product->product->image_ids)[0]}}" alt="" /></a></div>
                        <div>
                            <h4><a href="">{{$collection_product->product->title}}</a></h4>
                            <p class="hcode"><a href="">Material: {{$collection_product->product->type}}</a></p>
                            <p class="price"><a href="">Price:{{$collection_product->product->price}}</a></p>
                        </div>
                    </li>
                @endforeach
            </ul>
            <nav aria-label="...">
              <ul class="pager">
                <li class="previous"><a href="{{ $collection_products->previousPageUrl() }}"><span aria-hidden="true">&larr;</span> Pre</a></li>
                <li><span class="pagination-info text-muted">
                    Page:
                        <input id="page" class='form-control pagination-page-input' value={{$collection_products->currentPage()}} />
                        <button class="btn btn-default" id="change-page">Go</button>
                        <span class='pagination-page-total'>
                            Total: {{$collection_products->total()}}
                        </span>
                        PerPage:
                        <select class="form-control pagination-page-select" id="per-page" autocomplete="false">
                            <option value="9" @if($collection_products->perPage() == "9") selected @endif>9</option>
                            <option value="18" @if($collection_products->perPage() == "18") selected @endif>18</option>
                            <option value="24" @if($collection_products->perPage() == "24") selected @endif>24</option>
                            <option value="36" @if($collection_products->perPage() == "36") selected @endif>36</option>
                        </select>
                </span></li>
                <li class="next"><a href="{{$collection_products->nextPageUrl()}}">Next <span aria-hidden="true">&rarr;</span></a></li>
              </ul>
            </nav>

        </div>
    </div>
</div>
<script type="html/template" id="product-template">
    <li>
        <div><a href="/products/{* id *}"><img src="/images/{* image_id *}" alt="" /></a></div>
        <div>
            <h4><a href="">{* title *}</a></h4>
            <p class="hcode"><a href="">Material: {* type *}</a></p>
            <p class="price"><a href="">Price:{* price *}</a></p>
        </div>
    </li>
</script>

@include ('front.footer')

<script src="/front/js/cbpHorizontalSlideOutMenu.min.js"></script>


<script>
    var menu = new cbpHorizontalSlideOutMenu( document.getElementById( 'cbp-hsmenu-wrapper' ) );
</script>
<script type=text/javascript>
    let collections_id = ["{{ $current_navigation->id }}-{{ $current_collection->id }}"];
    let filter_collections = [];
    $(document).ready(function(){
        $("#firstpane .menu_body:eq(0)").show();
        $("#firstpane p.menu_head").click(function(){
            $(this).addClass("current").next("div.menu_body").slideToggle(300).siblings("div.menu_body").slideUp("slow");
            $(this).siblings().removeClass("current");
        });
        $("#secondpane .menu_body:eq(0)").show();
        $("#secondpane p.menu_head").mouseover(function(){
            $(this).addClass("current").next("div.menu_body").slideDown(500).siblings("div.menu_body").slideUp("slow");
            $(this).siblings().removeClass("current");
        });

    });
</script>
</body>
</html>