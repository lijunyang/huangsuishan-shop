@foreach ($allow_filter_navigations as $navigation)
    @if ($navigation->id !== $top_navigation->id)
        <div class="panel panel-default search-collections">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" href="#navigation-{{ $navigation->id }}" class="collapseWill">
                        {{ $navigation->title }} <span class="pull-left"> <i class="fa fa-caret-right"></i></span>
                    </a>
                </h4>
            </div>
            <div id="navigation-{{ $navigation->id }}" class="panel-collapse collapse in">
                <div class="panel-body smoothscroll maxheight300">
                    @foreach ($navigation->sonNavigations as $sonNavigation)
                    <div class="block-element hitlist-facet orange-hover ">
                        <label>
                            <input type="checkbox" name="" value="{{ $sonNavigation->id }}-{{ $sonNavigation->navigable->id }}" data-facet-cat="Newly Added" class="facet-item" autocomplete="off">
                            <span class="facet-text">{{ $sonNavigation->navigable->title }}</span>
                        </label>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    @endif
@endforeach