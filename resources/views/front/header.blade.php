<link rel="stylesheet" type="text/css" href="/css/common.css">
<div class="header-prefix">
    <div class="account-info clearfix">
        @if(Auth::check())
            <div class="pull-right text-danger"><a href="@if(Auth::user()->role === 'role_admin')/admin/orders @else /orders/0 @endif">Orders</a></div>
        @endif
        <div class="pull-right text-danger @if(Auth::check()) log-in @endif"><a href="/register">Sign Up</a></div>
        @if(Auth::check())
            <div class="pull-right log-in"><a href="/logout">Log Out</a></div>
            <div class="pull-right log-in">{{ Auth::user()->name }}</div>
        @else
            <div class="pull-right log-in"><a href="/front/login">Log In</a></div>
        @endif
    </div>
</div>
<!-- 头部开始 -->
<div class="container logo">
    <div class="row">
        <div class="col-md-3"><img src="/front/images/hlogo.png" alt="" class="img-responsive"> <a href="javascript:;" id="menu" class="glyphicon glyphicon-th"></a></div>

        <div class="col-md-6 col-md-offset-3">
            <input type="text" class="form-control" name="key_words" placeholder="search">
            <a id="search" class="search-btn btn btn-danger"></a>
        </div>
    </div>
</div>

<!-- 导航栏开始 -->
<div class="main">
    <nav class="cbp-hsmenu-wrapper text-center" id="cbp-hsmenu-wrapper">
        <div class="cbp-hsinner">
            <ul class="cbp-hsmenu">
                <li><a href="{{ route('front.recentlyProducts') }}">NEWLY ADDED</a></li>
                <li><a href="/">HOME</a></li>
                @foreach ($navigations as $navigation)
                    @if ($navigation->parent_id === 0)
                        <li>
                            <a href="/navigations/{{ $navigation->id }}">{{ $navigation->title }}</a>
                            <ul class="cbp-hssubmenu">
                                @foreach ($navigation->sonNavigations as $sub_navigation)
                                    @if ($loop->index <= 10)
                                        <li><a href="/navigations/{{ $sub_navigation->id }}"><img src="/images/{{ $sub_navigation->navigable->pc_image_id }}" alt="{{ $sub_navigation->title }}"/><span>{{ $sub_navigation->title }}</span></a></li>
                                    @endif
                                @endforeach

                                @if ($navigation->sonNavigations->count() >= 11)
                                <li>
                                    <div class="view-all">
                                        <a href="/navigations/{{ $navigation->id }}/all"><span>View All</span></a>
                                    </div>
                                </li>
                                @endif
                            </ul>
                        </li>
                    @endif
                @endforeach

            </ul>
        </div>
    </nav>
</div>
<script>
    $("#menu").click(function(){
        if($('#cbp-hsmenu-wrapper').is(':hidden')){
            $('#cbp-hsmenu-wrapper').slideDown();
        }else{
            $('#cbp-hsmenu-wrapper').slideUp();
        }
    });
</script>
<!-- 导航栏结束 -->
<!-- 导航轮播开始 -->
<div id="featureContainer">
    <div id="feature">
        <div id="block">
            <div id="botton-scroll">
                <ul class="featureUL">
                    @foreach ($products_navigations as $navigation)
                    <li class="featureBox">
                        <div class="box">
                            <a href="/navigations/{{ $navigation->id }}">
                                <img alt="{{ $navigation->title }}" src="/images/{{ $navigation->navigable->pc_image_id }}" width="100%" class="img-responsive">
                            </a>
                        </div>
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>
        <a class="prev" id="btnl" href="javascript:void(0);">Previous</a><a class="scroll-next" id="btnr" href="javascript:void(0);">Next</a>
    </div>
</div>
<script type="text/javascript">
    //滚动元素id，左切换按钮，右切换按钮，切换元素个数id,滚动方式，切换方向，是否自动滚动，滚动距离，滚动时间，滚动间隔，显示个数
    LbMove('botton-scroll','btnl','btnr','',true,'left',true,100,1000,2000,8);
    $("#search").on("click", function(){
        $key_words = $("input[name='key_words']").val();
        if ($key_words !== null && $key_words !== "") {
            window.location.href = "/search/" + $key_words;
        }
    });
</script>
<!-- 导航轮播结束 -->