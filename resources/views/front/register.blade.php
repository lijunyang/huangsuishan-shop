<!DOCTYPE html>
<html>
<head>
    <title>Register</title>
    <link rel="stylesheet" type="text/css" href="/front/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/css/register.css">
</head>
<body>
    <div class="register-container">
        <div class="panel panel-default">
            <div class="panel-heading">
                Register
            </div>
            <div class="panel-body">
                <form class="form-horizontal" method="POST" data-toggle="validator" role="form" action="{{ route('front.userAdd') }}">
                    <div class="form-group">
                        <label for="name" class="col-md-2 control-label">Username</label>
                        <div class="col-md-8">
                            <input class="form-control" type="text" name="name" placeholder="Username" data-error="no less than 6 characters" data-minlength="6" required  data-required-error="please enter this item">
                            <div class="help-block  with-errors"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email" class="col-md-2 control-label">Email</label>
                        <div class="col-md-8">
                            <input class="form-control" type="email" data-error="please enter valid email" name="email" placeholder="Email" required data-required-error="please enter this item">
                            <div class="help-block  with-errors"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="password" class="col-md-2 control-label">Passwords</label>
                        <div class="col-md-8">
                            <input class="form-control" data-minlength="6" type="password" name="password" placeholder="Passwords" id="inputPassword" data-error="no less than 6 characters" required data-required-error="please enter this item">
                            <div class="help-block  with-errors"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="confirm-password" class="col-md-2 control-label">Passwords Confirm</label>
                        <div class="col-md-8">
                            <input class="form-control" type="password" name="confirm-password" data-match="#inputPassword" data-match-error="Passwords does not match" placeholder="Passwords Confirm" required data-required-error="please enter this item">
                            <div class="help-block  with-errors"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="telephone" class="col-md-2 control-label">Telephone</label>
                        <div class="col-md-8">
                            <input class="form-control" type="text" name="telephone" placeholder="Telephone" required data-required-error="please enter this item">
                            <div class="help-block  with-errors"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="province" class="col-md-2 control-label">Province</label>
                        <div class="col-md-8">
                            <input class="form-control" type="text" name="province" placeholder="Province" required data-required-error="please enter this item">
                            <div class="help-block  with-errors"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="city" class="col-md-2 control-label">Prefecture</label>
                        <div class="col-md-8">
                            <input class="form-control" type="text" name="city" placeholder="Prefecture" required data-required-error="please enter this item">
                            <div class="help-block  with-errors"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="district" class="col-md-2 control-label">Country</label>
                        <div class="col-md-8">
                            <input class="form-control" type="text" name="district" placeholder="Country" required data-required-error="please enter this item">
                            <div class="help-block  with-errors"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="detail_address" class="col-md-2 control-label">Address</label>
                        <div class="col-md-8">
                            <input class="form-control" type="text" name="detail_address" placeholder="Address" required data-required-error="please enter this item">
                            <div class="help-block  with-errors"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="zip_code" class="col-md-2 control-label">Zip Code</label>
                        <div class="col-md-8">
                            <input class="form-control" type="text" name="zip_code" placeholder="Zip Code" required data-required-error="please enter this item">
                            <div class="help-block  with-errors"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-8 col-md-offset-2">
                            <button type="submit" class="btn btn-success form-control">register</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        
    </div>
    <script src="https://cdn.bootcss.com/jquery/1.11.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.9/validator.min.js"></script>
    <script type="text/javascript" src="/js/register/index.js"></script>
</body>
</html>