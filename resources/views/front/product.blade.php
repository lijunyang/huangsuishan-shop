<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <title>{{ $site_name }}</title>
    <meta name="Keywords" content="{dede:field.keywords/}"/>
    <meta name="Description" content="{dede:field.description  function='html2text(@me)'/}"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="stylesheet" type="text/css" href="/front/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/front/css/style.css">
    <link rel="stylesheet" type="text/css" href="/front/css/zzsc.css">
    <link href="http://cdn.bootcss.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/front/css/menu-css.css">
    <script src="/front/js/jquery.min.js"></script>
    <script src="http://cdn.bootcss.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

    <script src="/front/js/modernizr.custom.js"></script>
    <script src="/front/js/scroll.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.9/validator.min.js"></script>
</head>
<body>
@if (session('status'))
    <div class="alert alert-success text-center">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        {{ session('status') }}
    </div>
@endif

@include ('front.header')

<!-- 商品详情开始 -->
<div class="container">
    <div class="row">
        <div class="col-md-5">
            <div class="exzoom" id="exzoom">
                <!--大图区域-->
                <div class="exzoom_img_box">
                    <ul class='exzoom_img_ul'>
                        @foreach (explode(",", $current_product->image_ids) as $image_id)
                        <li><img src="/images/{{ $image_id }}" style="max-width: 100%;" /></li>
                        @endforeach
                    </ul>
                </div>
                <!--缩略图导航-->
                <div class="exzoom_nav"></div>
                <!--控制按钮-->
                <p class="exzoom_btn">
                    <a href="javascript:void(0);" class="exzoom_prev_btn"> &lt; </a>
                    <a href="javascript:void(0);" class="exzoom_next_btn"> &gt; </a>
                </p>
            </div>
        </div>
        <div class="col-md-7">
            <h1 class="product-name">{{ $current_product->title }}</h1>
            <table class="table table-condensed product-detail">
                <tbody>
                <tr>
                    <td>Brand:</td>
                    <td class="product-info-value">{{ $current_product->brand }}</td></tr>
                <tr>
                    <td>Quantity:</td>
                    <td class="product-info-value">{{ $current_product->number }}</td></tr>
                <tr>
                    <td>Material:</td>
                    <td class="product-info-value">{{ $current_product->type }}</td></tr>
                </tbody>
            </table>
            <div class="sale"><span>sale</span></div>
            <div class="product-price">￥ {{ $current_product->price }}</div>
            <button class="btn btn-primary" id="buy">Add To Order</button>
            
        </div>
    </div>
    <div class="row">
        <div class="product-des">
            <div class="product-des-title">OVERVIEW</div>
            <div class="product-des-con">
                {!! $current_product->content !!}
            </div>
            <div class="product-des-title">You'll Love</div>
            <div>
                <ul class="product-des-lis">
                    @foreach ($related_collections_products as $related_collections_product)
                    <li>
                        <div><a href="/products/{{ $related_collections_product->product['id'] }}"><img src="/images/{{ explode(',', $related_collections_product->product['image_ids'])[0] }}" alt="" class="img-responsive" /></a></div>
                        <div>
                            <h4><a href="[field:arcurl/]">{{ $related_collections_product->product['title'] }}</a></h4>
                            <p class="hcode"><a href="">Meterial: {{ $related_collections_product->product['type'] }}</a></p>
                            <p class="price"><a href="">Price：{{ $related_collections_product->product['price'] }}</a></p>
                        </div>
                    </li>
                    @endforeach

                </ul>
            </div>
        </div>
    </div>
</div>
<!-- 商品详情结束 -->

@include ('front.order_modal')

@include ('front.footer')

<script src="/front/js/cbpHorizontalSlideOutMenu.min.js"></script>
<!-- <script src="/skin/js/slide.js"></script> -->

<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.1/js/bootstrap-select.min.js"></script> -->
<!-- <script type="text/javascript" src="/skin/js/flexible-bootstrap-carousel.js"></script> -->
<!-- <script type="text/javascript" src="/skin/js/script.js"></script> -->
<link href="/front/css/jquery.exzoom.css" rel="stylesheet">
<script src="/front/js/jquery.exzoom.js"></script>

<script>
    var menu = new cbpHorizontalSlideOutMenu( document.getElementById( 'cbp-hsmenu-wrapper' ) );
</script>
<script type=text/javascript>
    $(document).ready(function(){
        $("#firstpane .menu_body:eq(0)").show();
        $("#firstpane p.menu_head").click(function(){
            $(this).addClass("current").next("div.menu_body").slideToggle(300).siblings("div.menu_body").slideUp("slow");
            $(this).siblings().removeClass("current");
        });
        $("#secondpane .menu_body:eq(0)").show();
        $("#secondpane p.menu_head").mouseover(function(){
            $(this).addClass("current").next("div.menu_body").slideDown(500).siblings("div.menu_body").slideUp("slow");
            $(this).siblings().removeClass("current");
        });

    });
</script>
<script type="text/javascript">
    @if (Auth::check()) 
        var is_login = true;
    @else
        var is_login = false;
    @endif
    $("#buy").on("click", function(){
        if (is_login) {
            $("#myModal").modal("show");
        }else{
            window.location.href = "/login";
        }
    });
    $("#exzoom").exzoom({
        autoPlay: false,
    });//方法调用，务必在加载完后执行
</script>
</body>
</html>