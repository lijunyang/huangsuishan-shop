<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <title>{{ $site_name }}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="Keywords" content="网站关键词"/>
    <meta name="Description" content="网站描述"/>
    <link rel="stylesheet" type="text/css" href="/front/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/front/css/flexible-bootstrap-carousel.css" />
    <link rel="stylesheet" type="text/css" href="/front/css/style.css">
    <link rel="stylesheet" type="text/css" href="/front/css/zzsc.css">
    
    <link href="http://cdn.bootcss.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">
    <script src="/front/js/jquery.min.js"></script>
    <script src="/front/js/modernizr.custom.js"></script>
    <script src="/front/js/scroll.js"></script>

</head>
<body>

@include('front.header')

<!-- 轮播图开始 -->
<div id="slide" class="slide">
    <ul>
        @foreach($banners as $banner)
            <li><a href="{{ $banner->url }}"><img src="/images/{{ $banner->image_id }}"></a></li>
        @endforeach
    </ul>
    <div>
        <span class="prev_btn"> < </span>
        <span class="next_btn"> > </span>
    </div>
</div>
<!-- 轮播图结束 -->
<!-- NEW & TRENDING开始 -->
<div class="container box1">
    <div class="carousel-example">

        <h2 class="section-title text-center"><span>{{ $collection_first->title }}</span></h2>
        <!-- FLEXIBLE BOOTSTRAP CAROUSEL -->
        <div id="simple-content-carousel" class="carousel flexible slide" data-ride="carousel" data-interval="5000" data-wrap="true">
            <div class="items">
                @foreach ($collection_first->products as $product)
                <div class="flex-item">
                    <a href="/products/{{ $product->id }}"><img class="img-responsive" src="/images/{{ explode(',', $product->image_ids)[0] }}"/></a>
                    <h4><a href="/products/{{ $product->id }}">{{ $product->title }}</a></h4>
                </div>
                @endforeach

            </div>

            <div class="carousel-inner" role="listbox">

            </div>

            <a class="left carousel-control" href="#simple-content-carousel" role="button" data-slide="prev">
                <span class="fa fa-angle-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#simple-content-carousel" role="button" data-slide="next">
                <span class="fa fa-angle-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
        <h2 class="section-title text-center"><span>{{ $collection_second->title }}</span></h2>
        <!-- FLEXIBLE BOOTSTRAP CAROUSEL -->
        <div id="simple-content-carousel2" class="carousel flexible slide" data-ride="carousel" data-interval="5000" data-wrap="true">
            <div class="items">
                @foreach ($collection_second->products as $product)
                <div class="flex-item">
                    <a href="/products/{{ $product->id }}"><img class="img-responsive" src="/images/{{ explode(',', $product->image_ids)[0] }}"/></a>
                    <h4><a href="/products/{{ $product->id }}">{{ $product->title }}</a></h4>
                </div>
                @endforeach
            </div>

            <div class="carousel-inner" role="listbox">

            </div>

            <a class="left carousel-control" href="#simple-content-carousel2" role="button" data-slide="prev">
                <span class="fa fa-angle-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#simple-content-carousel2" role="button" data-slide="next">
                <span class="fa fa-angle-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
</div>
<!-- NEW & TRENDING结束 -->
<!-- 四图展示开始 -->
<div class="container">
    <div class=" row featureImg">
        <div class="col-md-3"><a href="javascript:;"><img src="/front/images/2f6c0f2174814978af111d1a77dc7dc0.webp.jpg" alt=""  class="img-responsive"></a></div>
        <div class="col-md-3"><a href="javascript:;"><img src="/front/images/71b84d9970cd49b0b9a80c24147c8fb4.jpg" alt=""  class="img-responsive"></a></div>
        <div class="col-md-3"><a href="javascript:;"><img src="/front/images/163178fab06343b698e4a7d302a1a140.webp.jpg" alt=""  class="img-responsive"></a></div>
        <div class="col-md-3"><a href="javascript:;"><img src="/front/images/963a2beee4574e56844a4ed6494ae773.jpg" alt=""  class="img-responsive"></a></div>
    </div>
</div>
<!-- 四图展示结束 -->
<div class="container" style="margin-bottom: 60px;">
    <div class="row we">
        <div class="col-md-6">
            <h2>About Us</h2>
            <p>{{ $about_us->description }}</p>
            <div class="text-center story"><a href="/pages/{{ $about_us->id }} "> More </a></div>
        </div>
        <div class="col-md-6">
            <img src="/images/{{ $video }}" alt="" />
        </div>
    </div>
</div>
<!-- 客户评论开始 -->
<!-- <div class="Testimonials">
    <div class="container text-center">
        <p class="tex">我的订单比预期提前两天到达！（是的）。我所有的东西都完好无损地运到了，包装都非常好。</p>
        <p class="smalltex"><a href="javascript:;">Read More</a></p>
    </div>
</div> -->

@include ('front.footer')

<script src="/front/js/cbpHorizontalSlideOutMenu.min.js"></script>
<script src="/front/js/slide.js"></script>
<script src="http://cdn.bootcss.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

<script type="text/javascript" src="/front/js/flexible-bootstrap-carousel.js"></script>
<script type="text/javascript" src="/front/js/script.js"></script>
<script>
    var menu = new cbpHorizontalSlideOutMenu( document.getElementById( 'cbp-hsmenu-wrapper' ) );
</script>
<script>
    slideLlx("slide");//参数为字符串是外层容器的id,可以多次调用
</script>


</body>
</html>