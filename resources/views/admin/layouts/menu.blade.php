<ul class="sidebar-menu" id="menu" data-widget="tree">
    <li class="header">@if (Auth::user()->role === "role_user") main navigations @else 主导航 @endif</li>
    <li id="home">
        <a href="@if (Auth::user()->role === 'role_user') {{ route('front.index') }} @else {{ route('admin.index') }} @endif">
            <i class="fa fa-dashboard"></i> <span>@if (Auth::user()->role === "role_user") Home @else 主页 @endif</span><span class="pull-right-container"></span>
        </a>
    </li>
    @if (Auth::user()->role === "role_admin")
    <li class="treeview menu-open">
        <a href="#">
            <i class="fa fa-map"></i>
            <span>首页</span>
            <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
        </a>
        <ul class="treeview-menu" style="display: block;">
            <li><a href="{{ route('admin.banners.index') }}"><i class="fa fa-circle-o"></i> 轮播图管理 </a></li>
        </ul>
    </li>
    <li class="treeview menu-open">
        <a href="#">
            <i class="fa fa-users"></i>
            <span>会员管理</span>
            <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
        </a>
        <ul class="treeview-menu" style="display: block;">
            <li><a href="{{ route('admin.users.index') }}"><i class="fa fa-circle-o"></i> 会员列表 </a></li>
        </ul>
    </li>

    <li class="treeview menu-open">
        <a href="#">
            <i class="fa fa-map"></i>
            <span>导航管理</span>
            <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
        </a>
        <ul class="treeview-menu" style="display: block;">
            <li><a href="{{ route('admin.navigations.index') }}"><i class="fa fa-circle-o"></i> 导航列表 </a></li>
            <li><a href="{{ route('admin.navigations.add') }}"><i class="fa fa-circle-o"></i> 添加导航 </a></li>
        </ul>
    </li>

    <li class="treeview menu-open">
        <a href="{{ route('admin.collections.index') }}">
            <i class="fa fa-list"></i>
            <span>列表页管理</span>
            <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
        </a>
        <ul class="treeview-menu" style="display: block;">
            <li><a href="{{ route('admin.collections.index') }}"><i class="fa fa-circle-o"></i> 列表页列表 </a></li>
            <li><a href="{{ route('admin.collections.add') }}"><i class="fa fa-circle-o"></i> 添加列表页 </a></li>
        </ul>
    </li>

    <li class="treeview menu-open">
        <a href="#">
            <i class="fa fa-file"></i>
            <span>封面页管理</span>
            <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
        </a>
        <ul class="treeview-menu" style="display: block;">
            <li><a href="{{ route('admin.pages.index') }}"><i class="fa fa-circle-o"></i> 封面页列表 </a></li>
            <li><a href="{{ route('admin.pages.add') }}"><i class="fa fa-circle-o"></i> 添加封面页 </a></li>
        </ul>
    </li>

    <!-- <li class="treeview">
        <a href="#">
            <i class="fa fa-files-o"></i>
            <span>文章页管理</span>
            <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
        </a>
        <ul class="treeview-menu">
            <li><a href="{{ route('admin.articles.index') }}"><i class="fa fa-circle-o"></i> 文章页列表 </a></li>
            <li><a href="{{ route('admin.articles.add') }}"><i class="fa fa-circle-o"></i> 添加文章页 </a></li>
        </ul>
    </li> -->

    <li class="treeview menu-open">
        <a href="#">
            <i class="fa fa-product-hunt"></i>
            <span>商品管理</span>
            <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
        </a>
        <ul class="treeview-menu" style="display: block;">
            <li><a href="{{ route('admin.products.index') }}"><i class="fa fa-circle-o"></i> 商品列表 </a></li>
            <li><a href="{{ route('admin.products.add') }}"><i class="fa fa-circle-o"></i> 添加商品 </a></li>
        </ul>
    </li>

    <li class="treeview menu-open">
        <a href="#">
            <i class="fa fa-money"></i>
            <span>订单管理</span>
            <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
        </a>
        <ul class="treeview-menu" style="display: block;">
            <li><a href="{{ route('admin.orders.index') }}"><i class="fa fa-circle-o"></i> 订单列表 </a></li>
        </ul>
    </li>

    <li>
        <a href="{{ route('admin.settings') }}">
            <i class="fa fa-cog"></i><span>系统设置</span><span class="pull-right-container"></span>
        </a>
    </li>
    @endif

    @if (Auth::user()->role === "role_user")
    <li class="treeview">
        <a href="#">
            <i class="fa fa-money"></i>
            <span>Orders</span>
            <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
        </a>
        <ul class="treeview-menu">
            <li><a href="{{ route('front.orders.index', ['status' => 0]) }}"><i class="fa fa-circle-o"></i> Draft </a></li>
            <li><a href="{{ route('front.orders.index', ['status' => 1]) }}"><i class="fa fa-circle-o"></i> Confirmed </a></li>
        </ul>
    </li>
    @endif
</ul>