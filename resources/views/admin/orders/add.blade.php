@extends('admin.layouts.main')

@section('title', '查看订单')

@section('css')
    <link rel="stylesheet" type="text/css" href="/css/webuploader.css">
    <link rel="stylesheet" type="text/css" href="/css/images/add.css">
@endsection

@section('content')
    <section class="content-header">
        <h1>
            @if (Auth::user()->role === "role_user") Orders @else 订单管理 @endif<small>@if (Auth::user()->role === "role_user") View @else 查看订单 @endif</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> @if (Auth::user()->role === "role_user") Home @else 主页 @endif</a></li>
            <li class="active">@if (Auth::user()->role === "role_user") View @else 查看订单 @endif</li>
        </ol>
    </section>

    <section class="content">
        <div class="box">

            <div class="box-header"></div>
            <div class="box-body">
                <form class="form-horizontal" role="form" method="POST" action="@if(Auth::user()->role === 'role_user'){{ route('front.orders.updatePost', array('id' => $order->id)) }}@else {{ route('admin.orders.updatePost', array('id' => $order->id)) }} @endif">
                    <div class="form-group">
                        <label class="col-md-2 control-label">@if (Auth::user()->role === "role_user") Name @else 姓名 @endif</label>
                        <div class="col-md-8">
                            <input type="text" name="name" class="form-control" placeholder='@if (Auth::user()->role === "role_user") Name @else 姓名 @endif' required value="{{ $order->user->name }}">
                            <div class="help-block  with-errors"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">@if (Auth::user()->role === "role_user") Phone Number @else 联系电话 @endif</label>
                        <div class="col-md-8">
                            <input type="text" name="telephone" class="form-control" placeholder='@if (Auth::user()->role === "role_user") Phone Number @else 联系电话 @endif' value="{{ $order->telephone }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">@if (Auth::user()->role === "role_user") Province @else 省份 @endif</label>
                        <div class="col-md-8">
                            <input type="text" name="province" class="form-control" placeholder='@if (Auth::user()->role === "role_user") Province @else 省份 @endif' value="{{ $order->province }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">@if (Auth::user()->role === "role_user") Prefecture @else 城市 @endif</label>
                        <div class="col-md-8">
                            <input type="text" name="city" class="form-control" placeholder='@if (Auth::user()->role === "role_user") Prefecture @else 城市 @endif' value="{{ $order->city }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">@if (Auth::user()->role === "role_user") Country @else 区县 @endif</label>
                        <div class="col-md-8">
                            <input type="text" name="district" class="form-control" placeholder='@if (Auth::user()->role === "role_user") Country @else 区县 @endif' value="{{ $order->district }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">@if (Auth::user()->role === "role_user") Address @else 详细地址 @endif</label>
                        <div class="col-md-8">
                            <input type="text" name="detail_address" class="form-control" placeholder='@if (Auth::user()->role === "role_user") Address @else 详细地址 @endif' value="{{ $order->detail_address }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">@if (Auth::user()->role === "role_user") E-Mail @else 邮箱 @endif</label>
                        <div class="col-md-8">
                            <input type="email" name="email" class="form-control" placeholder='@if (Auth::user()->role === "role_user") E-Mail @else 邮箱 @endif' value="{{ $order->email }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">@if (Auth::user()->role === "role_user") Zip Code @else 邮编 @endif</label>
                        <div class="col-md-8">
                            <input type="text" name="zip_code" class="form-control" placeholder='@if (Auth::user()->role === "role_user") Zip Code @else 邮编 @endif' value="{{ $order->zip_code }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">@if (Auth::user()->role === "role_user") Quantity @else 购买数量 @endif</label>
                        <div class="col-md-8">
                            <input type="text" name="purchase_number" class="form-control" placeholder='@if (Auth::user()->role === "role_user") Quantity @else 购买数量 @endif' value="{{ $order->purchase_number }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">@if (Auth::user()->role === "role_user") Remark @else 备注 @endif</label>
                        <div class="col-md-8">
                            <textarea name="comment" style="width:100%;height: 150px;">{{ $order->comment }}</textarea>
                        </div>
                    </div>
                    <button type="submit" class="col-md-offset-2 btn btn-success">@if (Auth::user()->role === "role_user") Submit @else 提交 @endif</button>
                </form>
                    
            </div>
        </div>
    </section>

@endsection

@section('js')
    <script type="text/javascript" src="/js/webuploader.min.js"></script>
@endsection
