@extends('admin.layouts.main')

@section('css')
    <link href="https://cdn.bootcss.com/bootstrap-table/1.15.4/bootstrap-table.min.css" rel="stylesheet">
@endsection

@section('title', 'Order')

@section('content')
    <section class="content-header">
        <h1>
            Order<small>List</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('front.index') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">List</li>
        </ol>
    </section>

    <section class="content">
        <div class="box" style="margin-bottom: 5px;">

            @include('admin.component.status')

            <div class="box-body">

                @include('admin.orders.front_table')

            </div>
        </div>
        @if($status == 0)
        <div class="box box-solid">
            <div class="box-body">
                <button class="btn btn-primary pull-right" id="multi-confirm">Orders Confirm</button>
                <div class="pull-right" style="line-height: 34px;margin-right: 20px;">Total: ¥<span id="total">0</span></div>
            </div>
        </div>
        @endif
    </section>

@endsection

@section('js')
    <script src="https://cdn.bootcss.com/bootstrap-table/1.15.4/bootstrap-table.min.js"></script>
    <script type="text/javascript" src="/js/orders/index.js?version=2019-12-09"></script>
    <script type="text/javascript">
        var role = "{{ Auth::user()->role }}";
    </script>
@endsection
