<table class="table table-bordered table-hover table-striped" data-toggle="table" data-pagination="true" data-page-size="20" data-pagination-loop='true' data-side-pagination="server" data-total-field="total" data-data-field="data" data-response-handler="formatResponseData" data-url="{{ route('api.orders.get') }}" id="table-orders" data-sort-name="id" data-sort-order="desc">
    <thead>
        <tr>
            <th data-checkbox="true"></th>
            <th data-align="center" data-valign="middle" data-sortable="true" data-field="id">Id</th>
            <th data-align="center" data-valign="middle" data-field="user" data-formatter="formatUser">用户名</th>
            <th data-align="center" data-valign="middle" data-field="product" data-formatter="formatProduct">缩略图</th>
            <th data-align="center" data-valign="middle" data-field="telephone" >电话</th>
            <th data-align="center" data-valign="middle" data-field="province" >省份</th>
            <th data-align="center" data-valign="middle" data-field="city">城市</th>
            <th data-align="center" data-valign="middle" data-field="district" >地区</th>
            <th data-align="center" data-valign="middle" data-field="detail_address" >详细地址</th>
            <th data-align="center" data-valign="middle" data-field="email" >邮箱</th>
            <th data-align="center" data-valign="middle" data-field="zip_code">邮政编码</th>
            <th data-align="center" data-valign="middle" data-field="purchase_number" >购买数量</th>
            <th data-align="center" data-valign="middle" data-field="comment" >备注</th>
            <th data-align="center" data-valign="middle" data-sortable="true" data-field="created_at" >更新时间</th>
            <th data-align="center" data-valign="middle" data-sortable="true" data-field="updated_at" >创建时间</th>
            <th data-align="center" data-valign="middle" data-field="id" data-formatter="formatOperate">操作</th>
        </tr>
    </thead>
    <tbody></tbody>
</table>