<table class="table table-bordered table-hover table-striped" data-toggle="table" data-data-field="data" data-url="{{ route('front.orders.getCurrentUserOrders', ['user_id' => Auth::user()->id, 'statue' => $status]) }}" id="table-orders" >
    <thead>
        <tr>
            <th data-checkbox="true"></th>
            <th data-align="center" data-valign="middle" data-sortable="true" data-field="id">Id</th>
            <th data-align="center" data-valign="middle" data-field="user" data-formatter="formatUser">Name</th>
            <th data-align="center" data-valign="middle" data-field="product" data-formatter="formatProduct">Item</th>
            <th data-align="center" data-valign="middle" data-field="telephone" >Phone Number</th>
            <th data-align="center" data-valign="middle" data-field="province" >Province</th>
            <th data-align="center" data-valign="middle" data-field="city">Prefecture</th>
            <th data-align="center" data-valign="middle" data-field="district" >Country</th>
            <th data-align="center" data-valign="middle" data-field="detail_address" >Address</th>
            <th data-align="center" data-valign="middle" data-field="email" >E-Mail</th>
            <th data-align="center" data-valign="middle" data-field="zip_code">Zip Code</th>
            <th data-align="center" data-valign="middle" data-field="purchase_number" >Quantity</th>
            <th data-align="center" data-valign="middle" data-field="comment" >Remark</th>
            <th data-align="center" data-valign="middle" data-sortable="true" data-field="created_at" >Updated At</th>
            <th data-align="center" data-valign="middle" data-sortable="true" data-field="updated_at" >Created At</th>
            <th data-align="center" data-valign="middle" data-field="id" data-formatter="formatOperate">Operation</th>
        </tr>
    </thead>
    <tbody></tbody>
</table>