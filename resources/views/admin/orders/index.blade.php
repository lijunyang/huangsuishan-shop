@extends('admin.layouts.main')

@section('css')
    <link href="https://cdn.bootcss.com/bootstrap-table/1.15.4/bootstrap-table.min.css" rel="stylesheet">
@endsection

@section('title', '订单管理')

@section('content')
    <section class="content-header">
        <h1>
            订单管理<small>订单列表</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> 主页</a></li>
            <li class="active">订单列表</li>
        </ol>
    </section>

    <section class="content">
        <div class="box">

            @include('admin.component.status')

            <div class="box-header">
                <button class="btn btn-xs btn-default pull-right" id="export-orders">导出Excel</button>
            </div>
            <div class="box-body">

                @include('admin.orders.table')

            </div>
        </div>
    </section>

@endsection

@section('js')
    <script src="https://cdn.bootcss.com/bootstrap-table/1.15.4/bootstrap-table.min.js"></script>
    <script src="https://cdn.bootcss.com/bootstrap-table/1.15.4/locale/bootstrap-table-zh-CN.js"></script>
    <script type="text/javascript" src="/js/orders/index.js?v=20200914"></script>
    <script type="text/javascript">
        var role = "{{ Auth::user()->role }}";
    </script>
@endsection
