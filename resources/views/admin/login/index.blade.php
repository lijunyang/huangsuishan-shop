<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{ $site_name }} | Login</title>
    <link href="https://cdn.bootcss.com/twitter-bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdn.bootcss.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="/css/animate.css" rel="stylesheet">
    <link href="/css/style.css" rel="stylesheet">
</head>
<body class="gray-bg">
    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            <h5 class="logo-name">{{ $site_name }}</h5>
        </div>
        <h3>Sign In</h3>
        <form class="m-t" role="form" action="{{ route('auth.login.auth') }}" method="post">
            {{ csrf_field() }}
            <div class="form-group">
                <input type="text" name="name" class="form-control" placeholder="name" required>
            </div>
            <div class="form-group">
                <input type="password" name="password" class="form-control" placeholder="password" required>
            </div>
            @if($errors->any())
                @foreach ($errors->all() as $error)
                    <div class="alert alert-danger alert-dismissible show" id="formMessage" role="alert">
                        {{ $error }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endforeach
            @endif
            @if(session("error"))
                <div class="alert alert-danger alert-dismissible show" id="formMessage" role="alert">
                    {{ session("error") }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            <input type="hidden" name="previous" value="{{ $previous }}">
            <button type="submit" class="btn btn-primary block full-width m-b">sign in</button>
        </form>
        <div class="text-right text-small"><a href="{{ route('front.register') }}">create an account</a></div>
    </div>

</body>
</html>