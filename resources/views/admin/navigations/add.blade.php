@extends('admin.layouts.main')

@section('title', '添加导航')

@section('css')
    <link rel="stylesheet" type="text/css" href="/css/webuploader.css">
@endsection

@section('content')
    <section class="content-header">
        <h1>
            导航管理<small>添加/更新导航</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> 主页</a></li>
            <li class="active">添加/更新导航</li>
        </ol>
    </section>

    <section class="content">
        <div class="box">

            @include('admin.component.formValidateError')

            <div class="box-header"></div>
            <div class="box-body">
                <form class="form-horizontal" id="add-navigation-form"  method="POST" 
                    @if(isset($current_navigation))
                        action="{{ route('admin.navigations.updatePost', array('id' => $current_navigation->id)) }}"
                    @else
                        action="{{ route('admin.navigations.addPost') }}"
                    @endif
                >
                    <div class="form-group">
                        <label for="title" class="col-md-2 control-label">标题</label>
                        <div class="col-md-8">
                            <input class="form-control" type="text" name="title" placeholder="列表页标题" @if(isset($current_navigation)) value="{{ $current_navigation->title }}" @endif>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="type" class="col-md-2 control-label">类型</label>
                        <div class="col-md-8">
                            <select class="form-control" name="type">
                                @foreach (constant('App\Models\Navigations::TYPES') as $key => $value)
                                    <option value={{ $key }} @if(isset($current_navigation) && $current_navigation->id === $key) selected="selected" @endif>{{ $value }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="title" class="col-md-2 control-label">关联页面</label>
                        <div class="col-md-8">
                            <select class="form-control" name="type_id">
                                @if(isset($current_navigation))
                                    @if($current_navigation->type === 1)
                                        @foreach($collections as $collection)
                                            <option value="{{ $collection->id }}" @if ($current_navigation->type_id === $collection->id) selected="selected" @endif>{{ $collection->title }}</option>
                                        @endforeach
                                    @endif
                                    @if($current_navigation->type === 2)
                                        @foreach($pages as $page)
                                            <option value="{{ $page->id }}" @if($current_navigation->type_id === $page->id) selected="selected" @endif>{{ $page->title }}</option>
                                        @endforeach
                                    @endif
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="type" class="col-md-2 control-label">父导航</label>
                        <div class="col-md-8">
                            <select class="form-control" name="parent_id">
                                <option value=0>--请选择导航--</option>
                                @foreach ($navigations as $navigation)
                                    @unless (isset($current_navigation) && $current_navigation->id === $navigation->id)
                                        <option value={{ $navigation->id }} @if (isset($current_navigation) && $current_navigation->parent_id === $navigation->id) selected="selected" @endif>{{ $navigation->title }}</option>
                                    @endunless  
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="sort_order" class="col-md-2 control-label">排序</label>
                        <div class="col-md-8">
                            <input class="form-control" type="text" name="sort_order" placeholder="排序" @if(isset($current_navigation)) value="{{ $current_navigation->sort_order }}" @endif>
                        </div>
                    </div>
                    
                    <a id="submit" class="col-md-offset-2 btn btn-success">提交</a>
                </form>
                    
            </div>
        </div>
    </section>

@endsection

@section('js')
    <script type="text/javascript" src="/js/webuploader.min.js"></script>
    <script type="text/javascript" src="/js/navigations/add.js"></script>
    <script type="text/javascript">
        var upload_url = "{{ route('admin.images.upload') }}";
        @if (isset($current_navigation))
            var current_navigation = @json($current_navigation);
            var collections = @json($collections);
            var pages = @json($pages);
        @endif
    </script>
@endsection
