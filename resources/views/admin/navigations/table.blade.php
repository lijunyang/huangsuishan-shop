<table class="table table-bordered table-hover table-striped" data-toggle="table" data-pagination="true" data-page-size="20" data-pagination-loop='true' data-side-pagination="server" data-total-field="total" data-data-field="data" data-response-handler="formatResponseData" data-url="{{ route('api.navigations.get') }}" id="table-navigations">
    <thead>
        <tr>
            <th data-align="center" data-valign="middle" data-sortable="true" data-field="id">Id</th>
            <th data-align="center" data-valign="middle" data-field="title">标题</th>
            <th data-align="center" data-valign="middle" data-field="type" data-formatter="formatType">导航类型</th>
            <th data-align="center" data-valign="middle" data-field="type_id" data-formatter="formatRelatedPage">关联页面</th>
            <th data-align="center" data-valign="middle" data-field="parent_id" data-formatter="formatParentNavigation">父级导航</th>
            <th data-align="center" data-valign="middle" data-field="sort_order">排序</th>
            <th data-align="center" data-valign="middle" data-sortable="true" data-field="updated_at" >更新时间</th>
            <th data-align="center" data-valign="middle" data-sortable="true" data-field="created_at" >创建时间</th>
            <th data-align="center" data-valign="middle" data-field="id" data-formatter="formatOperate">操作</th>
        </tr>
    </thead>
    <tbody></tbody>
</table>