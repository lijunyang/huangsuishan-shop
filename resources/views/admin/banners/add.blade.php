@extends('admin.layouts.main')

@section('title', '添加轮播图')

@section('css')
    <link rel="stylesheet" type="text/css" href="/css/webuploader.css">
    <link rel="stylesheet" type="text/css" href="/css/images/add.css">
@endsection

@section('content')
    <section class="content-header">
        <h1>
            轮播图管理<small>添加/更新轮播图</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> 主页</a></li>
            <li class="active">添加/更新轮播图</li>
        </ol>
    </section>

    <section class="content">
        <div class="box">

            @include('admin.component.formValidateError')

            <div class="box-header"></div>
            <div class="box-body">
                <form class="form-horizontal" id="add-banner-form"  method="POST" 
                    @if(isset($banner))
                        action="{{ route('admin.banners.updatePost', array('id' => $banner->id)) }}"
                    @else
                        action="{{ route('admin.banners.addPost') }}"
                    @endif
                >
                    <div class="form-group">
                        <label for="image" class="col-md-2 control-label"> <b style="color: red">*</b> 图片</label>
                        <div class="col-md-8">
                            <div id="uploader-pc" class="wu-example">
                                <div id="thelist-pc" class="uploader-list">
                                    @if(isset($banner))
                                        <img src="/images/{{ $banner->image_id }}" class="img-thumbnail">
                                    @endif
                                </div>
                                <div id="picker-pc" class="pull-left">选择文件</div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="url"> <b style="color: red">*</b> 跳转链接</label>
                        <div class="col-md-8">
                            <input type="text" name="url" class="form-control" value="@if(isset($banner)) {{ $banner->url }} @endif" placeholder="请输入跳转链接">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label" for="sort">排序</label>
                        <div class="col-md-8">
                            <input type="text" name="sort" class="form-control" value="@if(isset($banner)) {{ $banner->sort }} @endif" placeholder="请输入排序序号">
                        </div>
                    </div>
        
                    <input type="hidden" name="image_id" @if(isset($banner)) value="{{ $banner->pc_image_id }}" @endif>
                    <a id="submit" class="col-md-offset-2 btn btn-success">提交</a>
                </form>
                    
            </div>
        </div>
    </section>

@endsection

@section('js')
    <script type="text/javascript" src="/js/webuploader.min.js"></script>
    <script type="text/javascript" src="/js/banners/add.js?version=2019-11-28"></script>
    <script type="text/javascript">
        var upload_url = "{{ route('admin.images.upload') }}";
    </script>
@endsection
