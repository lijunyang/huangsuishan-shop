<table class="table table-bordered table-hover table-striped" data-toggle="table" data-pagination="true" data-page-size="20" data-pagination-loop='true' data-side-pagination="server" data-total-field="total" data-data-field="data" data-response-handler="formatResponseData" data-url="{{ route('api.banners.get') }}" id="table-banners">
    <thead>
        <tr>
            <th data-align="center" data-valign="middle" data-sortable="true" data-field="id">Id</th>
            <th data-align="center" data-valign="middle" data-field="image_id" data-formatter="formatImage">图片</th>
            <th data-align="center" data-valign="middle" data-field="url">跳转链接</th>
            <th data-align="center" data-valign="middle" data-field="sort">排序</th>
            <th data-align="center" data-valign="middle" data-sortable="true" data-field="created_at" >更新时间</th>
            <th data-align="center" data-valign="middle" data-sortable="true" data-field="updated_at" >创建时间</th>
            <th data-align="center" data-valign="middle" data-field="id" data-formatter="formatOperate">操作</th>
        </tr>
    </thead>
    <tbody></tbody>
</table>