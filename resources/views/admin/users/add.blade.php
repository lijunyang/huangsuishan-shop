@extends('admin.layouts.main')

@section('title', '添加用户')

@section('css')
    <link rel="stylesheet" type="text/css" href="/css/webuploader.css">
    <link rel="stylesheet" type="text/css" href="/css/images/add.css">
@endsection

@section('content')
    <section class="content-header">
        <h1>
            用户管理<small>添加/更新用户</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> 主页</a></li>
            <li class="active">添加/更新用户</li>
        </ol>
    </section>

    <section class="content">
        <div class="box">

            @include('admin.component.formValidateError')

            <div class="box-header"></div>
            <div class="box-body">
                <form class="form-horizontal" id="add-user-form"  method="POST" data-toggle="validator" role="form" action="{{ route('admin.users.updatePost', array('id' => $user->id)) }}">
                    <div class="form-group">
                        <label for="name" class="col-md-2 control-label">用户名</label>
                        <div class="col-md-8">
                            <input class="form-control" data-error="至少六位字符" data-minlength="6" type="text" name="name" placeholder="用户标题" @if(isset($user)) value="{{ $user->name }}" @endif required>
                            <div class="help-block  with-errors"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email" class="col-md-2 control-label">邮箱</label>
                        <div class="col-md-8">
                            <input class="form-control" type="email" name="email" placeholder="邮箱"  data-error="请输入正确邮箱" @if(isset($user)) value="{{ $user->email }}" @endif required>
                            <div class="help-block  with-errors"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="role" class="col-md-2 control-label">角色</label>
                        <div class="col-md-8">
                            <select class="form-control" name="role">
                                <option value="role_user" @if ($user->role === "role_user") selected="selected" @endif>普通用户</option>
                                <option value="role_admin" @if ($user->role === "role_admin") selected="selected" @endif>管理员</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="password" class="col-md-2 control-label">新密码</label>
                        <div class="col-md-8">
                            <input class="form-control" type="text" name="password" placeholder="新密码" data-minlength="6" data-error="至少六位字符" required>
                            <div class="help-block  with-errors"></div>
                        </div>
                    </div>
                    <button type="submit" class="col-md-offset-2 btn btn-success">提交</button>
                </form>
                    
            </div>
        </div>
    </section>

@endsection

@section('js')
    <script type="text/javascript" src="/js/webuploader.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.9/validator.min.js"></script>
    <script type="text/javascript" src="/js/users/add.js"></script>
    <script type="text/javascript">
        var upload_url = "{{ route('admin.images.upload') }}";
    </script>
@endsection
