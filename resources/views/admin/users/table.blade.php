<table class="table table-bordered table-hover table-striped" data-toggle="table" data-pagination="true" data-page-size="20" data-pagination-loop='true' data-side-pagination="server" data-total-field="total" data-data-field="data" data-response-handler="formatResponseData" data-url="{{ route('api.users.get') }}" id="table-users">
    <thead>
        <tr>
            <th data-align="center" data-valign="middle" data-sortable="true" data-field="id">Id</th>
            <th data-align="center" data-valign="middle" data-field="name">用户名</th>
            <th data-align="center" data-valign="middle" data-field="email">邮箱</th>
            <th data-align="center" data-valign="middle" data-field="role" data-formatter="formatRole">角色</th>
            <th data-align="center" data-valign="middle" data-sortable="true" data-field="updated_at">更新时间</th>
            <th data-align="center" data-valign="middle" data-sortable="true" data-field="created_at">创建时间</th>
            <th data-align="center" data-valign="middle" data-field="id" data-formatter="formatOperate">操作</th>
        </tr>
    </thead>
    <tbody></tbody>
</table>