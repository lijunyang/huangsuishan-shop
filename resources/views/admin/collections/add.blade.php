@extends('admin.layouts.main')

@section('title', '添加列表页')

@section('css')
    <link rel="stylesheet" type="text/css" href="/css/webuploader.css">
    <link rel="stylesheet" type="text/css" href="/css/images/add.css">
@endsection

@section('content')
    <section class="content-header">
        <h1>
            列表页管理<small>添加/更新列表页</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> 主页</a></li>
            <li class="active">添加/更新列表页</li>
        </ol>
    </section>

    <section class="content">
        <div class="box">

            @include('admin.component.formValidateError')

            <div class="box-header"></div>
            <div class="box-body">
                <form class="form-horizontal" id="add-collection-form"  method="POST" 
                    @if(isset($collection))
                        action="{{ route('admin.collections.updatePost', array('id' => $collection->id)) }}"
                    @else
                        action="{{ route('admin.collections.addPost') }}"
                    @endif
                >
                    <div class="form-group">
                        <label for="title" class="col-md-2 control-label">标题</label>
                        <div class="col-md-8">
                            <input class="form-control" type="text" name="title" placeholder="列表页标题" @if(isset($collection)) value="{{ $collection->title }}" @endif>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="image" class="col-md-2 control-label">列表页图片</label>
                        <div class="col-md-8">
                            <div id="uploader-pc" class="wu-example">
                                <div id="thelist-pc" class="uploader-list">
                                    @if(isset($collection))
                                        <img src="/admin/images/{{ $collection->pc_image_id }}" class="img-thumbnail">
                                    @endif
                                </div>
                                <div id="picker-pc" class="pull-left">选择文件</div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="description" class="col-md-2 control-label">描述</label>
                        <div class="col-md-8">
                            <textarea class="form-control" type="text" rows=15 name="description" >@if(isset($collection)){{ $collection->description }}@endif</textarea>
                        </div>
                    </div>
                    <input type="hidden" name="pc_image_id" @if(isset($collection)) value="{{ $collection->pc_image_id }}" @endif>
                    <a id="submit" class="col-md-offset-2 btn btn-success">提交</a>
                </form>
                    
            </div>
        </div>
    </section>

@endsection

@section('js')
    <script type="text/javascript" src="/js/webuploader.min.js"></script>
    <script type="text/javascript" src="/js/collections/add.js"></script>
    <script type="text/javascript">
        var upload_url = "{{ route('admin.images.upload') }}";
    </script>
@endsection
