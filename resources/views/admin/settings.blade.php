@extends('admin.layouts.main')

@section('css')
    <link href="https://cdn.bootcss.com/bootstrap-table/1.15.4/bootstrap-table.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/css/webuploader.css">
@endsection
    
@section('title', '系统设置')

@section('content')
    <section class="content-header">
        <h1>
            系统设置
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> 主页</a></li>
            <li class="active">系统设置</li>
        </ol>
    </section>

    <section class="content">
        <div class="box">

            @include('admin.component.status')

            <div class="box-header"></div>
            <div class="box-body">
                
                <form class="form-horizontal" id="update-settings-form"  method="POST" action="{{ route('admin.settingsPost') }}">
                    <div class="form-group">
                        <label for="site_name" class="col-md-2 control-label">站点名称</label>
                        <div class="col-md-8">
                            <input class="form-control" type="text" name="site_name" placeholder="列表页标题" value="{{ $site_name }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="about_us" class="col-md-2 control-label">关于我们</label>
                        <div class="col-md-8">
                            <select class="form-control" name="about_us">
                                @foreach ($pages as $page)
                                    <option value={{ $page->id }} @if($page->id == $about_us) selected @endif>{{ $page->title }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="collection_first" class="col-md-2 control-label">列表一</label>
                        <div class="col-md-8">
                            <select class="form-control" name="collection_first">
                                @foreach ($collections as $collection)
                                    <option value={{ $collection->id }} @if($collection->id == $collection_first) selected @endif>{{ $collection->title }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="collection_second" class="col-md-2 control-label">列表二</label>
                        <div class="col-md-8">
                            <select class="form-control" name="collection_second">
                                @foreach ($collections as $collection)
                                    <option value={{ $collection->id }} @if($collection->id == $collection_second) selected @endif>{{ $collection->title }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="logo" class="col-md-2 control-label">Logo</label>
                        <div class="col-md-8">
                            <div id="uploader-logo" class="wu-example">
                                <div id="thelist-logo" class="uploader-list">
                                    @if($logo !== "")
                                        <img src="/images/{{ $logo }}" class="img-thumbnail">
                                    @endif
                                </div>
                                <div id="picker-logo" class="pull-left">选择文件</div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="video" class="col-md-2 control-label">首页图片</label>
                        <div class="col-md-8">
                            <div id="uploader-video" class="wu-example">
                                <div id="thelist-video" class="uploader-list">
                                    @if($video !== "")
                                        <img src="/images/{{ $video }}" class="img-thumbnail">
                                    @endif
                                </div>
                                <div id="picker-video" class="pull-left">选择文件</div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="footer_description" class="col-md-2 control-label">底部描述</label>
                        <div class="col-md-8">
                            <textarea class="form-control" cols="6" name="footer_description">{{ $footer_description }}</textarea>
                        </div>
                    </div>
                    <input type="hidden" name="logo" value="{{ $logo }}">
                    <input type="hidden" name="video" value="{{ $video }}">
                    <a id="submit" class="col-md-offset-2 btn btn-success">提交</a>
                </form>
            </div>
        </div>
    </section>
  
@endsection

@section('js')
    <script src="https://cdn.bootcss.com/bootstrap-table/1.15.4/bootstrap-table.min.js"></script>
    <script src="https://cdn.bootcss.com/bootstrap-table/1.15.4/locale/bootstrap-table-zh-CN.js"></script>
    <script type="text/javascript" src="/js/webuploader.min.js"></script>
    <script type="text/javascript" src="/js/settings.js?v=2020-01-13"></script>
    <script type="text/javascript">
        var upload_url = "{{ route('admin.images.upload') }}";
    </script>
@endsection
