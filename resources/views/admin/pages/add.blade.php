@extends('admin.layouts.main')

@section('title', '添加封面页')

@section('css')
    <link rel="stylesheet" type="text/css" href="/css/webuploader.css">
    <link rel="stylesheet" type="text/css" href="/css/images/add.css">
@endsection

@section('content')
    <section class="content-header">
        <h1>
            封面页管理<small>添加/更新封面页</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> 主页</a></li>
            <li class="active">添加/更新封面页</li>
        </ol>
    </section>

    <section class="content">
        <div class="box">

            @include('admin.component.formValidateError')

            <div class="box-header"></div>
            <div class="box-body">
                <form class="form-horizontal" id="add-page-form"  method="POST" 
                    @if(isset($page))
                        action="{{ route('admin.pages.updatePost', array('id' => $page->id)) }}"
                    @else
                        action="{{ route('admin.pages.addPost') }}"
                    @endif
                >
                    <div class="form-group">
                        <label for="title" class="col-md-2 control-label">标题</label>
                        <div class="col-md-8">
                            <input class="form-control" type="text" name="title" placeholder="列表页标题" @if(isset($page)) value="{{ $page->title }}" @endif>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="image" class="col-md-2 control-label">封面页图片</label>
                        <div class="col-md-8">
                            <div id="uploader-pc" class="wu-example">
                                <div id="thelist-pc" class="uploader-list">
                                    @if(isset($page) && $page->pc_image_id !== null)
                                        <img src="/admin/images/{{ $page->pc_image_id }}" class="img-thumbnail">
                                    @endif
                                </div>
                                <div id="picker-pc" class="pull-left">选择文件</div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="description" class="col-md-2 control-label">描述</label>
                        <div class="col-md-8">
                            <textarea class="form-control" type="text" rows=10 name="description" placeholder="简介">@if(isset($page)) {{ $page->description }} @endif</textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="content" class="col-md-2 control-label">内容</label>
                        <div class="col-md-8">
                            <textarea id="container" name="content">@if(isset($page)) {{ $page->content }} @endif</textarea>
                        </div>
                    </div>

                    @include('vendor.ueditor.assets')
                    
                    <input type="hidden" name="pc_image_id" @if(isset($page)) value="{{ $page->pc_image_id }}" @endif>
                    <input type="hidden" name="app_image_id" @if(isset($page)) value="{{ $page->app_image_id }}" @endif>
                    <a id="submit" class="col-md-offset-2 btn btn-success">提交</a>
                </form>
                    
            </div>
        </div>
    </section>

@endsection

@section('js')
    <script type="text/javascript" src="/js/webuploader.min.js"></script>
    <script type="text/javascript" src="/js/pages/add.js"></script>
    <script type="text/javascript">
        var upload_url = "{{ route('admin.images.upload') }}";
        var ue = UE.getEditor('container');
        ue.ready(function() {
            ue.setHeight(500);
            ue.execCommand('serverparam', '_token', '{{ csrf_token() }}');
        });
    </script>
@endsection
