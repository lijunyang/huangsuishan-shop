<table class="table table-bordered table-hover table-striped" data-toggle="table" data-pagination="true" data-page-size="20" data-pagination-loop='true' data-side-pagination="server" data-total-field="total" data-data-field="data" data-response-handler="formatResponseData" data-url="{{ route('api.pages.get') }}" id="table-pages">
    <thead>
        <tr>
            <th data-align="center" data-valign="middle" data-sortable="true" data-field="id">Id</th>
            <th data-align="center" data-valign="middle" data-field="title">标题</th>
            <th data-align="center" data-valign="middle" data-field="pc_image_id" data-formatter="formatImage">封面页图片</th>
            <th data-align="center" data-valign="middle" data-sortable="true" data-field="updated_at" >更新时间</th>
            <th data-align="center" data-valign="middle" data-sortable="true" data-field="created_at" >创建时间</th>
            <th data-align="center" data-valign="middle" data-field="id" data-formatter="formatOperate">操作</th>
        </tr>
    </thead>
    <tbody></tbody>
</table>