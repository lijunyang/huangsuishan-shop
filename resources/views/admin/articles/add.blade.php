@extends('admin.layouts.main')

@section('title', '添加文章')

@section('css')
    <link rel="stylesheet" type="text/css" href="/css/webuploader.css">
    <link rel="stylesheet" type="text/css" href="/css/images/add.css">
@endsection

@section('content')
    <section class="content-header">
        <h1>
            文章页管理<small>添加/更新文章</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> 主页</a></li>
            <li class="active">添加/更新文章</li>
        </ol>
    </section>

    <section class="content">
        <div class="box">

            @include('admin.component.formValidateError')

            <div class="box-header"></div>
            <div class="box-body">
                <form class="form-horizontal" id="add-article-form"  method="POST" 
                    @if(isset($article))
                        action="{{ route('admin.articles.updatePost', array('id' => $article->id)) }}"
                    @else
                        action="{{ route('admin.articles.addPost') }}"
                    @endif
                >
                    <div class="form-group">
                        <label for="title" class="col-md-2 control-label">标题</label>
                        <div class="col-md-8">
                            <input class="form-control" type="text" name="title" placeholder="列表页标题" @if(isset($article)) value="{{ $article->title }}" @endif>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="collection_id" class="col-md-2 control-label">所属列表</label>
                        <div class="col-md-8">
                            <select class="form-control" name="collection_id">
                                @foreach ($collections as $collection)
                                    <option value="{{ $collection->id }}" @if(isset($article) && $article->collection_id === $collection->id) selected="selected" @endif>{{ $collection->title }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="title" class="col-md-2 control-label">状态</label>
                        <div class="col-md-8">
                            <label class="radio-inline">
                                <input type="radio" name="status" checked="checked" value=1 @if(isset($article) && $article->status === 1) checked="checked" @endif> 有效
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="status" value=0 @if(isset($article) && $article->status === 0) checked="checked" @endif> 无效
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="image" class="col-md-2 control-label">PC图片</label>
                        <div class="col-md-3">
                            <div id="uploader-pc" class="wu-example">
                                <div id="thelist-pc" class="uploader-list">
                                    @if(isset($article) && $article->pc_image_id !== null)
                                        <img src="/admin/images/{{ $article->pc_image_id }}" class="img-thumbnail">
                                    @endif
                                </div>
                                <div id="picker-pc" class="pull-left">选择文件</div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <label for="image" class="col-md-2 control-label">M图片</label>
                        <div class="col-md-3">
                            <div id="uploader-m" class="wu-example">
                                <div id="thelist-m" class="uploader-list">
                                    @if(isset($article) && $article->app_image_id !== null)
                                        <img src="/admin/images/{{ $article->app_image_id }}" class="img-thumbnail">
                                    @endif
                                </div>
                                <div id="picker-m" class="pull-left">选择文件</div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="good_count" class="col-md-2 control-label">点赞数</label>
                        <div class="col-md-8">
                            <input class="form-control" type="text" name="good_count" placeholder="点赞数" @if(isset($article)) value="{{ $article->good_count }}" @endif>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="description" class="col-md-2 control-label">描述</label>
                        <div class="col-md-8">
                            <textarea class="form-control" type="text" rows=10 name="description" placeholder="简介">@if(isset($article)) {{ $article->description }} @endif</textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="content" class="col-md-2 control-label">内容</label>
                        <div class="col-md-8">
                            <textarea id="container" name="content">@if(isset($article)) {{ $article->content }} @endif</textarea>
                        </div>
                    </div>

                    @include('vendor.ueditor.assets')
                    
                    <input type="hidden" name="pc_image_id" @if(isset($article)) value="{{ $article->pc_image_id }}" @endif>
                    <input type="hidden" name="app_image_id" @if(isset($article)) value="{{ $article->app_image_id }}" @endif>
                    <a id="submit" class="col-md-offset-2 btn btn-success">提交</a>
                </form>
                    
            </div>
        </div>
    </section>

@endsection

@section('js')
    <script type="text/javascript" src="/js/webuploader.min.js"></script>
    <script type="text/javascript" src="/js/articles/add.js"></script>
    <script type="text/javascript">
        var upload_url = "{{ route('admin.images.upload') }}";
        var ue = UE.getEditor('container');
        ue.ready(function() {
            ue.setHeight(500);
            ue.execCommand('serverparam', '_token', '{{ csrf_token() }}');
        });
    </script>
@endsection
