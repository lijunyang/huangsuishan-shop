<table class="table table-bordered table-hover table-striped" data-toggle="table" data-pagination="true" data-page-size="20" data-pagination-loop='true' data-side-pagination="server" data-total-field="total" data-data-field="data" data-query-params="queryParams" data-response-handler="formatResponseData" data-sort-name="id" data-sort-order="desc" data-url="{{ route('api.products.get') }}" id="table-products">
    <thead>
        <tr>
            <th data-align="center" data-valign="middle" data-field="id">Id</th>
            <th data-align="center" data-valign="middle" data-field="title">标题</th>
            <th data-align="center" data-valign="middle" data-field="collections" data-formatter="formatCollection">所属列表</th>
            <th data-align="center" data-valign="middle" data-field="image_ids" data-formatter="formatImage">缩略图</th>
            <th data-align="center" data-valign="middle" data-field="brand">品牌</th>
            <th data-align="center" data-valign="middle" data-field="number">数量</th>
            <th data-align="center" data-valign="middle" data-field="type">型号</th>
            <th data-align="center" data-valign="middle" data-field="price">价格</th>
            <th data-align="center" data-valign="middle" data-sortable="true" data-field="updated_at" >更新时间</th>
            <th data-align="center" data-valign="middle" data-sortable="true" data-field="created_at" >创建时间</th>
            <th data-align="center" data-valign="middle" data-field="id" data-formatter="formatOperate">操作</th>
        </tr>
    </thead>
    <tbody></tbody>
</table>