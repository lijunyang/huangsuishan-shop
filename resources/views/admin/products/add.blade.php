@extends('admin.layouts.main')

@section('title', '添加商品')

@section('css')
    <link rel="stylesheet" type="text/css" href="/css/webuploader.css">
    <link rel="stylesheet" type="text/css" href="/css/images/add.css">
@endsection

@section('content')
    <section class="content-header">
        <h1>
            商品管理<small>添加/更新商品</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> 主页</a></li>
            <li class="active">添加/更新商品</li>
        </ol>
    </section>

    <section class="content">
        <div class="box">

            @include('admin.component.formValidateError')

            <div class="box-header"></div>
            <div class="box-body">
                <form class="form-horizontal" id="add-product-form"  method="POST" 
                    @if(isset($product))
                        action="{{ route('admin.products.updatePost', array('id' => $product->id)) }}"
                    @else
                        action="{{ route('admin.products.addPost') }}"
                    @endif
                >
                    <div class="form-group">
                        <label for="title" class="col-md-2 control-label">标题</label>
                        <div class="col-md-8">
                            <input class="form-control" type="text" name="title" placeholder="商品标题" @if(isset($product)) value="{{ $product->title }}" @endif>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="collection_id" class="col-md-2 control-label">所属列表</label>
                        <div class="col-md-8">
                            <select name="collection_ids[]" id="collection-ids" class="form-control" multiple="multiple">
                                @foreach ($collections as $collection)
                                    <option value={{ $collection->id }} @if (isset($product) && in_array($collection->id, $collection_ids)) selected="selected" @endif>{{ $collection->title }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="brand" class="col-md-2 control-label">品牌</label>
                        <div class="col-md-8">
                            <input class="form-control" type="text" name="brand" placeholder="商品品牌" @if(isset($product)) value="{{ $product->brand }}" @endif>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="number" class="col-md-2 control-label">数量</label>
                        <div class="col-md-8">
                            <input class="form-control" type="text" name="number" placeholder="商品数量" @if(isset($product)) value="{{ $product->number }}" @endif>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="type" class="col-md-2 control-label">型号</label>
                        <div class="col-md-8">
                            <input class="form-control" type="text" name="type" placeholder="商品型号" @if(isset($product)) value="{{ $product->type }}" @endif>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="price" class="col-md-2 control-label">价格</label>
                        <div class="col-md-8">
                            <input class="form-control" type="text" name="price" placeholder="商品价格" @if(isset($product)) value="{{ $product->price }}" @endif>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="image" class="col-md-2 control-label">图片集</label>
                        <div class="col-md-8">
                            <div id="uploader" class="wu-example">
                                <div id="thelist" class="uploader-list">
                                    @if (isset($images))
                                        @foreach ($images as $image)
                                            <div class="multiple-images col-md-3"><img src="/images/{{ $image }}" class="img-thumbnail"><br><a class="btn btn-xs btn-danger delete-image related-image" data-id={{ $image }}>删除</a></div>
                                        @endforeach
                                    @endif
                                    <div class="clearfix"></div>
                                </div>
                                <div id="picker" class="pull-left">选择文件</div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="content" class="col-md-2 control-label">内容</label>
                        <div class="col-md-8">
                            <textarea id="container" name="content">@if(isset($product)) {{ $product->content }} @endif</textarea>
                        </div>
                    </div>

                    @include('vendor.ueditor.assets')
                    
                    <input type="hidden" name="image_ids" @if(isset($product)) value="{{ $product->image_ids }}" @endif>
                    <a id="submit" class="col-md-offset-2 btn btn-success">提交</a>
                </form>
                    
            </div>
        </div>
    </section>

@endsection

@section('js')
    <script type="text/javascript" src="/js/webuploader.min.js"></script>
    <script type="text/javascript" src="/js/products/add.js"></script>
    <script type="text/javascript">
        var upload_url = "{{ route('admin.images.upload') }}";
        var ue = UE.getEditor('container');
        ue.ready(function() {
            ue.setHeight(500);
            ue.execCommand('serverparam', '_token', '{{ csrf_token() }}');
        });
    </script>
@endsection
