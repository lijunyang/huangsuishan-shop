@extends('admin.layouts.main')

@section('css')
    <link href="https://cdn.bootcss.com/bootstrap-table/1.15.4/bootstrap-table.min.css" rel="stylesheet">
@endsection
    
@section('title', '商品管理')

@section('content')
    <section class="content-header">
        <h1>
            商品管理<small>商品列表</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> 主页</a></li>
            <li class="active">商品列表</li>
        </ol>
    </section>

    <section class="content">
        <div class="box">

            @include('admin.component.status')

            <div class="box-header">
                <form>
                    <div class="form-inline">
                        <div class="form-group col-md-3">
                            <label for="collection_id">列表</label>
                            <select name="collection_id"  class="form-control" style="width: 80%">
                                <option value="">全部</option>
                                @foreach ($collections as $collection)
                                    <option value={{ $collection->id }}>{{ $collection->title }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="brand">品牌</label>
                            <input type="text" class="form-control" name="brand" style="width: 80%">
                        </div>
                        <a class="btn btn-default" id="search-products">搜索</a>
                    </div>
                </form>
            </div>
            <div class="box-body">
                
                @include('admin.products.table')
                    
            </div>
        </div>
    </section>
  
@endsection

@section('js')
    <script src="https://cdn.bootcss.com/bootstrap-table/1.15.4/bootstrap-table.min.js"></script>
    <script src="https://cdn.bootcss.com/bootstrap-table/1.15.4/locale/bootstrap-table-zh-CN.js"></script>
    <script type="text/javascript" src="/js/products/index.js?v=20200914"></script>
@endsection
