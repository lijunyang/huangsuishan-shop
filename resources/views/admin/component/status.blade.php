@if (session('status'))
    @component('admin.layouts.alert')
        @slot('style')
            info
        @endslot
        @slot('message')
            {{ session('status') }}
        @endslot
    @endcomponent
@endif