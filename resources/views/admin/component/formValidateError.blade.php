@if (count($errors) > 0)
    @component('admin.layouts.alert')
        @slot('style')
            danger
        @endslot
        @slot('message')
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        @endslot
    @endcomponent
@endif