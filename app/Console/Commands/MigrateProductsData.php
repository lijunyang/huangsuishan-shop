<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Products;
use App\Models\CollectionsProducts;

class MigrateProductsData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'migrate:products';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '迁移products数据表数据';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $products = Products::all();
        $bar = $this->output->createProgressBar(count($products));
        foreach ($products as $product) {
            $collection_product = new CollectionsProducts;
            $collection_product->collection_id = $product->collection_id;
            $collection_product->product_id = $product->id;
            $collection_product->save();
            $bar->advance();
        }
        $bar->finish();
    }
}
