<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Collections extends Model
{
    public $table = 'collections';

    public function navigation(){
        return $this->morphMany('App\Models\Navigations', "type", "type", 'type_id');
    }

    public function articles()
    {
        return $this->hasMany('App\Models\Article', 'collection_id');
    }

    public function products()
    {
        return $this->belongsToMany('App\Models\Products', 'collections_products', 'collection_id', 'product_id')->limit(12);
    }

    protected function filter(array $filter): Builder
    {
        $query = $this->newQuery();

        if($sort = array_get($filter,'sort', 'id')){
            $order = array_get($filter, 'order', 'asc');
            $query->orderBy($sort, $order);
        }

        return $query;
    }
}
