<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Orders extends Model {
	const STATUS_DRAFT = 0;
	const STATUS_CONFIRM = 1;
	public $table = 'orders';

	public function user() {
		return $this->belongsTo('App\Models\Users', 'user_id');
	}

	public function product() {
		return $this->belongsTo('App\Models\Products', 'product_id');
	}

	protected function filter(array $filter): Builder{
		$query = $this->newQuery();

		if ($sort = array_get($filter, 'sort', 'id')) {
			$order = array_get($filter, 'order', 'desc');
			$query->orderBy($sort, $order);
		}

		return $query;
	}
}
