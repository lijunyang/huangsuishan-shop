<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Pages extends Model
{
    public $table = 'pages';

    public function navigation(){
        return $this->morphMany('App\Models\Navigations', "type", "type", 'type_id');
    }

    protected function filter(array $filter): Builder
    {
        $query = $this->newQuery();

        if($sort = array_get($filter,'sort', 'id')){
            $order = array_get($filter, 'order', 'asc');
            $query->orderBy($sort, $order);
        }

        return $query;
    }
}
