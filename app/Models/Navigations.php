<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

class Navigations extends Model
{
    const TYPE_COLLECTION = 1;
    const TYPE_PAGE = 2;
    const TYPES = [
        self::TYPE_COLLECTION => "列表页",
        self::TYPE_PAGE => "封面页"
    ];
    public $table = 'navigations';

    public function navigable(){
        return $this->morphTo("type", "type", 'type_id');
    }

    public function sonNavigations(){
        return $this->hasMany("App\Models\Navigations", "parent_id");
    }

    protected function filter(array $filter): Builder
    {
        $query = $this->newQuery();

        if($sort = array_get($filter,'sort', 'id')){
            $order = array_get($filter, 'order', 'asc');
            $query->orderBy($sort, $order);
        }

        return $query;
    }

    public static function getTopNavigations(): Collection{
        $top_navigations = self::where("parent_id", 0)->get();
        return $top_navigations;
    }

    public static function getTopNavigationByNavigationId($navigation_id)
    {
        $navigation = self::find($navigation_id);
        $parent_navigation_id = $navigation->parent_id;
        $top_navigation_id = $navigation->id;
        while ($parent_navigation_id !== 0) {
            $parent_navigation = self::getParentNavigationByNavigationId($parent_navigation_id);
            if ($parent_navigation === null){
                $top_navigation_id = $parent_navigation_id;
                $parent_navigation_id = 0;
            }else{
                $parent_navigation_id = $parent_navigation->parent_id;
                $top_navigation_id = $parent_navigation->id;
            }      
        }
        $top_navigation = self::find($top_navigation_id);
        return $top_navigation;
    }
    private static function getParentNavigationByNavigationId($navigation_id)
    {
        $navigation = self::find($navigation_id);
        $parent_navigation = self::find($navigation->parent_id);
        return $parent_navigation;
    }

}
