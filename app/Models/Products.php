<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Products extends Model
{
    public $table = 'products';

    public function collections(){
        return $this->belongsToMany('App\Models\Collections', 'collections_products', 'product_id', 'collection_id');
    }

    protected function filter(array $filter): Builder
    {
        $query = $this->newQuery();
        if ($collection_id = array_get($filter, 'collection_id', '')) {
            $query->whereHas('collections', function($query) use ($collection_id){
                $query->where('collection_id', $collection_id);
            });
        }
        if ($brand = array_get($filter, 'brand', '')) {
            $query->where('brand', 'like', '%' . trim($brand) . '%');
        }
        if($sort = array_get($filter,'sort', 'id')){
            $order = array_get($filter, 'order', 'desc');
            $query->orderBy($sort, $order);
        }
        return $query;
    }
}
