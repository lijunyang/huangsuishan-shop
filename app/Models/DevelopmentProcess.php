<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

use Illuminate\Support\Facades\DB;

class DevelopmentProcess extends Model
{
    public $fillable = ['id','title', 'content', 'sort'];

    protected function filter(array $filter) : Builder
    {
        $query = $this->newQuery();

        if($sort = array_get($filter,'sort', 'sort')){
            $order = array_get($filter, 'order', 'asc');
            $query->orderBy($sort, $order);
        }

        return $query;
    }

    /**
     * 更新或修改数据
     * @param  DevelopmentProcess       $item        [description]
     * @param  array                    $data        [description]
     * @return integer                  [description]
     */
    public static function modify(DevelopmentProcess $item ,$data)
    {
        $id = 0;
        DB::transaction(function () use ($item, $data, &$id) {
            $item->title = $data['title'];
            $item->content = $data['content'];

            if($sort = array_get($data, 'sort','')){
                $item->sort = $sort;
            }else{
                $item->sort = DevelopmentProcess::max('sort') + 1;
            }

            $item->save();

            $id = $item->id;
        });
        return $id;
    }

    protected function updateProcessSort($orders)
    {
        $id = [];

        DB::transaction(function () use ($orders, &$id) {
            foreach ($orders as $order) {
                $banner = DevelopmentProcess::find($order['id']);
                $banner->update(['sort' => $order['sort']]);
                $id[] = $order['id'];
            }
        });

        return $id;
    }
}
