<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class CollectionsProducts extends Model
{

    public $table = 'collections_products';

    public function product(){
        return $this->hasOne("App\Models\Products", 'id', 'product_id');
    }

    public function collection(){
        return $this->hasOne("App\Models\Collections", 'id', 'collection_id');
    }

    public static function updateByProductIdAndCollectionIds($product_id, array $collection_ids): void{
        $product_collections = self::where('product_id', $product_id)->get()->toArray();
        $product_collections_ids = array_column($product_collections, "collection_id");
        $delete_ids = array_diff($product_collections_ids, $collection_ids);
        $add_ids = array_diff($collection_ids, $product_collections_ids);
        self::where('product_id', $product_id)->whereIn("collection_id", $delete_ids)->delete();
        foreach ($add_ids as $collection_id) {
            $collection_product = new self;
            $collection_product->product_id = $product_id;
            $collection_product->collection_id = $collection_id;
            $collection_product->save();
        }
    }
}
