<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Images extends Model
{
    public $table = 'images';

    public $fillable = ['id', 'path', 'created_at', 'updated_at'];
}
