<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    public $table = 'settings';

    public static function findByName($name){
        $settings = self::all();
        foreach ($settings as $setting) {
            if ($setting->name === $name) {
                return $setting;
            }
        }
        return false;
    }

}
