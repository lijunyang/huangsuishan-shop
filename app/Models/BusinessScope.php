<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

class BusinessScope extends Model
{
    public $fillable = ['id','name', 'content', 'type', 'pc_image_id', 'app_image_id'];

    //
    const TYPE_SCOPE = 1;
    const TYPE_AWARD = 2;
    const TYPE_PERSON = 3;

    public function pc_image(): BelongsTo
    {
        return $this->belongsTo(Images::Class, 'pc_image_id');
    }

    public function app_image(): BelongsTo
    {
        return $this->belongsTo(Images::Class, 'app_image_id');
    }

    protected function filter(array $filter) : Builder
    {
        $query = $this->newQuery();

        if($type = array_get($filter, 'type', '')){
            $query->where('type', $type);
        }

        if($sort = array_get($filter,'sort', 'sort')){
            $order = array_get($filter, 'order', 'asc');
            $query->orderBy($sort, $order);
        }

        return $query;
    }

    /**
     * 更新或修改数据
     * @param  BusinessScope       $item        [description]
     * @param  array                    $data        [description]
     * @return integer                  [description]
     */
    public static function modify(BusinessScope $item ,$data)
    {
        $id = 0;
        DB::transaction(function () use ($item, $data, &$id) {
            $item->type = $data['type'];

            $item->name = $data['name'];
            $item->content = $data['content'];
            $item->pc_image_id = $data['pc_image_id'];
            $item->app_image_id = $data['app_image_id'];

            if($data['type'] != BusinessScope::TYPE_PERSON){
                if($sort = array_get($data, 'sort','')){
                    $item->sort = $sort;
                }else{
                    $item->sort = BusinessScope::where('type', $data['type'])->max('sort') + 1;
                }
            }

            $item->save();

            $id = $item->id;
        });
        return $id;
    }

    protected function updateScopeSort($orders, $type)
    {
        $id = [];

        DB::transaction(function () use ($orders, &$id, $type) {
            foreach ($orders as $order) {
                $banner = BusinessScope::find($order['id']);
                if ($banner->type != $type) {
                    throw new \Exception('类型不符合');
                }
                $banner->update(['sort' => $order['sort']]);
                $id[] = $order['id'];
            }
        });

        return $id;
    }
}
