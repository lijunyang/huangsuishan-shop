<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class RecruitItem extends Model
{
    /**
     * 更新或修改数据
     * @param  RecruitItem $item [description]
     * @param  array $data [description]
     * @return integer                  [description]
     */
    public static function modify(RecruitItem $item, $data)
    {

        $id = 0;
        DB::transaction(function () use ($item, $data, &$id) {
            $item->recruit_id = $data['recruit_id'];
            $item->address = $data['address'];
            $item->phone = $data['phone'];
            $item->email = $data['email'];
            $item->contact_people = $data['contact_people'];
            $item->save();

            $id = $item->id;
        });
        return $id;
    }
}
