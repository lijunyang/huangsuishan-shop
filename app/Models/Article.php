<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\DB;

class Article extends Model
{

    const STATUS_ACTIVE = 1;
    const STATUS_ABANDON = 0;

    public static function status_map()
    {
        return [
            self::STATUS_ACTIVE => '开启',
            self::STATUS_ABANDON => '关闭',
        ];
    }

    protected $fillable = ['id', 'description', 'collection_id', 'pc_image_id',
        'app_image_id', 'template_id', 'content', 'status', 'good_count'];

    public function pc_image(): BelongsTo
    {
        return $this->belongsTo(Images::Class, 'pc_image_id');
    }

    public function app_image(): BelongsTo
    {
        return $this->belongsTo(Images::Class, 'app_image_id');
    }

    public function collection(): BelongsTo
    {
        return $this->belongsTo(Collections::Class, 'collection_id');
    }

    public function template(): BelongsTo
    {
        return $this->belongsTo(Templates::Class, 'template_id');
    }

    protected function filter(array $filter): Builder
    {
        $query = $this->newQuery();

        $date = '';
        if ($year = array_get($filter, 'year', '')) {
            $date .= $year;
        }

        if ($month = array_get($filter, 'month', '')) {
            $date .= '-' . $month;
        }

        if (!empty($date)) {
            $query->where('created_at', 'like', '%' . $date . '%');
        }

        if ($title = array_get($filter, 'title', '')) {
            $query->where('title', 'like', '%' . trim($title) . '%');
        }

        if ($collection_id = array_get($filter, 'collection_id', '')) {
            $query->where('collection_id', $collection_id);
        }

        if (($status = array_get($filter, 'status', '*')) !== '*') {
            $query->where('status', $status);
        }
        if ($sort = array_get($filter, 'sort', 'id')) {
            $order = array_get($filter, 'order', 'asc');
            $query->orderBy($sort, $order);
        }

        return $query;
    }

    /**
     * 更新或修改数据
     * @param  Article $item [description]
     * @param  array $data [description]
     * @return integer                  [description]
     */
    public static function modify(Article $item, $data)
    {

        $id = 0;
        DB::transaction(function () use ($item, $data, &$id) {

            $item->title = $data['title'];
            $item->description = $data['description'];
            $item->collection_id = $data['collection_id'];
            $item->pc_image_id = $data['pc_image_id'];
            $item->app_image_id = $data['app_image_id'];
            $item->template_id = $data['template_id'];
            $item->content = $data['content'];
            if (($status = array_get($data, 'status', '*')) === '*') {
                $item->status = self::STATUS_ACTIVE;
            } else {
                $item->status = $status;
            }
            $item->good_count = $data['good_count'];
            $item->save();

            $id = $item->id;
        });
        return $id;
    }


}
