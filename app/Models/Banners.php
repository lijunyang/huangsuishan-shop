<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

class Banners extends Model
{

    public $table = "banners";
    
    protected function filter(array $filter) : Builder
    {
        $query = $this->newQuery();

        if($sort = array_get($filter,'sort', 'sort')){
            $order = array_get($filter, 'order', 'asc');
            $query->orderBy($sort, $order);
        }

        return $query;
    }

}