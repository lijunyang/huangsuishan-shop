<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

class BannerAndKeyword extends Model
{

    public $fillable = ['id', 'type', 'pc_image_id', 'app_image_id'];

    //
    const TYPE_BANNER = 1;
    const TYPE_KEYWORD = 2;

    public function pc_image(): BelongsTo
    {
        return $this->belongsTo(Images::Class, 'pc_image_id');
    }

    public function app_image(): BelongsTo
    {
        return $this->belongsTo(Images::Class, 'app_image_id');
    }

    protected function filter(array $filter) : Builder
    {
        $query = $this->newQuery();

        if($type = array_get($filter, 'type', '')){
            $query->where('type', $type);
        }

        if($sort = array_get($filter,'sort', 'sort')){
            $order = array_get($filter, 'order', 'asc');
            $query->orderBy($sort, $order);
        }

        return $query;
    }

    /**
     * 更新或修改数据
     * @param  array       $data        [description]
     * @return integer                  [description]
     */
    public static function modify(BannerAndKeyword $item ,$data)
    {
        $id = 0;
        DB::transaction(function () use ($item, $data, &$id) {
            $item->type = $data['type'];

            $item->pc_image_id = $data['pc_image_id'];
            $item->app_image_id = $data['app_image_id'];

            if($sort = array_get($data, 'sort','')){
                $item->sort = $sort;
            }else{
                $item->sort = BannerAndKeyword::where('type', $data['type'])->max('sort') + 1;
            }

            $item->save();

            $id = $item->id;
        });
        return $id;
    }

    protected function updateBannerSort($orders, $type)
    {
        $id = [];

        DB::transaction(function () use ($orders, &$id, $type) {
            foreach ($orders as $order) {
                $banner = BannerAndKeyword::find($order['id']);
                if ($banner->type != $type) {
                    throw new \Exception('类型不符合');
                }
                $banner->update(['sort' => $order['sort']]);
                $id[] = $order['id'];
            }
        });

        return $id;
    }
}
