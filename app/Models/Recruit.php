<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\DB;

class Recruit extends Model
{

    const STATUS_ACTIVE = 1;
    const STATUS_ABANDON = 0;

    const AREA_GZ = 1;
    const AREA_SZ = 2;
    const AREA_BJ = 3;
    const AREA_XA = 4;

    public static function area_map(){
        return [
            self::AREA_GZ => '广州',
            self::AREA_SZ => '深圳',
            self::AREA_BJ => '北京',
            self::AREA_XA => '西安',
        ];
    }

    public static function status_map()
    {
        return [
            self::STATUS_ACTIVE => '开启',
            self::STATUS_ABANDON => '关闭',
        ];
    }

    protected $appends = ['deliver_number'];

    public function items(): HasMany
    {
        return $this->HasMany(RecruitItem::class, 'recruit_id');
    }

    public function getDeliverNumberAttribute()
    {
        return count($this->items);
    }

    protected $fillable = ['id', 'area_id', 'position', 'employee_email', 'status',
        'duty', 'require', 'address', 'phone', 'email', 'contact_people'];


    protected function filter(array $filter): Builder
    {
        $query = $this->newQuery();

        if ($position = array_get($filter, 'position', '')) {
            $query->where('position', 'like', '%' . trim($position) . '%');
        }

        if ($area_id = array_get($filter, 'area_id', '')) {
            $query->where('area_id', $area_id);
        }

        if (($status = array_get($filter, 'status', '*')) !== '*') {
            $query->where('status', $status);
        }

        if ($sort = array_get($filter, 'sort', 'id')) {
            $order = array_get($filter, 'order', 'asc');
            $query->orderBy($sort, $order);
        }

        return $query;
    }

    /**
     * 更新或修改数据
     * @param  Recruit $item [description]
     * @param  array $data [description]
     * @return integer                  [description]
     */
    public static function modify(Recruit $item, $data)
    {

        $id = 0;
        DB::transaction(function () use ($item, $data, &$id) {

            $item->area_id = $data['area_id'];
            $item->position = $data['position'];
            $item->employee_email = $data['employee_email'];
            $item->duty = serialize( $data['duty'] );
            $item->require = serialize( $data['require'] );
//            $item->address = $data['address'];
//            $item->phone = $data['phone'];
//            $item->email = $data['email'];
//            $item->contact_people = $data['contact_people'];
            if (($status = array_get($data, 'status', '*')) === '*') {
                $item->status = self::STATUS_ACTIVE;
            } else {
                $item->status = $status;
            }
            $item->save();

            $id = $item->id;
        });
        return $id;
    }

}
