<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Authenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->check()){
            if (Auth::user()->role === 'role_admin'){
                return $next($request);
            }else{
                return response('权限不足!');
            }
            
        }
        return redirect(route('auth.login.index'));
    }
}
