<?php

namespace App\Http\Services;

use Illuminate\Http\UploadedFile;

class ImagesService implements ImagesServiceInterface
{

    const IMAGE_DIR = "images";

    public static function generateUniqName(UploadedFile $file)
    {
        $original_extension = $file->getClientOriginalExtension();
        $uniq_name = uniqid() . "." . $original_extension;
        return $uniq_name;
    }

}