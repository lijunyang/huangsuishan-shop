<?php

namespace App\Http\Services;

use Illuminate\Http\UploadedFile;

Interface ImagesServiceInterface{

    public static function generateUniqName(UploadedFile $file);

}