<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddArticlePost extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules(): array
    {
        return [
            "title" => 'required',
            "status" => 'required|integer',
            "goog_count" => 'integer',
            "description" => 'required',
            "content" => 'required',
            "collection_id" => 'required'
        ];
    }

    public function messages(): array
    {
        return [
            'title.required' => '标题不能为空！',
            'status.required' => '状态不能为空！',
            'status.integer' => '状态值只能为整数！',
            'goog_count.integer' => '点赞数只能为整数！',
            'description.required' => '描述不能为空！',
            'content.required' => '内容不能为空！',
            'collection_id.required' => '列表不能为空！',
        ];
    }
}
