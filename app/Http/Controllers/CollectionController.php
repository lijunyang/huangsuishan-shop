<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\AddCollectionPost;
use App\Models\Collections;
use Illuminate\Http\Request;
use App\Models\Templates;

class CollectionController extends Controller
{

    public function index()
    {
        return view('admin.collections.index');
    }

    public function add()
    {
        $templates = Templates::all();
        return view('admin.collections.add', [
            'templates' => $templates,
        ]);
    }

    public function addPost(AddCollectionPost $request)
    {
        $collection = new Collections;
        $collection->title = $request->input("title");
        $collection->template_id = $request->input('template_id');
        if ($request->filled("description")){
            $collection->description = $request->input("description");
        }
        if ($request->filled("pc_image_id")){
            $collection->pc_image_id = $request->input("pc_image_id");
        }
        if ($request->filled("app_image_id")){
            $collection->app_image_id = $request->input("app_image_id");
        }
        $collection->save();
        return redirect()->route('admin.collections.index')->with('status', '添加成功！');
    }

    public function destroy($id)
    {
        Collections::where('id', $id)->delete();
        return redirect()->route("admin.collections.index");
    }

    public function update($id)
    {
        $collection = Collections::find($id);
        $templates = Templates::all();
        return view('admin.collections.add', [
            'templates' => $templates,
            'collection' => $collection,
        ]);
    }

    public function updatePost(Request $request, $id)
    {
        $collection = Collections::find($id);
        $collection->title = $request->input("title");
        $collection->template_id = $request->input('template_id');
        $collection->description = $request->input("description");
        $collection->pc_image_id = $request->input("pc_image_id");
        $collection->app_image_id = $request->input("app_image_id");
        $collection->save();
        return redirect()->route("admin.collections.index")->with('status', '更新成功！');
    }
}