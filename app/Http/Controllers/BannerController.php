<?php

namespace App\Http\Controllers;

use App\Models\Banners;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Validator;

class BannerController extends Controller
{
    public function index()
    {
        return view('admin.banners.index');
    }

    public function add()
    {
        return view('admin.banners.add');
    }

    public function addPost(Request $request)
    {
        $validator = $this->addPostValidation($request);
        if ($validator->fails()){
            $redirect_url = url()->previous();
            return redirect($redirect_url)->withErrors($validator->errors()->all());
        }

        $banner = new Banners;
        $banner->image_id = $request->input("image_id");
        $banner->url = $request->input("url");

        if ($request->filled("sort")){
            $banner->sort = $request->input("sort");
        }
        $banner->save();
        return redirect()->route('admin.banners.index')->with('status', '添加成功！');
    }

    private function addPostValidation(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "image_id" => 'required',
            "url" => 'required',
        ],[
            'image_id.required' => '图片不能为空！',
            'url.required' => '跳转链接不能为空！',
        ]);
        return $validator;
    }

    public function destroy($id)
    {
        Banners::where('id', $id)->delete();
        return redirect()->route("admin.banners.index");
    }

    public function update($id)
    {
        $banner = Banners::find($id);
        return view('admin.banners.add', [
            'banner' => $banner,
        ]);
    }

    public function updatePost(Request $request, $id)
    {
        $banner = Banners::find($id);
        $banner->image_id = $request->input("image_id");
        $banner->url = $request->input("url");

        if ($request->filled("sort")){
            $banner->sort = $request->input("sort");
        }
        $banner->save();
        return redirect()->route("admin.banners.index")->with('status', '更新成功！');
    }
}