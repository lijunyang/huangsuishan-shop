<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Models\Settings;
use Illuminate\Support\Facades\View;
use App\Models\Navigations;
use Illuminate\Support\Facades\DB;
use App\Models\Collections;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        $this->initView();
        $this->limit = 20;
    }
    private function initView()
    {
        $settings = Settings::all();
        foreach ($settings as $setting) {
            View::share($setting->name, $setting->value);
        }
        $navigations = Navigations::all();
        View::share("navigations", $navigations);

        $collections = Collections::all();
        View::share("collections", $collections);

        $products_navigations = Navigations::where("type", Navigations::TYPE_COLLECTION)->where("parent_id", "!=", 0)->get();
        View::share('products_navigations', $products_navigations);
    }
}
