<?php

namespace App\Http\Controllers;

use App\Models\Images;
use Illuminate\Http\Request;
use App\Http\Services\ImagesService;

class ImageController extends Controller
{

    public function getImage($id)
    {
        $image = Images::find($id);
        return redirect($image->path);
    }

    public function upload(Request $request)
    {
        $upload_file = $request->file("file");
        $uniq_name = ImagesService::generateUniqName($upload_file);
        $path = $upload_file->storeAS(ImagesService::IMAGE_DIR, $uniq_name);

        $images = new Images;
        $images->path = $path;
        $images->save();

        return response()->json($images);
    }

}

