<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Requests\LoginPost;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    protected $retirect_to = '/admin';

    public function index()
    {
        if(Auth::check()){
            return redirect()->intended($this->retirect_to);
        }
        $previous = url()->previous();
        return view('admin.login.index', [
            'previous' => $previous,
            'login_type' => "admin"
        ]);
    }
    public function frontLogin()
    {
        if(Auth::check()){
            return redirect()->intended($this->retirect_to);
        }
        $previous = url()->previous();
        return view('admin.login.index', [
            'previous' => $previous,
            'login_type' => "front"
        ]);
    }
    public function frontLoginPost(LoginPost $request)
    {
        $is_authed = Auth::attempt([
            'name' => $request->input('name'), 
            'password' => $request->input('password')
        ]);
        if ($is_authed){
            $user = Auth::user();
            $redirect_url = $request->input('previous');
            return redirect($redirect_url);
        }
        return redirect(route('auth.login.index'))->with('error', "账号或密码错误！");
    }

    public function auth(LoginPost $request)
    {
        $is_authed = Auth::attempt([
            'name' => $request->input('name'), 
            'password' => $request->input('password')
        ]);
        if ($is_authed){
            $user = Auth::user();
            $redirect_url = $request->input('previous');
            return redirect($redirect_url);
        }
        return redirect(route('auth.front.login'))->with('error', "name or password error！");
    }

    public function logout()
    {
        Auth::logout();
        return redirect(route('front.index'));
    }

}
