<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Products;
use Validator;
use App\Models\CollectionsProducts;

class ProductController extends Controller
{

    public function index()
    {
        return view('admin.products.index');
    }

    public function add()
    {
        $products = Products::all();
        return view('admin.products.add', [
            'products' => $products
        ]);
    }

    public function addPost(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "title" => 'required',
            "number" => 'required',
            "price" => 'required',
            "image_ids" => 'required',
            "content" => 'required',
        ],[
            'title.required' => '标题不能为空！',
            'price.required' => "商品价格不能为空！",
            'content.required' => '内容不能为空！',
            'number.required' => '商品数量不能为空!',
            'image_ids.required' => "图片集不能为空！"
        ]);
        if ($validator->fails()){
            $redirect_url = url()->previous();
            return redirect($redirect_url)->withErrors($validator->errors()->all());
        }

        $product = new Products;
        $product->title = $request->input("title");
        $product->number = $request->input("number");
        if ($request->filled("brand")){
            $product->brand = $request->input("brand");
        }
        if ($request->filled("type")){
            $product->type = $request->input("type");
        }
        $product->price = $request->input("price");
        $product->image_ids = $request->input("image_ids");
        $product->content = $request->input("content");
        $product->save();
        $collection_ids = $request->input("collection_ids") ?: [];
        CollectionsProducts::updateByProductIdAndCollectionIds($product->id, $collection_ids);
        return redirect()->route('admin.products.index')->with('status', '添加成功！');
    }

    public function destroy($id)
    {
        Product::where('id', $id)->delete();
        return redirect()->route("admin.products.index");
    }

    public function update($id)
    {
        $product = Products::find($id);
        $product_collections = CollectionsProducts::where("product_id", $product->id)->get()->toArray();
        $collection_ids = array_column($product_collections, "collection_id");
        $images = explode(',', $product->image_ids);
        return view('admin.products.add', [
            'product' => $product,
            'images' => $images,
            'collection_ids' => $collection_ids,
        ]);
    }

    public function updatePost(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            "title" => 'required',
            "number" => 'required',
            "price" => 'required',
            "image_ids" => 'required',
            "content" => 'required',
        ],[
            'title.required' => '标题不能为空！',
            'price.required' => "商品价格不能为空！",
            'content.required' => '内容不能为空！',
            'number.required' => '商品数量不能为空!',
            'image_ids.required' => "图片集不能为空！"
        ]);
        if ($validator->fails()){
            $redirect_url = url()->previous();
            return redirect($redirect_url)->withErrors($validator->errors()->all());
        }
        $product = Products::find($id);
        $product->title = $request->input("title");
        $product->number = $request->input("number");
        $product->brand = $request->input("brand");
        $product->type = $request->input("type");
        $product->price = $request->input("price");
        $product->image_ids = $request->input("image_ids");
        $product->content = $request->input("content");
        $product->save();
        $collection_ids = $request->input("collection_ids") ?: [];
        CollectionsProducts::updateByProductIdAndCollectionIds($product->id, $collection_ids);
        return redirect()->route("admin.products.index")->with('status', '更新成功！');
    }
}