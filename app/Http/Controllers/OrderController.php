<?php

namespace App\Http\Controllers;

use App\Exports\OrdersExport;
use App\Models\Orders;
use Excel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller {

	public function index() {
		return view('admin.orders.index');
	}
	public function frontIndex($status) {
		return view('admin.orders.front_index', [
			'status' => $status,
		]);
	}
	public function confirm($id) {
		$order = Orders::find($id);
		$order->status = Orders::STATUS_CONFIRM;
		$order->save();
		return response()->json([
			'order' => $order,
		]);
	}
	public function multiConfirm(Request $request) {
		$ids = $request->input("ids");
		$orders = Orders::whereIn('id', $ids)->update(['status' => Orders::STATUS_CONFIRM]);
		return response()->json([]);
	}
	public function add() {
		$orders = Orders::all();
		return view('admin.orders.add', [
			'orders' => $orders,
		]);
	}

	public function addPost(Request $request) {
		$user = Auth::user();
		$order = new Orders;
		$order->user_id = $user->id;
		$order->product_id = $request->input('product_id');
		$order->telephone = $user->telephone;
		$order->province = $user->province;
		$order->city = $user->city;
		$order->district = $user->district;
		$order->detail_address = $user->detail_address;
		$order->email = $user->email;
		$order->zip_code = $user->zip_code;
		$order->purchase_number = $request->input("purchase_number");
		$order->comment = $request->input("comment");
		$order->save();
		$redirect_url = url()->previous();
		return redirect($redirect_url)->with('status', '下单成功!');
	}

	public function destroy($id) {
		Order::where('id', $id)->delete();
		return redirect()->route("admin.orders.index");
	}

	public function frontUpdate($id) {
		$order = Orders::find($id);
		return view('admin.orders.add', [
			'order' => $order,
		]);
	}
	public function update($id) {
		$order = Orders::find($id);
		return view('admin.orders.add', [
			'order' => $order,
		]);
	}

	public function updatePost(Request $request, $id) {
		$order = Orders::find($id);
		$order->telephone = $request->input("telephone");
		$order->province = $request->input("province");
		$order->city = $request->input("city");
		$order->district = $request->input("district");
		$order->detail_address = $request->input("detail_address");
		$order->email = $request->input("email");
		$order->zip_code = $request->input("zip_code");
		$order->purchase_number = $request->input("purchase_number");
		$order->comment = $request->input("comment");
		$order->save();
		if (Auth::user()->role === "role_user") {
			return redirect()->route("front.orders.index")->with('status', 'Success！');
		} else {
			return redirect()->route("admin.orders.index")->with('status', '更新成功！');
		}

	}

	public function exportAll() {

		$orders = Orders::where("status", Orders::STATUS_CONFIRM)->get();
		return Excel::download(new OrdersExport($orders), 'orders.xlsx');
	}

	public function export(Request $request) {
		$order_ids = $request->input("ids");
		$orders = Orders::where("status", Orders::STATUS_CONFIRM)->whereIn("id", explode(',', $order_ids))->get();
		return Excel::download(new OrdersExport($orders), 'orders.xlsx');
	}
}