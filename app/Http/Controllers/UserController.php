<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Users;
use Validator;

class userController extends Controller
{

    public function index()
    {
        return view('admin.users.index');
    }

    public function destroy($id)
    {
        user::where('id', $id)->delete();
        return redirect()->route("admin.users.index");
    }

    public function update($id)
    {
        $user = Users::find($id);
        $images = explode(',', $user->image_ids);
        return view('admin.users.add', [
            'user' => $user,
        ]);
    }

    public function updatePost(Request $request, $id)
    {
        $user = Users::find($id);
        $user->name = $request->input("name");
        $user->email = $request->input("email");
        $user->role = $request->input("role");
        $user->password = bcrypt($request->input("password"));
        $user->save();
        return redirect()->route("admin.users.index")->with('status', '更新成功！');
    }
}