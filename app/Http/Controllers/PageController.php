<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pages;
use Validator;

class PageController extends Controller
{

    public function index()
    {
        return view('admin.pages.index');
    }

    public function add()
    {
        $pages = Pages::all();
        return view('admin.pages.add', [
            'pages' => $pages
        ]);
    }

    public function addPost(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "title" => 'required',
            "description" => 'required',
            "content" => 'required',
        ],[
            'title.required' => '标题不能为空！',
            'description.required' => '描述不能为空！',
            'content.required' => '内容不能为空！',
        ]);
        if ($validator->fails()){
            $redirect_url = url()->previous();
            return redirect($redirect_url)->withErrors($validator->errors()->all());
        }

        $page = new Pages;
        $page->title = $request->input("title");
        $page->description = $request->input("description");
        if ($request->filled("pc_image_id")){
            $page->pc_image_id = $request->input("pc_image_id");
        }
        if ($request->filled("app_image_id")){
            $page->app_image_id = $request->input("app_image_id");
        }
        $page->content = $request->input("content");
        $page->save();
        return redirect()->route('admin.pages.index')->with('status', '添加成功！');
    }

    public function destroy($id)
    {
        Page::where('id', $id)->delete();
        return redirect()->route("admin.pages.index");
    }

    public function update($id)
    {
        $page = Pages::find($id);
        return view('admin.pages.add', [
            'page' => $page,
        ]);
    }

    public function updatePost(Request $request, $id)
    {
        $page = Pages::find($id);
        $page->title = $request->input("title");
        $page->description = $request->input("description");
        $page->pc_image_id = $request->input("pc_image_id");
        $page->app_image_id = $request->input("app_image_id");
        $page->content = $request->input("content");
        $page->save();
        return redirect()->route("admin.pages.index")->with('status', '更新成功！');
    }
}