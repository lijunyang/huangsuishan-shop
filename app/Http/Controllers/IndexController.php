<?php

namespace App\Http\Controllers;

use App\Models\Banners;
use App\Models\Collections;
use App\Models\CollectionsProducts;
use App\Models\Images;
use App\Models\Navigations;
use App\Models\Pages;
use App\Models\Products;
use App\Models\Settings;
use App\Models\Users;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;

class IndexController extends Controller {
	const DEFAULT_PAGE_SIZE = 18;

	public function index() {
		$collection_first_setting = Settings::findByName("collection_first");
		$collection_first = Collections::find($collection_first_setting->value);
		$collection_second_setting = Settings::findByName("collection_second");
		$collection_second = Collections::find($collection_second_setting->value);
		$about_us_setting = Settings::findByName("about_us");
		$about_us = Pages::find($about_us_setting->value);
		$banners = Banners::all();
		return view('front.index', [
			'collection_first' => $collection_first,
			'collection_second' => $collection_second,
			'about_us' => $about_us,
			'banners' => $banners,
		]);
	}
	public function search($key_words) {
		$products = Products::where("title", 'like', '%' . $key_words . "%")->get();
		return view('front.search', [
			'products' => $products,
		]);
	}

	public function register() {
		return view('front.register');
	}

	public function userAdd(Request $request) {
		$validator = Validator::make($request->all(), [
			"name" => 'required',
			"email" => 'required',
			"password" => 'required',
			"telephone" => 'required',
			"province" => 'required',
			"city" => 'required',
			"district" => 'required',
			"detail_address" => 'required',
			"zip_code" => 'required',
		], [
			'name.required' => '用户名不能为空！',
			'email.required' => '邮箱不能为空！',
			'password.required' => '密码不能为空！',
			'telephone.required' => '联系电话不能为空！',
			'province.required' => '省份不能为空！',
			'city.required' => '城市不能为空！',
			'district.required' => '区县不能为空！',
			'detail_address.required' => '详细地址不能为空！',
			'zip_code.required' => '邮编不能为空！',
		]);
		if ($validator->fails()) {
			$redirect_url = url()->previous();
			return redirect($redirect_url)->withErrors($validator->errors()->all());
		}

		$user = new Users;
		$user->name = $request->input("name");
		$user->email = $request->input("email");
		$user->password = bcrypt($request->input("password"));
		$user->telephone = $request->input("telephone");
		$user->province = $request->input("province");
		$user->city = $request->input("city");
		$user->district = $request->input("district");
		$user->detail_address = $request->input("detail_address");
		$user->zip_code = $request->input("zip_code");
		$user->save();
		Auth::login($user, true);
		return redirect(route('front.index'));
	}

	public function showImage($id) {
		$image = Images::find($id);
		$path = storage_path('app') . "/" . $image->path;
		return response()->file($path);
	}

	public function collection(Request $request, $collection_id, $navigation_id = null) {
		$page_size = $request->input("limit", self::DEFAULT_PAGE_SIZE);
		$current_collection = Collections::find($collection_id);
		$current_navigation = Navigations::find($navigation_id);
		$collection_products = CollectionsProducts::where("collection_id", $collection_id)->paginate($page_size);
		$top_navigation = Navigations::getTopNavigationByNavigationId($current_navigation->id);
		$allow_filter_navigations = Navigations::whereIn('id', explode(",", env("ALLOW_FILTER_IDS")))->get();
		return view('front.collection', [
			'current_collection' => $current_collection,
			'collection_products' => $collection_products,
			'top_navigation' => $top_navigation,
			'allow_filter_navigations' => $allow_filter_navigations,
			'current_navigation' => $current_navigation,
		]);
	}

	public function navigation($id) {
		$navigation = Navigations::find($id);
		if ($navigation->type === Navigations::TYPE_COLLECTION) {
			$return_collection_id = $navigation->navigable->id;
			$return_navigation_id = $navigation->id;
			//如果是顶级导航且有子导航则跳转到第一个子导航
			if ($navigation->parent_id === 0 && count($navigation->sonNavigations) !== 0) {
				$return_collection_id = $navigation->sonNavigations->first()->navigable->id;
				$return_navigation_id = $navigation->sonNavigations->first()->id;
			}
			return redirect(route('front.collection', [
				'collection_id' => $return_collection_id,
				'navigation_id' => $return_navigation_id,
			]));
		} else {
			return redirectroute('front.page', [
				'page_id' => $navigation->navigable->id,
			]);
		}
	}

	public function recentlyProducts() {
		$products = Products::where('created_at', '>', date('Y-m-d H:i:s', strtotime('-30 days', time())))->orderBy('created_at', 'desc')->paginate(self::DEFAULT_PAGE_SIZE);
		return view('front.recently_products', [
			'products' => $products,
		]);
	}

	public function page($page_id) {
		$current_page = Pages::find($page_id);
		return view('front.page', [
			'current_page' => $current_page,
		]);
	}

	public function product($product_id) {
		$current_product = Products::find($product_id);
		$images = explode(',', $current_product->image_ids);
		$related_products = [];
		$related_collections = $current_product->collections;
		$related_collections_id = array_column($related_collections->toArray(), "id");
		$related_collections_products = CollectionsProducts::whereIn('collection_id', $related_collections_id)->limit(8)->get();
		$user = Auth::user();

		return view("front.product", [
			'current_product' => $current_product,
			'images' => $images,
			'related_collections_products' => $related_collections_products,
			'user' => $user,
		]);
	}

	public function showSonNavigations($parent_navigation_id) {
		$navigation = Navigations::find($parent_navigation_id);
		return view("front.all-navigations", [
			'son_navigations' => $navigation->sonNavigations,
		]);
	}

}
