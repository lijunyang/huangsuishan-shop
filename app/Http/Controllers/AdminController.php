<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Pages;
use App\Models\Collections;
use App\Models\Settings;

class AdminController extends Controller
{

    public function index()
    {
        return view('admin.layouts.main');
    }
    public function settings()
    {
        $pages = Pages::all();
        $collections = Collections::all();
        return view('admin.settings', [
            'pages' => $pages,
            'collections' => $collections
        ]);
    }

    public function settingsPost(Request $request)
    {
        $site_name = $request->input('site_name');
        $settings_site_name = Settings::where('name', 'site_name')->first();
        $settings_site_name->value = $site_name;
        $settings_site_name->save();

        $about_us = $request->input('about_us');
        $settings_about_us = Settings::where('name', 'about_us')->first();
        $settings_about_us->value = $about_us;
        $settings_about_us->save();

        $collection_first = $request->input("collection_first");
        $settings_collection_first = Settings::where('name', 'collection_first')->first();
        $settings_collection_first->value = $collection_first;
        $settings_collection_first->save();

        $collection_second = $request->input("collection_second");
        $settings_collection_second = Settings::where('name', 'collection_second')->first();
        $settings_collection_second->value = $collection_second;
        $settings_collection_second->save();

        $logo = $request->input("logo");
        $settings_logo = Settings::where('name', 'logo')->first();
        $settings_logo->value = $logo;
        $settings_logo->save();

        $video = $request->input("video");
        $settings_video = Settings::where('name', 'video')->first();
        $settings_video->value = $video;
        $settings_video->save();

        $footer_description = $request->input("footer_description");
        $settings_footer_description = Settings::where('name', 'footer_description')->first();
        $settings_footer_description->value = $footer_description;
        $settings_footer_description->save();

        return redirect()->route('admin.settings')->with('status', '更新成功！');
    }
}