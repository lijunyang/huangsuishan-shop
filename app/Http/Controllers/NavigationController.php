<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Navigations;
use Validator;
use App\Models\Collections;
use App\Models\Pages;

class NavigationController extends Controller
{

    public function index()
    {
        $collections = Collections::all();
        $pages = Pages::all();
        $navigations = Navigations::all();
        return view('admin.navigations.index', [
            'collections' => $collections,
            'pages' => $pages,
            'navigations' => $navigations
        ]);
    }

    public function add()
    {
        $navigations = Navigations::getTopNavigations();
        return view('admin.navigations.add', [
            'navigations' => $navigations
        ]);
    }

    public function addPost(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "title" => 'required',
            "type" => 'required',
            "type_id" => 'required',
        ],[
            'title.required' => '标题不能为空！',
            'type.required' => '请选择类型！',
            'type_id.required' => '请选择关联页面！',
        ]);
        if ($validator->fails()){
            $redirect_url = url()->previous();
            return redirect($redirect_url)->withErrors($validator->errors()->all());
        }

        $navigation = new Navigations;
        $navigation->title = $request->input("title");
        $navigation->type = $request->input("type");
        $navigation->type_id = $request->input("type_id");
        $navigation->parent_id = $request->input("parent_id");
        $navigation->sort_order = $request->input("sort_order")?: 1;
        $navigation->save();
        return redirect()->route('admin.navigations.index')->with('status', '添加成功！');
    }

    public function destroy($id)
    {
        Navigation::where('id', $id)->delete();
        return redirect()->route("admin.navigations.index");
    }

    public function update($id)
    {
        $navigation = Navigations::find($id);
        $navigations = Navigations::all();
        $collections = Collections::all();
        $pages = Pages::all();
        return view('admin.navigations.add', [
            'current_navigation' => $navigation,
            'navigations' => $navigations,
            'collections' => $collections,
            'pages' => $pages
        ]);
    }

    public function updatePost(Request $request, $id)
    {
        $navigation = Navigations::find($id);
        $navigation->title = $request->input("title");
        $navigation->type = $request->input("type");
        $navigation->type_id = $request->input("type_id");
        $navigation->parent_id = $request->input("parent_id");
        $navigation->sort_order = $request->input("sort_order");
        $navigation->save();
        return redirect()->route("admin.navigations.index")->with('status', '更新成功！');
    }
}