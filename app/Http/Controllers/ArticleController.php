<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddArticlePost;
use Illuminate\Http\Request;
use App\Models\Article;
use App\Models\Collections;
use Validator;

class ArticleController extends Controller
{

    public function index()
    {
        return view('admin.articles.index');
    }

    public function add()
    {
        $collections = Collections::all();
        return view('admin.articles.add', [
            'collections' => $collections
        ]);
    }

    public function addPost(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "title" => 'required',
            "status" => 'required|integer',
            "good_count" => 'required|integer',
            "description" => 'required',
            "content" => 'required',
            "collection_id" => 'required'
        ],[
            'title.required' => '标题不能为空！',
            'status.required' => '状态不能为空！',
            'status.integer' => '状态值只能为整数！',
            'good_count.required' => '点赞数不能为空！',
            'good_count.integer' => '点赞数只能为整数！',
            'description.required' => '描述不能为空！',
            'content.required' => '内容不能为空！',
            'collection_id.required' => '列表不能为空！',
        ]);
        if ($validator->fails()){
            $redirect_url = url()->previous();
            return redirect($redirect_url)->withErrors($validator->errors()->all());
        }

        $article = new Article;
        $article->title = $request->input("title");
        $article->status = $request->input("status");
        $article->collection_id = $request->input('collection_id');
        $article->good_count = $request->input("good_count");
        $article->description = $request->input("description");
        if ($request->filled("pc_image_id")){
            $article->pc_image_id = $request->input("pc_image_id");
        }
        if ($request->filled("app_image_id")){
            $article->app_image_id = $request->input("app_image_id");
        }
        $article->content = $request->input("content");
        $article->save();
        return redirect()->route('admin.articles.index')->with('status', '添加成功！');
    }

    public function destroy($id)
    {
        Article::where('id', $id)->delete();
        return redirect()->route("admin.articles.index");
    }

    public function update($id)
    {
        $article = Article::find($id);
        $collections = Collections::all();
        return view('admin.articles.add', [
            'article' => $article,
            'collections' => $collections
        ]);
    }

    public function updatePost(Request $request, $id)
    {
        $article = Article::find($id);
        $article->title = $request->input("title");
        $article->status = $request->input("status");
        $article->collection_id = $request->input('collection_id');
        $article->good_count = $request->input("good_count");
        $article->description = $request->input("description");
        $article->pc_image_id = $request->input("pc_image_id");
        $article->app_image_id = $request->input("app_image_id");
        $article->content = $request->input("content");
        $article->save();
        return redirect()->route("admin.articles.index")->with('status', '更新成功！');
    }
}