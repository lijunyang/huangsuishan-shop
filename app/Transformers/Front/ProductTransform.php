<?php

namespace App\Transformers\Front;
use App\Models\Products;
use League\Fractal\TransformerAbstract;

class ProductTransform extends TransformerAbstract
{
    public function transform(Products $product)
    {
        return [
            'id' => $product->id,
            'title' => $product->title,
            'brand' => $product->brand,
            'number' => $product->number,
            'type' => $product->type,
            'price' => $product->price,
            'image_ids' => $product->image_ids,
            'content' => $product->content,
        ];
    }
}