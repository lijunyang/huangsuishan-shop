<?php
/**
 * Created by PhpStorm.
 * User: eaon
 * Date: 2019-09-17
 * Time: 11:33
 */

namespace App\Transformers\Front;
use App\Models\Article;
use League\Fractal\TransformerAbstract;
use App\Models\BannerAndKeyword;

class ArticleTransform extends TransformerAbstract
{
    public function transform(Article $article)
    {
        return [
            'id'                => $article->id,
            'title'             => $article->title,
            'description'       => $article->description,
            'collection_id'     => $article->collection_id,
            'collection'        => $article->collection,
            'pc_image_id'       => $article->pc_image_id,
            'pc_image'          => $article->pc_image,
            'app_image_id'      => $article->app_image_id,
            'app_image'         => $article->app_image,
            'content'           => $article->content,
            'good_count'        => $article->good_count,
            'created_at'        => $article->created_at->toDateTimeString()
        ];
    }
}