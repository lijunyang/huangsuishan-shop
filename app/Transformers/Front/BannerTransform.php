<?php
/**
 * Created by PhpStorm.
 * User: eaon
 * Date: 2019-09-17
 * Time: 11:33
 */

namespace App\Transformers\Front;
use League\Fractal\TransformerAbstract;
use App\Models\BannerAndKeyword;

class BannerTransform extends TransformerAbstract
{
    public function transform(BannerAndKeyword $banner)
    {
        return [
            'id'          => $banner->id,
            'pc_image_id'        => $banner->pc_image_id,
            'pc_image'       => $banner->pc_image,
            'app_image_id'        => $banner->app_image_id,
            'app_image'        => $banner->app_image,
        ];
    }
}