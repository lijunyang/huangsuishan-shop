<?php
/**
 * Created by PhpStorm.
 * User: eaon
 * Date: 2019-09-17
 * Time: 11:33
 */

namespace App\Transformers\Admin;
use App\Models\BusinessScope;
use League\Fractal\TransformerAbstract;
use App\Models\BannerAndKeyword;

class BusinessScopeTransform extends TransformerAbstract
{
    public function transform(BusinessScope $businessScope)
    {
        return [
            'id'          => $businessScope->id,
            'name'       => $businessScope->name,
            'content'       => $businessScope->content,
            'pc_image_id'        => $businessScope->pc_image_id,
            'pc_image'       => $businessScope->pc_image,
            'app_image_id'        => $businessScope->app_image_id,
            'app_image'        => $businessScope->app_image,
            'sort'          =>$businessScope->sort
        ];
    }
}