<?php
/**
 * Created by PhpStorm.
 * User: eaon
 * Date: 2019-09-17
 * Time: 11:33
 */

namespace App\Transformers\Admin;
use League\Fractal\TransformerAbstract;
use App\Models\Banners;

class BannerTransform extends TransformerAbstract
{
    public function transform(Banners $banner)
    {
        return [
            'id' => $banner->id,
            'image_id' => $banner->image_id,
            'url' => $banner->url,
            'sort' => $banner->sort,
            'created_at' => $banner->created_at->toDateTimeString(),
            'updated_at' => $banner->updated_at->toDateTimeString(),
        ];
    }
}