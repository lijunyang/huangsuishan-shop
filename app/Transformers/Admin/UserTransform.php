<?php

namespace App\Transformers\Admin;

use App\Models\Users;
use League\Fractal\TransformerAbstract;

class userTransform extends TransformerAbstract
{
    public function transform(Users $user)
    {
        return [
            'id'         => $user->id,
            'name'       => $user->name,
            'role'       => $user->role,
            'email'      => $user->email,
            'created_at' => $user->created_at->toDateTimeString(),
            'updated_at' => $user->updated_at->toDateTimeString(),
        ];
    }
}
