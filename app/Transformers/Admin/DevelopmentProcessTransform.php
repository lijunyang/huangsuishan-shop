<?php
/**
 * Created by PhpStorm.
 * User: eaon
 * Date: 2019-09-17
 * Time: 11:33
 */

namespace App\Transformers\Admin;
use App\Models\BusinessScope;
use App\Models\DevelopmentProcess;
use League\Fractal\TransformerAbstract;
use App\Models\BannerAndKeyword;

class DevelopmentProcessTransform extends TransformerAbstract
{
    public function transform(DevelopmentProcess $developmentProcess)
    {
        return [
            'id'          => $developmentProcess->id,
            'title'       => $developmentProcess->title,
            'content'       => $developmentProcess->content,
            'sort'       => $developmentProcess->sort
        ];
    }
}