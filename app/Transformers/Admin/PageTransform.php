<?php

namespace App\Transformers\Admin;

use App\Models\Pages;
use League\Fractal\TransformerAbstract;

class PageTransform extends TransformerAbstract
{

    public function transform(Pages $page)
    {
        return [
            'id' => $page->id,
            'title' => $page->title,
            'description' => $page->description,
            'pc_image_id' => $page->pc_image_id,
            'app_image_id' => $page->app_image_id,
            'content' => $page->content,
            'created_at' => $page->created_at->toDateTimeString(),
            'updated_at' => $page->updated_at->toDateTimeString(),
        ];
    }

}