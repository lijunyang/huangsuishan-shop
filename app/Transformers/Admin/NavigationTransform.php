<?php

namespace App\Transformers\Admin;

use App\Models\Navigations;
use League\Fractal\TransformerAbstract;

class NavigationTransform extends TransformerAbstract
{

    public function transform(Navigations $navigation)
    {
        return [
            'id' => $navigation->id,
            'title' => $navigation->title,
            'type' => $navigation->type,
            'type_id' => $navigation->type_id,
            'parent_id' => $navigation->parent_id,
            'sort_order' => $navigation->sort_order,
            'created_at' => $navigation->created_at->toDateTimeString(),
            'updated_at' => $navigation->updated_at->toDateTimeString(),
        ];
    }

}