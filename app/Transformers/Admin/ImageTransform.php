<?php
/**
 * Created by PhpStorm.
 * User: eaon
 * Date: 2019-09-17
 * Time: 11:33
 */

namespace App\Transformers\Admin;
use App\Models\Images;
use League\Fractal\TransformerAbstract;

class ImageTransform extends TransformerAbstract
{
    public function transform(Images $image)
    {
        return [
            'id'          => $image->id,
            'path'        => $image->path,
            'created_at'       => $image->created_at->toDateTimeString(),
            'updated_at'       => $image->updated_at->toDateTimeString(),
        ];
    }
}