<?php
/**
 * Created by PhpStorm.
 * User: eaon
 * Date: 2019-09-17
 * Time: 11:33
 */

namespace App\Transformers\Admin;
use App\Models\Article;
use App\Models\Recruit;
use League\Fractal\TransformerAbstract;
use App\Models\BannerAndKeyword;

class RecruitTransform extends TransformerAbstract
{
    public function transform(Recruit $recruit)
    {
        return [
            'id'                => $recruit->id,
            'area_id'           => $recruit->area_id,
            'area_name'         =>  Recruit::area_map()[$recruit->area_id],
            'position'          => $recruit->position,
            'employee_email'    => $recruit->employee_email,
            'duty'              => unserialize( $recruit->duty),
            'require'           => unserialize($recruit->require),
            'status'            => $recruit->status,
            'status_name'       => Recruit::status_map()[$recruit->status],
            'deliver_number'    => $recruit->deliver_number,
            'items'             => $recruit->items,
        ];
    }
}