<?php

namespace App\Transformers\Admin;

use App\Models\Orders;
use League\Fractal\TransformerAbstract;

class orderTransform extends TransformerAbstract
{
    public function transform(Orders $order)
    {
        return [
            'id' => $order->id,
            'user' => $order->user,
            'product' => $order->product,
            'telephone' => $order->telephone,
            'province' => $order->province,
            'city' => $order->city,
            'district' => $order->district,
            'detail_address' => $order->detail_address,
            'email' => $order->email,
            'zip_code' => $order->zip_code,
            'purchase_number' => $order->purchase_number,
            'comment' => $order->comment,
            'status' => $order->status,
            'created_at' => $order->created_at->toDateTimeString(),
            'updated_at' => $order->updated_at->toDateTimeString(),
        ];
    }
}
