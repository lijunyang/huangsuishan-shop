<?php

namespace App\Transformers\Admin;

use App\Models\Collections;
use League\Fractal\TransformerAbstract;

class CollectionTransform extends TransformerAbstract
{

    public function transform(Collections $collection)
    {
        return [
            'id' => $collection->id,
            'title' => $collection->title,
            'description' => $collection->description,
            'pc_image_id' => $collection->pc_image_id,
            'app_image_id' => $collection->app_image_id,
            'template_id' => $collection->template_id,
            'created_at' => $collection->created_at->toDateTimeString(),
            'updated_at' => $collection->updated_at->toDateTimeString(),
        ];
    }

}