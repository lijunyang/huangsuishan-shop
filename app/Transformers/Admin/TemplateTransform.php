<?php

namespace App\Transformers\Admin;

use App\Models\Templates;
use League\Fractal\TransformerAbstract;

class TemplateTransform extends TransformerAbstract
{

    public function transform(Templates $templates)
    {
        return [
            'id' => $templates->id,
            'path' => $templates->title,
        ];
    }

}