<?php
/**
 * Created by PhpStorm.
 * User: eaon
 * Date: 2019-09-17
 * Time: 11:33
 */

namespace App\Transformers\Admin;

use App\Models\Article;
use League\Fractal\TransformerAbstract;

class ArticleTransform extends TransformerAbstract
{
    public function transform(Article $article)
    {
        return [
            'id'            => $article->id,
            'title'         => $article->title,
            'description'   => $article->description,
            'collection_id' => $article->collection_id,
            'collection'    => $article->collection,
            'pc_image_id'   => $article->pc_image_id,
            'pc_image'      => $article->pc_image,
            'app_image_id'  => $article->app_image_id,
            'app_image'     => $article->app_image,
            'template_id'   => $article->template_id,
            'template'      => $article->template,
            'content'       => $article->content,
            'status'        => $article->status,
            'status_name'   => Article::status_map()[$article->status],
            'good_count'    => $article->good_count,
            'created_at'    => $article->created_at->toDateTimeString(),
            'updated_at'    => $article->updated_at->toDateTimeString(),
        ];
    }
}
