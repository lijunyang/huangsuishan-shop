<?php

namespace App\Exports;

use App\Models\Orders;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithDrawings;
use Maatwebsite\Excel\Concerns\FromArray;
use Storage;
use App\Models\Images;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class OrdersExport implements FromCollection, WithMapping, WithHeadings, WithEvents
{

    protected $orders;

    public function __construct($orders)
    {
        $this->orders = $orders;
    }

    public function collection()
    {
        return $this->orders;
    }

    public function map($orders): array
    {
        return [
            $orders->user->name,
            "",
            $orders->telephone,
            $orders->detail_address,
            $orders->comment,
            $orders->product->id,
            $orders->product->title,
            $orders->purchase_number,
            $orders->created_at,
        ];
    }

    public function headings(): array
    {
        return [
            '客户名称',
            '产品图片',
            '客户电话&邮箱',
            '客户详细地址',
            '需求说明',
            '款号',
            '产品名称',
            '购买数量',
            '订单时间',
        ];
    }
    public function registerEvents(): array
    {
        return [ AfterSheet::class => function(AfterSheet $event){
            //设置垂直居中
            $event->sheet->getDelegate()->getStyle('A1:I' . (count($this->orders) + 1))->getAlignment()->setVertical('center');
            //设置表头
            $event->sheet->getDelegate()->getRowDimension(1)->setRowHeight(30);
            $event->sheet->getDelegate()->getStyle('A1:I1')->applyFromArray([
                'font' => [
                    'bold' => true,
                    'color' => [
                        'rgb' => '6eaad7'
                    ]
                ],
            ]);
            //设置列宽
            $event->sheet->getDelegate()->getColumnDimension('A')->setWidth(12);
            $event->sheet->getDelegate()->getColumnDimension('B')->setWidth(20);
            $event->sheet->getDelegate()->getColumnDimension('C')->setWidth(30);
            $event->sheet->getDelegate()->getColumnDimension('D')->setWidth(30);
            $event->sheet->getDelegate()->getColumnDimension('E')->setWidth(30);
            $event->sheet->getDelegate()->getColumnDimension('F')->setWidth(15);
            $event->sheet->getDelegate()->getColumnDimension('G')->setWidth(20);
            $event->sheet->getDelegate()->getColumnDimension('H')->setWidth(10);
            $event->sheet->getDelegate()->getColumnDimension('I')->setWidth(20);
            
            foreach ($this->orders as $key => $order) {
                $row_index = $key + 2;    
                $this->setRowImages($event, $order, $row_index);
                $event->sheet->getDelegate()->getRowDimension($row_index)->setRowHeight(100);
            }
        }];
    }
    private function setRowImages($event, $order, $row_index): void{
        $image_id = explode(',', $order->product->image_ids)[0];
        $image_path = Images::find($image_id)->path;
        $image_path = storage_path() . "/app/" . $image_path;
        $this->setImage2Excel($event, 'B' . $row_index , $image_path, 0, 90);
    }
    private function setImage2Excel($event, $position, $path, $width,$height){
        $drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
        $drawing->setName('Logo');
        $drawing->setDescription('Logo');
        $drawing->setCoordinates($position);
        $drawing->setPath($path);
        ($width==0)?null:$drawing->setWidth($width);
        ($height==0)?null:$drawing->setHeight($height);
        $drawing->setWorksheet($event->sheet->getDelegate());
    }
}
