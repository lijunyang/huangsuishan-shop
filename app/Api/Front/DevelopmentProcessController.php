<?php

namespace App\Api\Front;

use App\Api\ApiController as Controller;
use App\Models\DevelopmentProcess;
use App\Transformers\Front\DevelopmentProcessTransform;
use Illuminate\Http\Request;
use Exception;

class DevelopmentProcessController extends Controller
{
    public function index(){
        request()->validate(
            [
                'sort' => 'string',
                'order' => 'string',
            ]
        );
        $post = request()->all();
        $banners = DevelopmentProcess::filter($post)->get();
        return $this->response->collection($banners, new DevelopmentProcessTransform());
    }

}
