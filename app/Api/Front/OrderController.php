<?php

namespace App\Api\Front;

use App\Api\ApiController as Controller;
use App\Models\Orders;
use App\Transformers\Admin\OrderTransform;
use Illuminate\Http\Request;

class OrderController extends Controller {

	public function get($user_id, $status) {
		$post = request()->all();
		$orders = Orders::where('user_id', $user_id)->where('status', $status)->get();
		$order_transform = new OrderTransform();
		return $this->response->collection($orders, $order_transform);
	}

	public function destroy($id) {
		$ids = explode(',', $id);
		$product_counts = Orders::whereIn('id', $ids)->count();

		if ($product_counts != count($ids)) {
			return response()->json(['message' => 'request parameter is not validated'], 406);
		}

		Orders::whereIn('id', $ids)->delete();
		return response()->json(['msg' => '删除成功'], 200);
	}

	public function getAll() {
		$orders = Orders::all();
		return response()->json($orders);
	}

}