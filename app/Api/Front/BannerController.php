<?php

namespace App\Api\Front;

use App\Transformers\Front\BannerTransform;
use Exception;
use App\Api\ApiController as Controller;
use App\Models\BannerAndKeyword;
use Illuminate\Http\Request;

class BannerController extends Controller
{

    public function index(){
        request()->validate(
            [
                'sort' => 'string',
                'order' => 'string',
            ]
        );
        $post = request()->all();
        $post['type'] = BannerAndKeyword::TYPE_BANNER;
        $banners = BannerAndKeyword::filter($post)->with('pc_image', 'app_image')->get();
        return $this->response->collection($banners, new BannerTransform());
    }
}