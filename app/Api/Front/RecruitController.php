<?php

namespace App\Api\Front;

use App\Api\ApiController as Controller;
use App\Models\Recruit;
use App\Models\RecruitItem;
use App\Transformers\Front\PersonTransform;
use App\Transformers\Front\RecruitTransform;

class RecruitController extends Controller
{
    public function index(){
        request()->validate(
            [
                'limit' => 'int|min:1|max:200',
                'offset' => 'int',
                'sort' => 'string',
                'order' => 'string',
                'area_id' => 'int',
                'position' => 'string',
                'status' => 'int',
            ]
        );
        $post = request()->all();
        $post['status'] = Recruit::STATUS_ACTIVE;
        $recruits = Recruit::filter($post)
            ->paginate(request()->get('limit', 20));
        return $this->response->paginator($recruits, new RecruitTransform());
    }

    public function delivery($recruit_id){
        request()->validate(
            [
                'address' => 'required|string',
                'phone' => 'required|',
                'email' => 'required|email',
                'contact_people' => 'required|string',
            ]
        );
        $post = request()->all();
        $post['recruit_id'] = $recruit_id;
        RecruitItem::modify(new RecruitItem(),$post);
        return response()->json(['message' => '投递成功'], 201);
    }
}
