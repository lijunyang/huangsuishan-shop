<?php

namespace App\Api\Front;

use App\Api\ApiController as Controller;
use App\Models\Article;
use App\Transformers\Front\ArticleTransform;
use Exception;

class ArticleController extends Controller
{
    public function index(){
        request()->validate(
            [
                'limit' => 'int|min:1|max:200',
                'page' => 'int',
                'sort' => 'string',
                'order' => 'string',
                'year'  =>  'int|min:2000|max:9999',
                'month' =>  'int|min:1|max:12',
                'collection_id'  => 'int',
                'navigation_id'  => 'int'
            ]
        );
        try {
            $post = request()->all();
            $this->validatePost($post);
        } catch (Exception $ex) {
            return response()->json(['message' => $ex->getMessage()], 406);
        }
        $articles = Article::filter($post)->with('pc_image', 'app_image', 'collection', 'template')
            ->paginate(request()->get('limit', 20));
        return $this->response->paginator($articles, new ArticleTransform());
    }

    public function home(){
        return $this->response->collection(
            Article::limit(5)->orderby('created_at', 'desc')->get(),
            new ArticleTransform()
        );
    }

    public function show($id){
        $article = Article::find($id);
        if (empty($article)) {
            return response()->json(['error' => 'request parameter is not validated'], 406);
        }
        return $this->response
            ->item($article, new ArticleTransform());
    }

    protected function validatePost($post){
        if(empty($post['year']) && !empty($post['month'])){
            throw new \Exception(['message'=>'不能在没有选择年份的时候 选择月份']);
        }
    }

    public function good($id){
        $article = Article::find($id);
        if (empty($article)) {
            return response()->json(['error' => 'request parameter is not validated'], 406);
        }
        $article->increment('good_count');
        return $this->response
            ->item($article, new ArticleTransform());
    }

}
