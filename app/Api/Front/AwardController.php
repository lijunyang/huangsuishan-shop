<?php

namespace App\Api\Front;

use App\Models\BusinessScope;
use App\Transformers\Front\AwardTransform;
use Exception;
use App\Api\ApiController as Controller;
use App\Models\BannerAndKeyword;
use Illuminate\Http\Request;

class AwardController extends Controller
{

    public function index(){
        request()->validate(
            [
                'sort' => 'string',
                'order' => 'string',
            ]
        );
        $post = request()->all();
        $post['type'] = BusinessScope::TYPE_AWARD;
        $banners = BusinessScope::filter($post)->with('pc_image', 'app_image')->get();
        return $this->response->collection($banners, new AwardTransform());
    }
}