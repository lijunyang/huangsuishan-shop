<?php 

namespace App\Api\Front;

use App\Api\ApiController as Controller;
use App\Models\Products;
use App\Transformers\Front\ProductTransform;

class ProductController extends Controller
{
    public function index(){
        request()->validate([
            'limit' => 'int|min:1|max:200',
            'page' => 'int',
            'collections_id' => 'string',
        ]);
        $post = request()->all();
        $filter_collections_id_array = json_decode(request()->query("collections_id"));
        $products = Products::filter($post);
        foreach ($filter_collections_id_array as $collections_id_array) {
            if (count($collections_id_array) !== 0) {
                $products = $products->whereHas('collections', function($query) use ($collections_id_array){
                    $query->whereIn("collection_id", $collections_id_array);
                });
            }    
        }
        $products = $products->paginate(request()->get('limit', 18));
        $product_transform = new ProductTransform();
        return $this->response->paginator($products, $product_transform);
    }

}
