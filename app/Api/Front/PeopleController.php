<?php

namespace App\Api\Front;

use App\Api\ApiController as Controller;
use App\Models\BusinessScope;
use App\Transformers\Front\PersonTransform;
use Exception;

class PeopleController extends Controller
{
    public function index(){
        request()->validate(
            [
                'limit' => 'int|min:1|max:200',
                'page' => 'int',
                'sort' => 'string',
                'order' => 'string',
            ]
        );
        $post = request()->all();
        $post['type'] = BusinessScope::TYPE_PERSON;
        $banners = BusinessScope::filter($post)->with('pc_image', 'app_image')
            ->paginate(request()->get('limit', 20));
        return $this->response->paginator($banners, new PersonTransform());
    }
}
