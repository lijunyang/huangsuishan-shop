<?php

namespace App\Api\Front;

use App\Models\BusinessScope;
use App\Transformers\Admin\BusinessScopeTransform;
use Illuminate\Http\Request;
use App\Api\ApiController as Controller;
use Exception;

class BusinessScopeController extends Controller
{
    public function index(){
        request()->validate(
            [
                'sort' => 'string',
                'order' => 'string',
            ]
        );
        $post = request()->all();
        $post['type'] = BusinessScope::TYPE_SCOPE;
        $banners = BusinessScope::filter($post)->with('pc_image', 'app_image')->get();
        return $this->response->collection($banners, new BusinessScopeTransform());
    }

}
