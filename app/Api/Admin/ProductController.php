<?php

namespace App\Api\Admin;

use App\Api\ApiController as Controller;
use Illuminate\Http\Request;
use App\Models\Products;
use App\Transformers\Admin\ProductTransform;


class ProductController extends Controller
{

    public function get()
    {
        request()->validate(
            [
                'limit' => 'int|min:1|max:200',
                'offset' => 'int',
                'sort' => 'string',
                'order' => 'string',
            ]
        );
        $post = request()->all();
        request()->offsetSet('page', floor($post['offset'] / $post['limit']) + 1 );
        $pages = Products::filter($post)->paginate(request()->get('limit', 20));
        $product_transform = new ProductTransform();
        return $this->response->paginator($pages, $product_transform);
    }

    public function destroy($id){
        $ids = explode(',', $id);
        $product_counts = Products::whereIn('id', $ids)->count();

        if ($product_counts != count($ids)) {
            return response()->json(['message' => 'request parameter is not validated'], 406);
        }

        Products::whereIn('id', $ids)->delete();
        return response()->json(['msg' => '删除成功'], 200);
    }

    public function getAll(){
        $pages = Products::all();
        return response()->json($pages);
    }

}