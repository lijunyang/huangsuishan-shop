<?php

namespace App\Api\Admin;

use App\Api\ApiController as Controller;
use Illuminate\Http\Request;
use App\Models\Navigations;
use App\Transformers\Admin\NavigationTransform;


class NavigationController extends Controller
{

    public function get()
    {
        request()->validate(
            [
                'limit' => 'int|min:1|max:200',
                'offset' => 'int',
                'sort' => 'string',
                'order' => 'string',
            ]
        );
        $post = request()->all();
        request()->offsetSet('page', floor($post['offset'] / $post['limit']) + 1 );
        $pages = Navigations::filter($post)->paginate(request()->get('limit', 20));
        $navigation_transform = new NavigationTransform();
        return $this->response->paginator($pages, $navigation_transform);
    }

    public function destroy($id){
        $ids = explode(',', $id);
        $navigation_counts = Navigations::whereIn('id', $ids)->count();

        if ($navigation_counts != count($ids)) {
            return response()->json(['message' => 'request parameter is not validated'], 406);
        }

        Navigations::whereIn('id', $ids)->delete();
        return response()->json(['msg' => '删除成功'], 200);
    }

    public function getSubNavigations($id){
        $sub_navigations = Navigations::where('parent_id', $id)->get();
        return response()->json($sub_navigations);
    }

}