<?php

namespace App\Api\Admin;

use App\Api\ApiController as Controller;
use Illuminate\Http\Request;
use App\Models\Collections;
use App\Transformers\Admin\CollectionTransform;
use App\Models\Navigations;


class CollectionController extends Controller
{

    public function get()
    {
        request()->validate(
            [
                'limit' => 'int|min:1|max:200',
                'offset' => 'int',
                'sort' => 'string',
                'order' => 'string',
            ]
        );
        $post = request()->all();
        request()->offsetSet('page', floor($post['offset'] / $post['limit']) + 1 );
        // $filter_navigations_id = explode(",", env("ALLOW_FILTER_IDS"));
        // $filter_collections_id = [];
        // foreach ($filter_navigations_id as $navigation_id) {
        //     $collection_id = Navigations::find($navigation_id)->navigable->id;
        //     $filter_collections_id[] = $collection_id;
        // }
        $collections = Collections::filter($post)->paginate(request()->get('limit', 20));
        $collection_transform = new CollectionTransform();
        return $this->response->paginator($collections, $collection_transform);
    }

    public function destroy($id){
        $ids = explode(',', $id);
        $collection_counts = Collections::whereIn('id', $ids)->count();

        if ($collection_counts != count($ids)) {
            return response()->json(['message' => 'request parameter is not validated'], 406);
        }

        Collections::whereIn('id', $ids)->delete();
        return response()->json(['msg' => '删除成功'], 200);
    }

    public function getAll(){
        $collections = Collections::all();
        return response()->json($collections);
    }

}