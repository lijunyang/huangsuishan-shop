<?php

namespace App\Api\Admin;

use App\Models\BusinessScope;
use App\Transformers\Admin\BusinessScopeTransform;
use Illuminate\Http\Request;
use App\Api\ApiController as Controller;
use Exception;

class BusinessScopeController extends Controller
{
    public function index(){
        request()->validate(
            [
                'limit' => 'int|min:1|max:200',
                'offset' => 'int',
                'sort' => 'string',
                'order' => 'string',
            ]
        );
        $post = request()->all();
        $post['type'] = BusinessScope::TYPE_SCOPE;
        request()->offsetSet('page', floor($post['offset'] / $post['limit']) + 1 );
        $banners = BusinessScope::filter($post)->with('pc_image', 'app_image')
            ->paginate(request()->get('limit', 20));
        return $this->response->paginator($banners, new BusinessScopeTransform());
    }

    public function show($id){
        $banner = BusinessScope::where('type', BusinessScope::TYPE_SCOPE)->find($id);
        if (empty($banner)) {
            return response()->json(['error' => 'request parameter is not validated'], 406);
        }
        return $this->response
            ->item($banner, new BusinessScopeTransform());
    }

    public function create(){
        request()->validate(
            [
                'name' => 'required|string',
                'content' => 'required|string',
                'pc_image_id' => 'required|int',
                'app_image_id' => 'required|int'
            ]
        );
        try {
            $post = request()->all();
            $post['type'] = BusinessScope::TYPE_SCOPE;
            $this->validateScopePost($post);
        } catch (Exception $ex) {
            return response()->json(['message' => $ex->getMessage()], 406);
        }
        $id = BusinessScope::modify(new BusinessScope(), $post);
        return $this->response->item(BusinessScope::find($id), new BusinessScopeTransform());
    }

    public function update($id){
        request()->validate(
            [
                'name' => 'required|string',
                'content' => 'required|string',
                'pc_image_id' => 'required|int',
                'app_image_id' => 'required|int',
                'sort' => 'required|int',
            ]
        );
        try {
            $post = request()->all();
            $post['type'] = BusinessScope::TYPE_SCOPE;
            $this->validateScopePost($post);
        } catch (Exception $ex) {
            return response()->json(['message' => $ex->getMessage()], 406);
        }
        $id = BusinessScope::modify(BusinessScope::find($id), $post);
        return $this->response->item(BusinessScope::find($id), new BusinessScopeTransform());
    }

    public function destroy($id){
        $ids = explode(',', $id);
        $banner_counts = BusinessScope::whereIn('id', $ids)->count();

        if ($banner_counts != count($ids)) {
            return response()->json(['message' => 'request parameter is not validated'], 406);
        }

        BusinessScope::whereIn('id', $ids)->delete();
        return response()->json(['msg' => '删除成功'], 200);
    }

    public function sort(){
        request()->validate(
            [
                'order' => 'required|array',
                'order.*.id' => 'required|int',
                'order.*.sort' => 'required|int',
            ]
        );
        $ids = BusinessScope::updateScopeSort(request()->all()['order'], BusinessScope::TYPE_SCOPE);
        return $this->response
            ->collection(BusinessScope::whereIn('id', $ids)->get(), new BusinessScopeTransform());
    }

    private function validateScopePost($post){

    }
}
