<?php

namespace App\Api\Admin;

use App\Api\ApiController as Controller;
use Illuminate\Http\Request;
use App\Models\Templates;


class TemplateController extends Controller
{

    public function get(){
        $templates = Templates::all();
        return response()->json($templates);
    }

}