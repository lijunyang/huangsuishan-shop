<?php

namespace App\Api\Admin;

use App\Transformers\Admin\BannerTransform;
use Exception;
use App\Api\ApiController as Controller;
use App\Models\Banners;
use Illuminate\Http\Request;

class BannerController extends Controller
{

    public function index(){
        request()->validate(
            [
                'limit' => 'int|min:1|max:200',
                'offset' => 'int',
                'sort' => 'string',
                'order' => 'string',
            ]
        );
        $post = request()->all();
        request()->offsetSet('page', floor($post['offset'] / $post['limit']) + 1 );
        $banners = Banners::filter($post)->paginate(request()->get('limit', 20));
        return $this->response->paginator($banners, new BannerTransform());
    }

    public function show($id){
        $banner = Banners::find($id);
        if (empty($banner)) {
            return response()->json(['error' => 'request parameter is not validated'], 406);
        }
        return $this->response
            ->item($banner, new BannerTransform());
    }

    public function create(){
        request()->validate(
            [
                'pc_image_id' => 'required|int',
                'app_image_id' => 'required|int',
            ]
        );
        try {
            $post = request()->all();
            $post['type'] = Banners::TYPE_BANNER;
            $this->validateBannerPost($post);
        } catch (Exception $ex) {
            return response()->json(['message' => $ex->getMessage()], 406);
        }
        $id = Banners::modify(new Banners(), $post);
        return $this->response->item(Banners::find($id), new BannerTransform());
    }

    public function update($id){
        request()->validate(
            [
                'pc_image_id' => 'required|int',
                'app_image_id' => 'required|int',
                'sort' => 'required|int',
            ]
        );
        try {
            $post = request()->all();
            $post['type'] = Banners::TYPE_BANNER;
            $this->validateBannerPost($post);
        } catch (Exception $ex) {
            return response()->json(['message' => $ex->getMessage()], 406);
        }
        $id = Banners::modify(Banners::find($id), $post);
        return $this->response->item(Banners::find($id), new BannerTransform());
    }

    public function destroy($id){
        $ids = explode(',', $id);
        $banner_counts = Banners::whereIn('id', $ids)->count();

        if ($banner_counts != count($ids)) {
            return response()->json(['message' => 'request parameter is not validated'], 406);
        }

        Banners::whereIn('id', $ids)->delete();
        return response()->json(['msg' => '删除成功'], 200);
    }

    public function sort(){
        request()->validate(
            [
                'order' => 'required|array',
                'order.*.id' => 'required|int',
                'order.*.sort' => 'required|int',
            ]
        );
        $ids = Banners::updateBannerSort(request()->all()['order'], Banners::TYPE_BANNER);
        return $this->response
            ->collection(Banners::whereIn('id', $ids)->get(), new BannerTransform());
    }

    private function validateBannerPost($post){

    }
}