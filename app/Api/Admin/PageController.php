<?php

namespace App\Api\Admin;

use App\Api\ApiController as Controller;
use Illuminate\Http\Request;
use App\Models\Pages;
use App\Transformers\Admin\PageTransform;


class PageController extends Controller
{

    public function get()
    {
        request()->validate(
            [
                'limit' => 'int|min:1|max:200',
                'offset' => 'int',
                'sort' => 'string',
                'order' => 'string',
            ]
        );
        $post = request()->all();
        request()->offsetSet('page', floor($post['offset'] / $post['limit']) + 1 );
        $pages = Pages::filter($post)->paginate(request()->get('limit', 20));
        $collection_transform = new PageTransform();
        return $this->response->paginator($pages, $collection_transform);
    }

    public function destroy($id){
        $ids = explode(',', $id);
        $collection_counts = Pages::whereIn('id', $ids)->count();

        if ($collection_counts != count($ids)) {
            return response()->json(['message' => 'request parameter is not validated'], 406);
        }

        Pages::whereIn('id', $ids)->delete();
        return response()->json(['msg' => '删除成功'], 200);
    }

    public function getAll(){
        $pages = Pages::all();
        return response()->json($pages);
    }

}