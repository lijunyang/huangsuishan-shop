<?php

namespace App\Api\Admin;

use App\Api\ApiController as Controller;
use App\Models\Article;
use App\Transformers\Admin\ArticleTransform;
use Illuminate\Http\Request;
use Exception;

class ArticleController extends Controller
{
    public function index(){
        request()->validate(
            [
                'limit' => 'int|min:1|max:200',
                'offset' => 'int',
                'sort' => 'string',
                'order' => 'string',
            ]
        );
        $post = request()->all();
        request()->offsetSet('page', floor($post['offset'] / $post['limit']) + 1 );
        $articles = Article::filter($post)->with('pc_image', 'app_image', 'collection', 'template')
            ->paginate(request()->get('limit', 20));
        return $this->response->paginator($articles, new ArticleTransform());
    }

    public function show($id){
        $article = Article::find($id);
        if (empty($article)) {
            return response()->json(['error' => 'request parameter is not validated'], 406);
        }
        return $this->response
            ->item($article, new ArticleTransform());
    }

    public function create(){
        request()->validate(
            [
                'title' => 'required|string',
                'description' => 'required|string',
                'collection_id' => 'required|int',
                'pc_image_id' => 'required|int',
                'app_image_id' => 'required|int',
                'template_id' => 'required|int',
                'content' => 'required|string',
                'good_count' => 'required|int',
            ]
        );
        try {
            $post = request()->all();
            $this->validatePost($post);
        } catch (Exception $ex) {
            return response()->json(['message' => $ex->getMessage()], 406);
        }
        $id = Article::modify(new Article(), $post);
        return $this->response->item(Article::find($id), new ArticleTransform());
    }

    public function update($id){
        request()->validate(
            [
                'title' => 'required|string',
                'description' => 'required|string',
                'collection_id' => 'required|int',
                'pc_image_id' => 'required|int',
                'app_image_id' => 'required|int',
                'template_id' => 'required|int',
                'content' => 'required|string',
                'status' => 'required|int',
                'good_count' => 'required|int',
            ]
        );
        try {
            $post = request()->all();
            $this->validatePost($post);
        } catch (Exception $ex) {
            return response()->json(['message' => $ex->getMessage()], 406);
        }
        $id = Article::modify(Article::find($id), $post);
        return $this->response->item(Article::find($id), new ArticleTransform());
    }

    public function destroy($id){
        $ids = explode(',', $id);
        $article_counts = Article::whereIn('id', $ids)->count();

        if ($article_counts != count($ids)) {
            return response()->json(['message' => 'request parameter is not validated'], 406);
        }

        Article::whereIn('id', $ids)->delete();
        return response()->json(['msg' => '删除成功'], 200);
    }

    public function status($id, $status){
        $article = Article::find($id);
        if (empty($article)) {
            return response()->json(['error' => 'request parameter is not validated'], 406);
        }
        $article->update(['status'=>$status]);
        return $this->response
            ->item($article, new ArticleTransform());
    }

    private function validatePost($post){

    }
}
