<?php

namespace App\Api\Admin;

use App\Transformers\Admin\BannerTransform;
use Exception;
use App\Api\ApiController as Controller;
use App\Models\BannerAndKeyword;
use Illuminate\Http\Request;

class KeywordController extends Controller
{

    public function index(){
        request()->validate(
            [
                'limit' => 'int|min:1|max:200',
                'offset' => 'int',
                'sort' => 'string',
                'order' => 'string',
            ]
        );
        $post = request()->all();
        $post['type'] = BannerAndKeyword::TYPE_KEYWORD;
        request()->offsetSet('page', floor($post['offset'] / $post['limit']) + 1 );
        $keywords = BannerAndKeyword::filter($post)->with('pc_image', 'app_image')
            ->paginate(request()->get('limit', 20));
        return $this->response->paginator($keywords, new BannerTransform());
    }

    public function show($id){
        $keyword = BannerAndKeyword::find($id);
        if (empty($keyword)) {
            return response()->json(['error' => 'request parameter is not validated'], 406);
        }
        return $this->response
            ->item($keyword, new BannerTransform());
    }

    public function create(){
        request()->validate(
            [
                'pc_image_id' => 'required|int',
                'app_image_id' => 'required|int',
            ]
        );
        try {
            $post = request()->all();
            $post['type'] = BannerAndKeyword::TYPE_KEYWORD;
            $this->validateBannerPost($post);
        } catch (Exception $ex) {
            return response()->json(['message' => $ex->getMessage()], 406);
        }
        $id = BannerAndKeyword::modify(new BannerAndKeyword(), $post);
        return $this->response->item(BannerAndKeyword::find($id), new BannerTransform());
    }

    public function update($id){
        request()->validate(
            [
                'pc_image_id' => 'required|int',
                'app_image_id' => 'required|int',
            ]
        );
        try {
            $post = request()->all();
            $post['type'] = BannerAndKeyword::TYPE_KEYWORD;
            $this->validateBannerPost($post);
        } catch (Exception $ex) {
            return response()->json(['message' => $ex->getMessage()], 406);
        }
        $id = BannerAndKeyword::modify(BannerAndKeyword::find($id), $post);
        return $this->response->item(BannerAndKeyword::find($id), new BannerTransform());
    }

    public function destroy($id){
        $ids = explode(',', $id);
        $keyword_counts = BannerAndKeyword::whereIn('id', $ids)->count();

        if ($keyword_counts != count($ids)) {
            return response()->json(['message' => 'request parameter is not validated'], 406);
        }

        BannerAndKeyword::whereIn('id', $ids)->delete();
        return response()->json(['msg' => '删除成功'], 200);
    }

    public function sort(){
        request()->validate(
            [
                'order' => 'required|array',
                'order.*.id' => 'required|int',
                'order.*.sort' => 'required|int',
            ]
        );
        $ids = BannerAndKeyword::updateBannerSort(request()->all()['order'], BannerAndKeyword::TYPE_KEYWORD);
        return $this->response
            ->collection(BannerAndKeyword::whereIn('id', $ids)->get(), new BannerTransform());
    }

    private function validateBannerPost($post){

    }
}