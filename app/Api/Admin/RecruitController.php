<?php

namespace App\Api\Admin;

use App\Api\ApiController as Controller;
use App\Models\Recruit;
use App\Transformers\Admin\RecruitTransform;
use Illuminate\Http\Request;
use Exception;

class RecruitController extends Controller
{
    public function index(){
        request()->validate(
            [
                'limit' => 'int|min:1|max:200',
                'offset' => 'int',
                'sort' => 'string',
                'order' => 'string',
                'area_id' => 'int',
                'position' => 'string',
                'status' => 'int',
            ]
        );
        $post = request()->all();
        request()->offsetSet('page', floor($post['offset'] / $post['limit']) + 1 );
        $recruits = Recruit::filter($post)
            ->paginate(request()->get('limit', 20), ['*'], 'offset');
        return $this->response->paginator($recruits, new RecruitTransform());
    }

    public function show($id){
        $recruit = Recruit::find($id);
        if (empty($recruit)) {
            return response()->json(['error' => 'request parameter is not validated'], 406);
        }
        return $this->response
            ->item($recruit, new RecruitTransform());
    }

    public function create(){
        request()->validate(
            [
                'area_id' => 'required|int',
                'position' => 'required|string',
                'employee_email' => 'required|string',
                'status' => 'required|int',
                'duty' => 'required|array',
                'require' => 'required|array',
//                'address' => 'required|string',
//                'phone' => 'required|string',
//                'email' => 'required|string',
//                'contact_people' => 'required|string',
            ]
        );
        try {
            $post = request()->all();
            $this->validatePost($post);
        } catch (Exception $ex) {
            return response()->json(['message' => $ex->getMessage()], 406);
        }
        $id = Recruit::modify(new Recruit(), $post);
        return $this->response->item(Recruit::find($id), new RecruitTransform());
    }

    public function update($id){
        request()->validate(
            [
                'area_id' => 'required|int',
                'position' => 'required|string',
                'employee_email' => 'required|string',
                'status' => 'required|int',
                'duty' => 'required|array',
                'require' => 'required|array',
//                'address' => 'required|string',
//                'phone' => 'required|string',
//                'email' => 'required|string',
//                'contact_people' => 'required|string',
            ]
        );
        try {
            $post = request()->all();
            $this->validatePost($post);
        } catch (Exception $ex) {
            return response()->json(['message' => $ex->getMessage()], 406);
        }
        $id = Recruit::modify(Recruit::find($id), $post);
        return $this->response->item(Recruit::find($id), new RecruitTransform());
    }

    public function destroy($id){
        $ids = explode(',', $id);
        $recruit_counts = Recruit::whereIn('id', $ids)->count();

        if ($recruit_counts != count($ids)) {
            return response()->json(['message' => 'request parameter is not validated'], 406);
        }

        Recruit::whereIn('id', $ids)->delete();
        return response()->json(['msg' => '删除成功'], 200);
    }

    public function status($id, $status){
        $recruit = Recruit::find($id);
        if (empty($recruit)) {
            return response()->json(['error' => 'request parameter is not validated'], 406);
        }
        $recruit->update(['status'=>$status]);
        return $this->response
            ->item($recruit, new RecruitTransform());
    }

    private function validatePost($post){

    }
}
