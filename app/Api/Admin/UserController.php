<?php

namespace App\Api\Admin;

use App\Api\ApiController as Controller;
use Illuminate\Http\Request;
use App\Models\Users;
use App\Transformers\Admin\UserTransform;


class UserController extends Controller
{

    public function get()
    {
        request()->validate(
            [
                'limit' => 'int|min:1|max:200',
                'offset' => 'int',
                'sort' => 'string',
                'order' => 'string',
            ]
        );
        $post = request()->all();
        request()->offsetSet('page', floor($post['offset'] / $post['limit']) + 1 );
        $Users = Users::filter($post)->paginate(request()->get('limit', 20));
        $User_transform = new UserTransform();
        return $this->response->paginator($Users, $User_transform);
    }

    public function destroy($id){
        $ids = explode(',', $id);
        $User_counts = Users::whereIn('id', $ids)->count();

        if ($User_counts != count($ids)) {
            return response()->json(['message' => 'request parameter is not validated'], 406);
        }

        Users::whereIn('id', $ids)->delete();
        return response()->json(['msg' => '删除成功'], 200);
    }

    public function getAll(){
        $Users = Users::all();
        return response()->json($Users);
    }

}