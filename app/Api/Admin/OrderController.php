<?php

namespace App\Api\Admin;

use App\Api\ApiController as Controller;
use Illuminate\Http\Request;
use App\Models\Orders;
use App\Transformers\Admin\OrderTransform;

class OrderController extends Controller
{

    public function get()
    {
        request()->validate(
            [
                'limit' => 'int|min:1|max:200',
                'offset' => 'int',
                'sort' => 'string',
                'order' => 'string',
            ]
        );
        $post = request()->all();
        request()->offsetSet('order', floor($post['offset'] / $post['limit']) + 1 );
        $orders = Orders::filter($post)->where('status', Orders::STATUS_CONFIRM)->paginate(request()->get('limit', 20));
        $product_transform = new OrderTransform();
        return $this->response->paginator($orders, $product_transform);
    }

    public function getCurrentUserOrders()
    {
        request()->validate(
            [
                'limit' => 'int|min:1|max:200',
                'offset' => 'int',
                'sort' => 'string',
                'order' => 'string',
            ]
        );
        $post = request()->all();
        request()->offsetSet('order', floor($post['offset'] / $post['limit']) + 1 );
        $orders = Orders::filter($post)->where('user_id', Auth::user()->id)->paginate(request()->get('limit', 20));
        $product_transform = new OrderTransform();
        return $this->response->paginator($orders, $product_transform);
    }

    public function destroy($id){
        $ids = explode(',', $id);
        $product_counts = Orders::whereIn('id', $ids)->count();

        if ($product_counts != count($ids)) {
            return response()->json(['message' => 'request parameter is not validated'], 406);
        }

        Orders::whereIn('id', $ids)->delete();
        return response()->json(['msg' => '删除成功'], 200);
    }

    public function getAll(){
        $orders = Orders::all();
        return response()->json($orders);
    }

}