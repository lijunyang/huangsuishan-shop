<?php

namespace App\Api\Admin;

use App\Api\ApiController as Controller;
use App\Models\DevelopmentProcess;
use App\Transformers\Admin\DevelopmentProcessTransform;
use Illuminate\Http\Request;
use Exception;

class DevelopmentProcessController extends Controller
{
    public function index(){
        request()->validate(
            [
                'limit' => 'int|min:1|max:200',
                'offset' => 'int',
                'sort' => 'string',
                'order' => 'string',
            ]
        );
        $post = request()->all();
        request()->offsetSet('page', floor($post['offset'] / $post['limit']) + 1 );
        $banners = DevelopmentProcess::filter($post)
            ->paginate(request()->get('limit', 20));
        return $this->response->paginator($banners, new DevelopmentProcessTransform());
    }

    public function show($id){
        $banner = DevelopmentProcess::find($id);
        if (empty($banner)) {
            return response()->json(['error' => 'request parameter is not validated'], 406);
        }
        return $this->response
            ->item($banner, new DevelopmentProcessTransform());
    }

    public function create(){
        request()->validate(
            [
                'title' => 'required|string',
                'content' => 'required|string',
            ]
        );
        try {
            $post = request()->all();
            $this->validateProcessPost($post);
        } catch (Exception $ex) {
            return response()->json(['message' => $ex->getMessage()], 406);
        }
        $id = DevelopmentProcess::modify(new DevelopmentProcess(), $post);
        return $this->response->item(DevelopmentProcess::find($id), new DevelopmentProcessTransform());
    }

    public function update($id){
        request()->validate(
            [
                'title' => 'required|string',
                'content' => 'required|string',
                'sort' => 'required|int',
            ]
        );
        try {
            $post = request()->all();
            $this->validateProcessPost($post);
        } catch (Exception $ex) {
            return response()->json(['message' => $ex->getMessage()], 406);
        }
        $id = DevelopmentProcess::modify(DevelopmentProcess::find($id), $post);
        return $this->response->item(DevelopmentProcess::find($id), new DevelopmentProcessTransform());
    }

    public function destroy($id){
        $ids = explode(',', $id);
        $banner_counts = DevelopmentProcess::whereIn('id', $ids)->count();

        if ($banner_counts != count($ids)) {
            return response()->json(['message' => 'request parameter is not validated'], 406);
        }

        DevelopmentProcess::whereIn('id', $ids)->delete();
        return response()->json(['msg' => '删除成功'], 200);
    }

    public function sort(){
        request()->validate(
            [
                'order' => 'required|array',
                'order.*.id' => 'required|int',
                'order.*.sort' => 'required|int',
            ]
        );
        $ids = DevelopmentProcess::updateProcessSort(request()->all()['order']);
        return $this->response
            ->collection(DevelopmentProcess::whereIn('id', $ids)->get(), new DevelopmentProcessTransform());
    }

    private function validateProcessPost($post){

    }
}
